/** @type {import('tailwindcss').Config} */

export default {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      colors: {
        'branch-blue': '#3371AF',
        'game1-color': 'rgba(91, 216, 255, 1)',
        "text-color": "#3371AF",
        // "main-color": "#FFC217",
        "black-1": "#060606",
      },
      backgroundImage: {
        "background-game-item": "url('/BackroundListGame.png')",
        "background-game-1": "url('/BGContainerGift.png')",
        // "bg-yellow-2": "url('/BackgroundYellow2.png')",
        "bg-yellow-5": "url('/BackgroundYellow2.png')",
        "bg-dark-3": "url('/Game2BGPremier.png')",
        "bg-loading": "url('/BackgroundLoading.png')",
        "background-game-1-item-bg": "url('/GiftBackGrLock.png')",
      },
      dropShadow: {
        "text-game-item": "0 4px 7px #202981",
        "button-shadow": "0 20px 20px rgba(0, 0, 0, 0.2)",
        money: [
          "0px 4px 4px rgba(0, 0, 0, 0.25)",
          "0px 4px 4px rgba(0, 0, 0, 0.25)",
        ],
        text: [
          "0px 3px 6px rgba(0, 0, 0, 0.50)",
          "-1px 1px 0px rgba(1, 26, 21, 0.25)",
        ],
      },
      boxShadow: {
        "game-2-gradient-1": "0px 4px 4px 0px rgba(0, 0, 0, 0.25)",
      },
      borderColor: {
        "gray-1": "rgba(0, 0, 0, 0.20)",
        "button-avatar": " #000",
      },
    },
  },
  plugins: [],
};
