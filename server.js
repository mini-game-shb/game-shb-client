const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const path = require("path");
const passport = require("passport");
require("dotenv").config();
const port = process.env.PORT || 5000;
const hostname = "127.0.0.1";
const cors = require("cors");
const http = require("http");
const methodOverride = require("method-override");

const socketIo = require("socket.io");

// app
const app = express();

const server = http.createServer(app);

module.exports = io = socketIo(server, {
  cors: {
    origin: "*",
  },
});

io.on("connection", (socket) => {
  console.log("New client connected");
});

//route
const user = require("./routes/user");
const files = require("./routes/files");
const notifications = require("./routes/notificaiton");
const type = require("./routes/type");
const category = require("./routes/category");
const setting = require("./routes/setting");
const reports = require("./routes/report");
const mockup = require("./routes/mockup");
const amz = require("./routes/amz");
const amzReport = require("./routes/amz-report");

app.use(cors());

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");

  req.io = io;

  next();
});

app.use("/uploads", express.static("./uploads"));

// body parser middleaware
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(methodOverride());

// passport
app.use(passport.initialize());
app.use(passport.session());

// connect DB
mongoose
  .connect(process.env.MONGOURLTEST, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
  })
  .then(() => console.log("Mongodb connected !!!"))
  .catch((err) => console.log(err));

// passport config
require("./config/passport")(passport);

// set global vars
app.use((req, res, next) => {
  res.locals.user = req.user || null;
  next();
});

// router
app.use("/api/user", user);
app.use("/api/files", files);
app.use("/api/notify", notifications);
app.use("/api/type", type);
app.use("/api/category", category);
app.use("/api/setting", setting);
app.use("/api/report", reports);
app.use("/api/mockup", mockup);
app.use("/api/amz", amz);
app.use("/api/amz-report", amzReport);

// test router
app.get("/test", async (req, res) => {
  User.find({})
    .then((data) => {
      res.json(data);
    })
    .catch((err) => console.log(err));
});

app.get("/", async (req, res) => {
  res.send("hello word");
});

// server static asts if in production
// if (process.env.NODE_ENV == "production") {
app.use(express.static("client/build"));
app.get("*", (req, res) => {
  res.sendFile(path.resolve(__dirname, "client", "build", "index.html"));
});
// }
// "192.168.1.27"
server.listen(port, hostname, () => console.log(`Server running on ${port}`));
