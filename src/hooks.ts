import dayjs from 'dayjs';
import { createTypedHooks } from 'easy-peasy';
import debounce from 'lodash.debounce';
import { MutableRefObject, useCallback, useEffect, useRef, useState } from 'react';

import config from './config';
import { EMITTER_EVENTS } from './constants';
import { dummyInventory, dummyPool } from './dummyData';
import gamefoxSDK, { Inventory, Pool } from './gamefoxSDK';
import { StoreModel } from './store';
import eventEmitter from './utils/eventEmitter';

const typedHooks = createTypedHooks<StoreModel>();

const useStoreActions = typedHooks.useStoreActions;
const useStoreState = typedHooks.useStoreState;
export const useTouchOutside = (): [MutableRefObject<HTMLDivElement | undefined>, string, (key: string) => void] => {
  const nodeRef = useRef<HTMLDivElement>();
  const [currentRefKey, setCurrentRefKey] = useState('');

  useEffect(() => {
    const clickEvent = (() => {
      if ('ontouchstart' in document.documentElement === true) {
        return 'touchstart';
      } else {
        return 'mousedown';
      }
    })();

    const handleTouch = debounce((event: TouchEvent | MouseEvent) => {
      if (!nodeRef.current) {
        setCurrentRefKey('');
        return;
      }
      const target = event.target as any;
      if (target === nodeRef.current || nodeRef.current?.contains(target)) {
        return;
      } else {
        setCurrentRefKey('');
        nodeRef.current = undefined;
      }
    }, 0);

    window?.document?.addEventListener(clickEvent, handleTouch);
    return () => {
      window?.document?.removeEventListener(clickEvent, handleTouch);
    };
  }, []);

  return [nodeRef, currentRefKey, setCurrentRefKey];
};
const useRefreshInventory = () => {
  const setInventory = useStoreActions((actions) => actions.setInventory);

  const fetchInventory = useCallback(async () => {
    const sdkParams = gamefoxSDK.getParams();
    const inventory = await gamefoxSDK.getInventory(sdkParams.campaignId, 'CAMPAIGN');
    setInventory(inventory);
  }, [setInventory]);

  return fetchInventory;
};

const useRefreshUser = () => {
  const setUser = useStoreActions((actions) => actions.setUser);
  const fetchUser = useCallback(async () => {
    const user = await gamefoxSDK.auth();
    setUser(user);
  }, [setUser]);
  return fetchUser;
};
export const useInventory = (): [Inventory | null, () => Promise<Inventory>] => {
  const inventory = useStoreState((state) => state.inventory);
  const setInventory = useStoreActions((actions) => actions.setInventory);

  const getInventory = useCallback(async () => {
    if (!config.online) {
      setInventory(dummyInventory);
      return dummyInventory;
    }

    const inventory = await gamefoxSDK.getInventory();
    inventory.items.sort((a, b) => {
      return dayjs(b.createdAt).unix() - dayjs(a.createdAt).unix();
    });
    setInventory(inventory);
    eventEmitter.emit(EMITTER_EVENTS.UPDATE_NUMBER_OF_TICKETS, inventory.tickets.length);
    return inventory;
  }, [setInventory]);

  return [inventory, getInventory];
};

export const usePool = (): [Pool, () => Promise<Pool | Pool[]>] => {
  const pool = useStoreState((state) => state.pool) as Pool;
  const setPool = useStoreActions((actions) => actions.setPool);

  const getPool = useCallback(async () => {
    if (!config.online) {
      setPool(dummyPool);
      return dummyPool;
    }

    const pool = await gamefoxSDK.getPools();
    setPool(pool);
    return pool;
  }, [setPool]);

  return [pool, getPool];
};

export { useRefreshInventory, useRefreshUser, useStoreActions, useStoreState };
