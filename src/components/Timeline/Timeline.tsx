import React from 'react';

import classes from './Timeline.module.css';

interface TimelineProps {
  milestones: number[];
}

const Timeline: React.FC<TimelineProps> = ({ milestones }) => {
  return (
    <div className={classes.timeline}>
      {milestones.map((milestone, index) => (
        <div key={index} className="milestone">
          {milestone} điểm
        </div>
      ))}
    </div>
  );
};

export default Timeline;
