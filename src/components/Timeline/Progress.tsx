import { useEffect, useState } from 'react';

import classes from './Progress.module.css';

const ProgressBar = () => {
  const [_completed, setCompleted] = useState(0);

  useEffect(() => {
    setInterval(() => setCompleted(Math.floor(Math.random() * 100) + 1), 2000);
  }, []);

  return (
    <div className={classes.Wapper}>
      <div className={classes.group_pl2}>
        <img src="/assets/ui/group_pl2.png" />
      </div>
      <div className={classes.WapperBike}>
        <div className={classes.goBike}>
          <img src="/assets/ui/go.png" />
        </div>
        <div className={classes.container}>
          <div className={classes.filler}>{/* <span className={classes.label}>{`${completed}%`}</span> */}</div>
        </div>
        <div className={classes.bike}>
          <img src="/assets/ui/bike.png" />
        </div>
      </div>
      <div className={classes.WapperDoremi}>
        <div className={classes.hoa}>
          <img src="/assets/ui/hoa.png" />
        </div>
        <div className={classes.doremi}>
          <img src="/assets/ui/doremi.png" />
        </div>
      </div>

      <div className={classes.iconHeart}>
        <div className={classes.heart}>
          <img src="/assets/ui/heart.png" />
        </div>
        <div className={classes.pink}>
          <img src="/assets/ui/pink.png" />
        </div>
        <div className={classes.pink}>
          <img src="/assets/ui/pink.png" />
        </div>
      </div>

      <div className={classes.complete}>
        <div className={classes.completeText}>
          <h1>01</h1>
        </div>
        <div className={classes.completeText}>
          <h1>02</h1>
        </div>
        <div className={classes.completeText}>
          <h1>03</h1>
        </div>
      </div>
    </div>
  );
};

export default ProgressBar;
