import clsx from 'clsx';
import { useCallback } from 'react';

import { useStoreActions, useStoreState } from '../hooks';
import classes from './NoticeModal.module.css';

const NoticeModal = () => {
  const noticeModal = useStoreState((state) => state.noticeModal);
  const setNoticeModal = useStoreActions((actions) => actions.setNoticeModal);

  const handleClick = useCallback(() => {
    setNoticeModal({ ...noticeModal, visible: false });
  }, [noticeModal, setNoticeModal]);

  return (
    <div className={clsx(classes.noticeModal, noticeModal.visible ? classes.visible : classes.nonvisible)}>
      <div className={clsx(classes.container, noticeModal.visible ? classes.visible : classes.nonvisible)}>
        <p className={classes.title}>{noticeModal.notice}</p>
        <button className={classes.buttonClose} onClick={handleClick}>
          {noticeModal.buttonLabel || 'Close'}
        </button>
      </div>
    </div>
  );
};

export default NoticeModal;
