import { useCallback, useEffect, useState } from 'react';

import Button from '.';
import { AudioType, soundUtils } from '../../utils/soundUtils';
import { getMusicSetting, getSoundFXSetting, setMusicSetting, setSoundFXSetting } from '../../utils/storage';
import classes from './buttonSound.module.css';

function ButtonSound() {
  const [_isSoundFxOn, setIsSoundFxOn] = useState(false);
  const [isMusicOn, setIsMusicOn] = useState(false);

  useEffect(() => {
    const isEnableMusic = getMusicSetting();
    setIsMusicOn(isEnableMusic);
    const isEnableSoundFx = getSoundFXSetting();
    setIsSoundFxOn(isEnableSoundFx);
  }, []);

  const onUpdateMusicSetting = useCallback(async (on: boolean) => {
    if (soundUtils.mute(AudioType.MUSIC, !on)) {
      soundUtils.setMusicEnable(on);
      setMusicSetting(on);
      setIsMusicOn(on);
      soundUtils.play('theme');
      soundUtils.muteAll(false);
    } else {
      soundUtils.muteAll(true);
      soundUtils.setMusicEnable(false);
      soundUtils.setSoundFxEnable(false);
    }
  }, []);

  const onUpdateSoundFXSetting = useCallback(async (on: boolean) => {
    if (soundUtils.mute(AudioType.SOUND_FX, !on)) {
      soundUtils.setSoundFxEnable(on);
      setSoundFXSetting(on);
      setIsSoundFxOn(on);
    }
  }, []);

  const onUpdateSound = useCallback(() => {
    const currentState = getMusicSetting();
    const newState = !currentState;
    onUpdateMusicSetting(newState);
    onUpdateSoundFXSetting(newState);
  }, [onUpdateMusicSetting, onUpdateSoundFXSetting]);

  return (
    <Button title="sound" onClick={onUpdateSound}>
      <div
        className={classes.button}
        style={{
          backgroundImage: `url(${isMusicOn ? '/assets/ui2/mute.png' : '/assets/ui2/mute.png'})`,
        }}></div>
    </Button>
  );
}
export default ButtonSound;
