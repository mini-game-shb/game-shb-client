// components/CustomButton.tsx
import clsx from 'clsx';
import React from 'react';

interface CustomButtonProps {
  className?: string;
  children?: React.ReactNode;
  disabled?: boolean;
  customStyle?: React.CSSProperties;
  title?: string;
  onClick?: () => void;
  backgroundImage?: string;
}

const Button: React.FC<CustomButtonProps> = ({
  className,
  children,
  disabled = false,
  customStyle,
  title,
  onClick,
  backgroundImage,
}) => {
  const buttonStyles: React.CSSProperties = {
    ...customStyle,
    ...(backgroundImage ? { backgroundImage: `url(${backgroundImage})` } : {}),
    backgroundRepeat: 'none',
    backgroundSize: 'cover',
  };

  return (
    <button
      className={clsx('bg-transparent', 'text-white', 'font-bold', 'rounded', className)}
      disabled={disabled}
      style={buttonStyles}
      title={title}
      onClick={onClick}>
      {children}
    </button>
  );
};

export default Button;
