import cx from 'classnames';

import { ASSETS } from '../../../utils/assetUtils';
import Modal from '../modal';
import classes from './ChucMayMan.module.css';

interface props {
  isOpen: boolean;
  closeModal: () => void;
}

// Hom trung E-Voucher 500K
const ChucMayMan = ({ isOpen, closeModal }: props) => {
  const handleCloseModal = () => {
    closeModal();
  };
  return (
    <Modal visible={isOpen} onDismiss={closeModal}>
      <div className={classes.container}>
        <div className={classes.header}>
          <div className={cx(classes.text_title)}>
            Tiếc quá!!!<br></br> Chúc bạn may mắn lần sau
          </div>
        </div>
        <div className={classes['bg-img-wrapper']}>
          <img className={classes['bg-img']} src={ASSETS.MAT_BUON} alt="" />
          <div className={classes.btn_continue} onClick={handleCloseModal}>
            <img src={ASSETS.BTN_TIEP_TUC_G1} alt="" />
          </div>
          <div className={classes['btn-close']} onClick={handleCloseModal}>
            <img src={ASSETS.ICON_CLOSE_G1} alt="" />
          </div>
        </div>
      </div>
    </Modal>
  );
};

export default ChucMayMan;
