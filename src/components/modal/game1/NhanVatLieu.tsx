import { ASSETS } from '../../../utils/assetUtils';
import Modal from '../modal';
import classes from './NhanVatLieu.module.css';

interface props {
  isOpen: boolean;
  closeModal: () => void;
  rewardType: string;
}

const vatLieu:any = {
  GACH: 'Gạch',
  NUOC: 'Nước',
  CAT: 'Cát',
  THEP: 'Thép',
};

const iconVatLieu:any = {
  GACH: ASSETS.ICON_GACH,
  NUOC: ASSETS.ICON_NUOC,
  CAT: ASSETS.ICON_CAT,
  THEP: ASSETS.ICON_THEP,
};

// Popup Nhan vat lieu
const NhanVatLieu = ({ isOpen, closeModal, rewardType }: props) => {
  const handleCloseModal = () => {
    closeModal();
  };
  return (
    <Modal visible={isOpen} onDismiss={closeModal}>
      <div className={classes.container}>
        <div className={classes['bg-img-wrapper']}>
          <img className={classes['bg-img']} src={ASSETS.BG_NHAN_VAT_LIEU} alt="" />
          <div className={classes['vat-lieu']}>+1 vật liệu {vatLieu[rewardType]}</div>
          <div className={classes['icon-vat-lieu']}>
            <img className={classes['icon-vat-lieu-img']} src={iconVatLieu[rewardType]} alt="" />
          </div>
          <div className={classes['vat-lieu-con-thieu']}>Bạn cần thu thập thêm</div>
          <div className={classes.btn_continue} onClick={handleCloseModal}>
            <img src={ASSETS.BTN_TIEP_TUC_G1} alt="" />
          </div>
          <div className={classes['btn-close']} onClick={handleCloseModal}>
            <img src={ASSETS.ICON_CLOSE_G1} alt="" />
          </div>
        </div>
      </div>
    </Modal>
  );
};

export default NhanVatLieu;
