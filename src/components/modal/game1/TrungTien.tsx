import cx from 'classnames';

import { ASSETS } from '../../../utils/assetUtils';
import Modal from '../modal';
import classes from './TrungTien.module.css';

interface props {
  isOpen: boolean;
  closeModal: () => void;
  rewardValue: string;
}

const faceValue:any = {
  CASH_10K: '10.000 VNĐ',
  CASH_20K: '20.000 VNĐ',
  CASH_50K: '50.000 VNĐ',
  CASH_100K: '100.000 VNĐ',
  CASH_200K: '200.000 VNĐ',
  CASH_500K: '500.000 VNĐ',
};

// Popup Trung Tien
const TrungTien = ({ isOpen, closeModal, rewardValue }: props) => {
  const handleCloseModal = () => {
    closeModal();
  };
  return (
    <Modal visible={isOpen} onDismiss={closeModal}>
      <div className={classes.container}>
        <div className={classes.header}>
          <div className={cx(classes.text_title, 'pro-light-shb')}>Wow!</div>
        </div>
        <div className={classes['bg-img-wrapper']}>
          <img className={classes['bg-img']} src={ASSETS.BG_TRUNG_TIEN} alt="" />
          <div className={classes['bg-text']}>Chúc mừng bạn đã mở trúng</div>
          <div className={classes['text-content']}>{faceValue[rewardValue]}</div>
          <div className={classes.btn_continue} onClick={handleCloseModal}>
            <img src={ASSETS.BTN_TIEP_TUC} alt="" />
          </div>
          <div className={classes['btn-close']} onClick={handleCloseModal}>
            <img src={ASSETS.ICON_CLOSE} alt="" />
          </div>
        </div>
      </div>
    </Modal>
  );
};

export default TrungTien;
