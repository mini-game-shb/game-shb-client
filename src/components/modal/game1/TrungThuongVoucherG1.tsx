import cx from 'classnames';

import { ASSETS } from '../../../utils/assetUtils';
import Modal from '../modal';
import classes from './TrungThuongVoucherG1.module.css';

interface props {
  isOpen: boolean;
  closeModal: () => void;
}
// Hom trung E-Voucher 500K
const TrungThuongVoucherG1 = ({ isOpen, closeModal }: props) => {
  const handleCloseModal = () => {
    closeModal();
  };
  return (
    <Modal visible={isOpen} onDismiss={closeModal}>
      <div className={classes.container}>
        <div className={classes.header}>
          <div className={cx(classes.text_title)}>Xờiii, Tuyệt vời!!!</div>
        </div>
        <div className={classes['bg-img-wrapper']}>
          <img src={ASSETS.G1_TRUNG_THUONG_VOUCHER} alt="" className={classes['bg-img']} />
          <div className={classes['bg-text']}>Bạn đã nhận được E-Voucher trị giá 500.000 vnđ</div>
          <div className={classes.btn_continue} onClick={handleCloseModal}>
            <img src={ASSETS.G1_BTN_TIEP_TUC} alt="" />
          </div>
          <div className={classes['btn-close']} onClick={handleCloseModal}>
            <img src={ASSETS.G1_ICON_CLOSE} alt="" />
          </div>
        </div>
      </div>
    </Modal>
  );
};

export default TrungThuongVoucherG1;
