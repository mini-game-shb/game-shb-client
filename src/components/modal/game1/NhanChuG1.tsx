import cx from 'classnames';

import { ASSETS } from '../../../utils/assetUtils';
import Modal from '../modal';
import classes from './NhanChuG1.module.css';

interface props {
  isOpen: boolean;
  closeModal: () => void;
}

// Hom trung E-Voucher 500K
const NhanChuG1 = ({ isOpen, closeModal }: props) => {
  const handleCloseModal = () => {
    closeModal();
  };
  return (
    <Modal visible={isOpen} onDismiss={closeModal}>
      <div className={classes.container}>
        <div className={classes.header}>
          <div className={cx(classes.text_title)}>Chúc mừng bạn nhận được 1 chữ Tâm</div>
        </div>
        <div className={classes['bg-img-wrapper']}>
          <img src={ASSETS.G1_CHU_TAM} alt="" />
          <div className={classes.btn_continue} onClick={handleCloseModal}>
            <img src={ASSETS.G1_CHANG_2_TIN} alt="" />
          </div>
          <div className={classes['btn-close']} onClick={handleCloseModal}>
            <img src={ASSETS.G1_ICON_CLOSE} alt="" />
          </div>
        </div>
      </div>
    </Modal>
  );
};

export default NhanChuG1;
