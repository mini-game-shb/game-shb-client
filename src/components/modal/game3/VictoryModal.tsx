import { ASSETS } from '../../../utils/assetUtils';
import Modal from '../modal';
import classes from './VictoryModal.module.css';

interface props {
  isOpen: boolean;
  closeModal: () => void;
}

// Modal chúc mừng xây dựng thành công
const VictoryModal = ({ isOpen, closeModal }: props) => {
  const handleCloseModal = () => {
    closeModal();
  };
  return (
    <Modal visible={isOpen} onDismiss={closeModal}>
      <div className={classes.container}>
        <div className={classes['bg-img-wrapper']}>
          <img src={ASSETS.VICTORY} alt="" className={classes['bg-img']} />
          <div className={classes.btn_continue} onClick={handleCloseModal}>
            <img src={ASSETS.BTN_NHAN_QUA} alt="" />
          </div>
          <div className={classes['btn-close']} onClick={handleCloseModal}>
            <img src={ASSETS.ICO_CLOSE_G3} alt="" />
          </div>
        </div>
      </div>
    </Modal>
  );
};

export default VictoryModal;
