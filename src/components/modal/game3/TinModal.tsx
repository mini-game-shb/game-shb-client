import Modal from '../modal';
import classes from './TinModal.module.css';

interface props {
  isOpen: boolean;
  closeModal: () => void;
}

// modal chu tin
const TinModal = ({ isOpen, closeModal }: props) => {
  const handleCloseModal = () => {
    closeModal();
  };
  return (
    <Modal visible={isOpen} onDismiss={closeModal}>
      <div className={classes.container}>
        <div className={classes.header}>
          <div className={classes.text_title}>
            <b>
              CHÚC MỪNG BẠN<br></br> NHẬN ĐƯỢC 1 CHỮ TÍN
            </b>
          </div>
        </div>
        <div className={classes['bg-img-wrapper']}>
          <img src={'assets/ui3/tin.png'} alt="" />
          <div className={classes.btn_continue} onClick={handleCloseModal}>
            <img src={'assets/ui3/btn_round_4.png'} alt="" />
          </div>
          <div className={classes['btn-close']} onClick={handleCloseModal}>
            <img src={'assets/ui3/icon_close.png'} alt="" />
          </div>
        </div>
      </div>
    </Modal>
  );
};

export default TinModal;
