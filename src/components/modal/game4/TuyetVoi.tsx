import { ASSETS } from '../../../utils/assetUtils';
import Modal from '../modal';
import classes from './TuyetVoi.module.css';

interface props {
  isOpen: boolean;
  closeModal: () => void;
}

// Popup Cau thu xuat sac
const TuyetVoi = ({ isOpen, closeModal }: props) => {
  const handleCloseModal = () => {
    closeModal();
  };
  return (
    <Modal visible={isOpen} onDismiss={closeModal}>
      <div className={classes.container}>
        <div className={classes['bg-img-wrapper']}>
          <img className={classes['bg-img']} src={ASSETS.BG_TUYET_VOI} alt="" />
          <div className={classes['bg-text']}>
            Bạn là một cầu thủ<br></br>xuất sắc
          </div>
          <div className={classes.btn_continue} onClick={handleCloseModal}>
            <img src={ASSETS.BTN_NHAN_QUA} alt="" />
          </div>
        </div>
      </div>
    </Modal>
  );
};

export default TuyetVoi;
