import { ASSETS } from '../../../utils/assetUtils';
import Modal from '../modal';
import classes from './HetThoiGian.module.css';

interface props {
  isOpen: boolean;
  closeModal: () => void;
}

// Popup Het thoi gian
const HetThoiGian = ({ isOpen, closeModal }: props) => {
  const handleCloseModal = () => {
    closeModal();
  };
  return (
    <Modal visible={isOpen} onDismiss={()=>closeModal}>
      <div className={classes.container}>
        <div className={classes['bg-img-wrapper']}>
          <img className={classes['bg-img']} src={ASSETS.BG_HET_THOI_GIAN} alt="" />
          <div className={classes['bg-text']}>
            Hết thời gian mất rồi.<br></br> Hãy cố gắng hơn ở lần sau nhé !!
          </div>
          <div className={classes.btn_continue} onClick={handleCloseModal}>
            <img src={ASSETS.BTN_BAT_DAU_LAI} alt="" />
          </div>
        </div>
      </div>
    </Modal>
  );
};

export default HetThoiGian;
