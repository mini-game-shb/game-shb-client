import cx from 'classnames';

import { ASSETS } from '../../../utils/assetUtils';
import Modal from '../modal';
import classes from './Chang5Tri.module.css';

interface props {
  isOpen: boolean;
  closeModal: () => void;
}

// Popup Trung chu
const Chang5Tri = ({ isOpen, closeModal }: props) => {
  const handleCloseModal = () => {
    closeModal();
  };
  return (
    <Modal visible={isOpen} onDismiss={closeModal}>
      <div className={classes.container}>
        <div className={classes.header}>
          <div className={cx(classes.text_title)}>
            Chúc mừng bạn <br></br>nhận được 1 chữ Tri
          </div>
        </div>
        <div className={classes['bg-img-wrapper']}>
          <img src={ASSETS.BG_CHANG_5_TRI} alt="" />
          <div className={classes.btn_continue} onClick={handleCloseModal}>
            <img src={ASSETS.BTN_CHANG_5_TRI} alt="" />
          </div>
          <div className={classes['btn-close']} onClick={handleCloseModal}>
            <img src={ASSETS.ICON_CLOSE} alt="" />
          </div>
        </div>
      </div>
    </Modal>
  );
};

export default Chang5Tri;
