import { ASSETS } from '../../utils/assetUtils';
import Modal from '../modal/modal';
import classes from './NhanLuotChoi.module.css';

interface props {
  isOpen: boolean;
  closeModal: () => void;
  luotChoi: string;
}

const LuotChoi:any= {
  1: ASSETS.THEM_1_LUOT_CHOI,
  2: ASSETS.THEM_2_LUOT_CHOI,
  3: ASSETS.THEM_3_LUOT_CHOI,
  4: ASSETS.THEM_4_LUOT_CHOI,
  5: ASSETS.THEM_5_LUOT_CHOI,
  6: ASSETS.THEM_6_LUOT_CHOI,
  7: ASSETS.THEM_7_LUOT_CHOI,
  8: ASSETS.THEM_8_LUOT_CHOI,
  9: ASSETS.THEM_9_LUOT_CHOI,
};

// Popup Nhan luot choi
const NhanLuotChoi = ({ isOpen, closeModal, luotChoi }: props) => {
  const handleCloseModal = () => {
    closeModal();
  };
  return (
    <Modal visible={isOpen} onDismiss={closeModal}>
      <div className={classes.container}>
        <div className={classes['bg-img-wrapper']}>
          <img className={classes['bg-img']} src={ASSETS.BG_NHAN_LUOT_CHOI} alt="" />
          <div className={classes['luot-choi']}>
            <img src={LuotChoi[luotChoi]} alt="" />
          </div>
          <div className={classes['bg-text']}>lượt chơi</div>
          <div className={classes.btn_continue} onClick={handleCloseModal}>
            <img src={ASSETS.BTN_VAO_CHOI} alt="" />
          </div>
        </div>
      </div>
    </Modal>
  );
};

export default NhanLuotChoi;
