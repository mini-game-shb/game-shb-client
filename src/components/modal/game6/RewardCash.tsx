import Modal from '../modal';
import classes from './RewardCash.module.css';

interface props {
  isOpen: boolean;
  closeModal: () => void;
  rewardValue: string;
}

const faceValue :any= {
  CASH_10K: '10.000 VNĐ',
  CASH_20K: '20.000 VNĐ',
  CASH_50K: '50.000 VNĐ',
  CASH_100K: '100.000 VNĐ',
  CASH_200K: '200.000 VNĐ',
  CASH_500K: '500.000 VNĐ',
};

// Trung tien mat
const RewardCash = ({ isOpen, closeModal, rewardValue }: props) => {
  const handleCloseModal = () => {
    closeModal();
  };
  return (
    <Modal visible={isOpen} onDismiss={closeModal}>
      <div className={classes.container}>
        <div className={classes.header}>
          <div className={classes.text_title}>
            <b>Wow!</b>
          </div>
        </div>
        <div className={classes['bg-img-wrapper']}>
          <img src={'assets/ui6/happy.png'} alt="" />
          <div className={classes.text_content}>
            <b>{faceValue[rewardValue]}</b>
          </div>
          <div className={classes['btn_continue']} onClick={handleCloseModal}>
            <img src={'assets/ui6/btn_continue_3.png'} alt="" />
          </div>
          <div className={classes['btn-close']} onClick={handleCloseModal}>
            <img src={'assets/ui6/icon_close.png'} alt="" />
          </div>
        </div>
      </div>
    </Modal>
  );
};

export default RewardCash;
