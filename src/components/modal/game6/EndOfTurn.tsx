import Modal from '../modal';
import classes from './EndOfTurn.module.css';

interface props {
  isOpen: boolean;
  closeModal: () => void;
}

// Modal hết lượt chơi
const EndOfTurn = ({ isOpen, closeModal }: props) => {
  const handleCloseModal = () => {
    closeModal();
  };
  return (
    <Modal visible={isOpen} onDismiss={closeModal}>
      <div className={classes.container}>
        <div className={classes.header}>
          <div className={classes.text_title}>
            <b>Ôi bạn hết lượt chơi mất rồi!</b>
          </div>
        </div>
        <div className={classes['bg-img-wrapper']}>
          <img src={'assets/ui6/kiem_luot_choi.png'} alt="" />
          <div className={classes.btn_continue} onClick={handleCloseModal}>
            <img src={'assets/ui6/btn_tim_them_luot.png'} alt="" />
          </div>
          <div className={classes['btn-close']} onClick={handleCloseModal}>
            <img src={'assets/ui6/icon_close.png'} alt="" />
          </div>
        </div>
      </div>
    </Modal>
  );
};

export default EndOfTurn;
