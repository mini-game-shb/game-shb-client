import Modal from '../modal';
import classes from './LuckyCode.module.css';

interface props {
  isOpen: boolean;
  closeModal: () => void;
  attendanceCode: string;
}

const LuckyCode = ({ isOpen, closeModal, attendanceCode }: props) => {
  const handleCloseModal = () => {
    closeModal();
  };
  return (
    <Modal visible={isOpen} onDismiss={closeModal}>
      <div className={classes.container}>
        <div className={classes.header}>
          <div className={classes.text_title}>
            <b>
              Wow, chúc mừng bạn đã <br></br> nhận được mã dự quay số
            </b>
          </div>
        </div>
        <div className={classes['bg-img-wrapper']}>
          <img src={'assets/ui6/gold_reward.png'} alt="" />
          <div className={classes.text_content}>Mã số dự thưởng của bạn là</div>
          <div className={classes['text_code']}>
            <b>{attendanceCode}</b>
          </div>
          <div className={classes['text-content-2']}>
            Quay số sẽ diễn ra vào ngày <br></br> <b>14:00 29/10/2023</b>
          </div>
          <div className={classes['btn-continue']} onClick={handleCloseModal}>
            <img src={'assets/ui6/btn_continue_3.png'} alt="" />
          </div>
          <div className={classes['btn-close']} onClick={handleCloseModal}>
            <img src={'assets/ui6/icon_close.png'} alt="" />
          </div>
        </div>
      </div>
    </Modal>
  );
};

export default LuckyCode;
