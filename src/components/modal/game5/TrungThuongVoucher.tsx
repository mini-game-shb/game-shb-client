import cx from 'classnames';

import { ASSETS } from '../../../utils/assetUtils';
import Modal from '../modal';
import classes from './TrungThuongVoucher.module.css';

interface props {
  isOpen: boolean;
  closeModal: () => void;
  rewardValue: string;
}
const faceValue:any = {
  CASH_10K: '10.000 VNĐ',
  CASH_20K: '20.000 VNĐ',
  CASH_50K: '50.000 VNĐ',
  CASH_100K: '100.000 VNĐ',
  CASH_200K: '200.000 VNĐ',
  CASH_500K: '500.000 VNĐ',
};

const TrungThuongVoucher = ({ isOpen, closeModal, rewardValue }: props) => {
  const handleCloseModal = () => {
    closeModal();
  };
  return (
    <Modal visible={isOpen} onDismiss={closeModal}>
      <div className={classes.container}>
        <div className={classes.header}>
          <div className={cx(classes.text_title)}>Xờiii, Tuyệt vời!!!</div>
        </div>
        <div className={classes['bg-img-wrapper']}>
          <img className={classes['bg-img']} src={ASSETS.TRUNG_THUONG_VOUCHER} alt="" />
          <div className={classes['bg-text']}>Bạn đã nhận được E-Voucher trị giá {faceValue[rewardValue]}</div>

          <div className={classes.btn_continue} onClick={handleCloseModal}>
            <img src={ASSETS.BTN_TIEP_TUCC} alt="" />
          </div>
          <div className={classes['btn-close']} onClick={handleCloseModal}>
            <img src={ASSETS.ICON_CLOSEE} alt="" />
          </div>
        </div>
      </div>
    </Modal>
  );
};

export default TrungThuongVoucher;
