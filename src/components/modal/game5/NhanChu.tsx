import cx from 'classnames';

import { ASSETS } from '../../../utils/assetUtils';
import Modal from '../modal';
import classes from './NhanChu.module.css';

interface props {
  isOpen: boolean;
  closeModal: () => void;
  onNext: () => void;
}

// Hom trung E-Voucher 500K
const NhanChu = ({ isOpen, closeModal, onNext }: props) => {
  const handleCloseModal = () => {
    closeModal();
  };
  return (
    <Modal visible={isOpen} onDismiss={closeModal}>
      <div className={classes.container}>
        <div className={classes.header}>
          <div className={cx(classes.text_title)}>
            Chúc mừng bạn <br></br>nhận được 1 chữ Trí
          </div>
        </div>
        <div className={classes['bg-img-wrapper']}>
          <img src={ASSETS.CHU_TRI} alt="" />
          <div className={classes.btn_continue} onClick={onNext}>
            <img src={ASSETS.CHANG_5} alt="" />
          </div>
          <div className={classes['btn-close']} onClick={handleCloseModal}>
            <img src={ASSETS.ICON_CLOSEE} alt="" />
          </div>
        </div>
      </div>
    </Modal>
  );
};

export default NhanChu;
