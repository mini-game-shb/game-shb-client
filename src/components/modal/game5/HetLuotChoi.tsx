import cx from 'classnames';

import { ASSETS } from '../../../utils/assetUtils';
import Modal from '../modal';
import classes from './HetLuotChoi.module.css';

interface props {
  isOpen: boolean;
  closeModal: () => void;
}

// Hom trung E-Voucher 500K
const HetLuotChoi = ({ isOpen, closeModal }: props) => {
  const handleCloseModal = () => {
    closeModal();
  };
  return (
    <Modal visible={isOpen} onDismiss={closeModal}>
      <div className={classes.container}>
        <div className={classes.header}>
          <div className={cx(classes.text_title)}>Ôi bạn hết lượt chơi mất rồi!</div>
        </div>
        <div className={classes['bg-img-wrapper']}>
          <img className={classes['bg-img']} src={ASSETS.HET_LUOT_CHOI} alt="" />
          <div className={classes['bg-text']}>Bạn hãy hoàn thành thêm nhiệm vụ để kiếm lượt chơi nhé!</div>
          <div className={classes.btn_timthemluot} onClick={handleCloseModal}>
            <img src={ASSETS.BTN_TIM_THEM_LUOTT} alt="" />
          </div>
          <div className={classes['btn-close']} onClick={handleCloseModal}>
            <img src={ASSETS.ICON_CLOSEE} alt="" />
          </div>
        </div>
      </div>
    </Modal>
  );
};

export default HetLuotChoi;
