import { ASSETS } from '../../../utils/assetUtils';
import Modal from '../modal';
import classes from './ThuLaiNao.module.css';

interface props {
  isOpen: boolean;
  closeModal: () => void;
}

const ThuLaiNao = ({ isOpen, closeModal }: props) => {
  const handleCloseModal = () => {
    closeModal();
  };
  return (
    <Modal visible={isOpen} onDismiss={closeModal}>
      <div className={classes.container}>
        <div className={classes.header}></div>
        <div className={classes['bg-img-wrapper']}>
          <img className={classes['bg-img']} src={ASSETS.BG_THU_LAI_NAO} alt="" />
          <div className={classes['bg-text']}>Cố gắng hơn ở lần kế tiếp nhé !</div>
          <div className={classes.btn_continue} onClick={handleCloseModal}>
            <img src={ASSETS.BTN_BAT_DAU_LAII} alt="" />
          </div>
        </div>
      </div>
    </Modal>
  );
};

export default ThuLaiNao;
