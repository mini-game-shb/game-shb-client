import cx from 'classnames';

import { ASSETS } from '../../../utils/assetUtils';
import Modal from '../modal';
import classes from './ChucMungKhien.module.css';

interface props {
  isOpen: boolean;
  closeModal: () => void;
}

// Hom trung E-Voucher 500K
const ChucMungKhien = ({ isOpen, closeModal }: props) => {
  const handleCloseModal = () => {
    closeModal();
  };
  return (
    <Modal visible={isOpen} onDismiss={closeModal}>
      <div className={classes.container}>
        <div className={classes.header}>
          <div className={cx(classes.text_title, 'pro-light-shb')}></div>
        </div>
        <div className={classes['bg-img-wrapper']}>
          <img className={classes['bg-img']} src={ASSETS.BG_CHUC_MUNG_KHIEN} alt="" />
          <p className={classes['bg-text']}>Bạn đã bảo vệ thành công ứng dụng SHB</p>
          <div className={classes.btn_continue} onClick={handleCloseModal}>
            <img src={ASSETS.BTN_NHAN_QUAA} alt="" />
          </div>
        </div>
      </div>
    </Modal>
  );
};

export default ChucMungKhien;
