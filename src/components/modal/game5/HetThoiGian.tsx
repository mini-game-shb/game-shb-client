import { ASSETS } from '../../../utils/assetUtils';
import Modal from '../modal';
import classes from './HetThoiGian.module.css';

interface props {
  isOpen: boolean;
  closeModal: () => void;
}

// Hom trung E-Voucher 500K
const HetThoiGian = ({ isOpen, closeModal }: props) => {
  const handleCloseModal = () => {
    closeModal();
  };
  return (
    <Modal visible={isOpen} onDismiss={closeModal}>
      <div className={classes.container}>
        <div className={classes.header}></div>
        <div className={classes['bg-img-wrapper']}>
          <img className={classes['bg-img']} src={ASSETS.BG_OVER_TIME} alt="" />
          <div className={classes['bg-text']}>Hết thời gian mất rồi. Hãy cố gắng hơn ở lần sau nhé!!</div>
          <div className={classes.btn_continue} onClick={handleCloseModal}>
            <img src={ASSETS.BTN_BAT_DAU_LAII} alt="" />
          </div>
        </div>
      </div>
    </Modal>
  );
};

export default HetThoiGian;
