import cx from 'classnames';

import Modal from '../modal';
import classes from './RewardVoucherModal.module.css';

interface props {
  isOpen: boolean;
  closeModal: () => void;
}

// Hom trung E-Voucher 500K
const RewardVoucherModal = ({ isOpen, closeModal }: props) => {
  const handleCloseModal = () => {
    closeModal();
  };
  return (
    <Modal visible={isOpen} onDismiss={closeModal}>
      <div className={classes.container}>
        <div className={classes.header}>
          <div className={cx(classes.text_title, 'pro-light-shb')}>Tadaaa, chúc mừng bạn đã mở trúng E-Voucher</div>
        </div>
        <div className={classes['bg-img-wrapper']}>
          <img src={'assets/ui2/e_voucher_500k.png'} alt="" />
          <div className={classes.btn_continue} onClick={handleCloseModal}>
            <img src={'assets/ui2/btn_tiep_tuc.png'} alt="" />
          </div>
          <div className={classes['btn-close']} onClick={handleCloseModal}>
            <img src={'assets/ui2/icon_close.png'} alt="" />
          </div>
        </div>
      </div>
    </Modal>
  );
};

export default RewardVoucherModal;
