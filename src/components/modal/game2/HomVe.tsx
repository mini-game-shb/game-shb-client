import { ASSETS } from '../../../utils/assetUtils';
import Modal from '../modal';
import classes from './HomVe.module.css';

interface props {
  isOpen: boolean;
  closeModal: () => void;
  manhVe: string;
}

const pathManhVe:any = {
  ve1: ASSETS.HOM_VE_1,
  ve2: ASSETS.HOM_VE_2,
  ve3: ASSETS.HOM_VE_3,
  ve4: ASSETS.HOM_VE_4,
};

// Hom trung manh ve may bay
const HomVe = ({ isOpen, closeModal, manhVe }: props) => {
  const handleCloseModal = () => {
    closeModal();
  };
  return (
    <Modal visible={isOpen} onDismiss={closeModal}>
      <div className={classes.container}>
        <div className={classes.header}>
          <div className={classes.text_title}>
            <b>
              Chúc mừng bạn đã tìm được mảnh <br></br> ghép đầu tiên của Vé máy bay.
            </b>
          </div>
          <div className={classes.text_content}>
            Tiếp tục chơi để thu thập đủ các mảnh ghép còn <br></br> lại và hoàn thành chặng TIN nhé!
          </div>
        </div>
        <div className={classes['bg-img-wrapper']}>
          <img src={pathManhVe[manhVe]} alt="" />
          <div className={classes.btn_continue} onClick={handleCloseModal}>
            <img src={'assets/ui2/btn_tiep_tuc.png'} alt="" />
          </div>
          <div className={classes['btn-close']} onClick={handleCloseModal}>
            <img src={'assets/ui2/icon_close.png'} alt="" />
          </div>
        </div>
      </div>
    </Modal>
  );
};

export default HomVe;
