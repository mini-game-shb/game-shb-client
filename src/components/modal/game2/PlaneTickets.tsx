import Modal from '../modal';
import classes from './PlaneTickets.module.css';

interface props {
  isOpen: boolean;
  closeModal: () => void;
  onOk: () => void;
}

// Hom trung ve may bay
const PlaneTickets = ({ isOpen, closeModal, onOk }: props) => {
  const handleCloseModal = () => {
    closeModal();
  };
  return (
    <Modal visible={isOpen} onDismiss={closeModal}>
      <div className={classes.container}>
        <div className={classes.header}>
          <div className={classes.text_title}>
            <b>Xờiii, Tuyệt vời!!!</b>
          </div>
        </div>
        <div className={classes['bg-img-wrapper']}>
          <div className={classes['bg-image']}>
            <img src={'assets/ui2/trung_ve_may_bay.png'} alt="" />
          </div>
          <div className={classes.btn_continue} onClick={() => onOk()}>
            <img src={'assets/ui2/btn_fly.png'} alt="" />
          </div>
          <div className={classes['btn-close']} onClick={handleCloseModal}>
            <img src={'assets/ui2/icon_close.png'} alt="" />
          </div>
        </div>
      </div>
    </Modal>
  );
};

export default PlaneTickets;
