import cx from 'classnames';
import React, { useCallback, useEffect, useState } from 'react';

import { useStoreState } from '../hooks';
import { WORLD_HEIGHT, WORLD_WIDTH } from '../utils/constants';
import classes from './Layout.module.css';
import NoticeModal from './NoticaModal';

const RATIO = WORLD_WIDTH / WORLD_HEIGHT;

const Layout = ({ children }: { children?: React.ReactNode | React.ReactNode[] }) => {
  const containerStyle = useStoreState((state) => state.layoutContainerStyle);
  const mainStyle = useStoreState((state) => state.layoutMainStyle);

  const [inited, setInited] = useState(false);

  const handleResize = useCallback(() => {
    const viewWidth = Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0);
    const viewHeight = Math.max(document.documentElement.clientHeight || 0, window.innerHeight || 0);

    document.documentElement.style.setProperty('--app-width', `${viewWidth}px`);
    document.documentElement.style.setProperty('--app-height', `${viewHeight}px`);
    if (viewWidth >= viewHeight * RATIO) {
      document.documentElement.style.setProperty('--viewport-width', `${viewHeight * RATIO}px`);
      document.documentElement.style.setProperty('--viewport-height', `${viewHeight}px`);
    } else {
      document.documentElement.style.setProperty('--viewport-width', `${viewWidth}px`);
      document.documentElement.style.setProperty('--viewport-height', `${(viewWidth * WORLD_HEIGHT) / WORLD_WIDTH}px`);
    }
  }, []);

  const initStyles = useCallback(() => {
    handleResize();
    window.addEventListener('resize', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, [handleResize]);

  useEffect(() => {
    if (!inited) {
      initStyles();
    }
    setInited(true);
  }, [initStyles, inited]);

  if (!inited) {
    return null;
  }

  return (
    <div className={classes.container} style={containerStyle}>
      <div className={classes.main} style={mainStyle}>
        <div className={cx(classes.content)}>{children}</div>
      </div>
      <NoticeModal />
    </div>
  );
};

export default Layout;
