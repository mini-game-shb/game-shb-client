import cx from 'classnames';
import { PropsWithChildren, ReactElement, useCallback } from 'react';
import { useNavigate } from 'react-router';

import config from '../config';
import gamefoxSDK from '../gamefoxSDK';
import classes from './Header.module.css';
import Button from './button';
import ButtonSound from './button/buttonSound';

type Props = {
  showBackBtn?: boolean;
  hideShareBtn?: boolean;
  hideMute?: boolean;
  content?: string;
  user?: string;
  onBack?: () => void;
  children?: ReactElement;
};

const Header: React.FC<PropsWithChildren<Props>> = ({
  children,
  showBackBtn,
  hideShareBtn,
  hideMute,
  content,
  user,
  onBack,
}: Props) => {
  const navigate = useNavigate();

  const onShareClicked = useCallback(() => {
    gamefoxSDK.postMessage({
      eventType: 'SHARE',
      eventData: {
        link: config.shareUrl,
      },
    });
  }, []);

  return (
    <div className={classes.header}>
      <div className={classes.headerWrapper}>
        <div className={classes.headerBtns}>
          {!hideShareBtn && (
            <Button className={classes.shareBtn} backgroundImage="/assets/ui2/share.png" onClick={onShareClicked} />
          )}
          {showBackBtn && (
            <Button
              className={classes.btnBack}
              backgroundImage="/assets/ui2/back.png"
              onClick={() => {
                onBack ? onBack() : navigate(-1);
              }}></Button>
          )}
          {content && (
            <div className={cx(classes.content, 'pro-light-shb')}>
              <span className={classes.contet_text}>{content}</span> &nbsp;
              <span className={classes.content_user}>{user}</span>
            </div>
          )}
          {hideMute && <ButtonSound />}
        </div>
        {children}
      </div>
    </div>
  );
};

export default Header;
