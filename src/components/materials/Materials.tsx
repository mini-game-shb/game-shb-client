import { ASSETS } from '../../utils/assetUtils';
import classes from './Material.module.css';

type Props = {
  cat: number;
  gach: number;
  nuoc: number;
  thep: number;
};

export const MaterialComponent: React.FC<Props> = ({ cat, gach, thep, nuoc }: Props) => {
  return (
    <div className={classes['banner']}>
      <div>
        <div style={{ display: 'flex', alignItems: 'center',justifyContent: "space-around"}}>
          <div className={classes.container}>
            <div className={classes.nuoc}>
              <img src={ASSETS.NUOC} alt="" />
              <div style={{ position: 'absolute', left: '62%',bottom: "80%" }} className={classes.content}>
                <p>{nuoc}</p>
              </div>
            </div>
          </div>
          <div className={classes.container}>
            <div className={classes.gach}>
              <img src={ASSETS.GACH} alt="" />
              <div style={{ position: 'absolute', left: '80%', bottom: "80%" }} className={classes.content}>
                <p>{gach}</p>
              </div>
            </div>
          </div>
          <div className={classes.container}>
            <div className={classes.cat}>
              <img src={ASSETS.CAT} alt="" />
              <div style={{ position: 'absolute', left: '57%', top: '-34%' }} className={classes.content}>
                <p>{cat}</p>
              </div>
            </div>
          </div>
          <div className={classes.container}>
            <div className={classes.thep}>
              <img src={ASSETS.THEP} alt="" />
              <div style={{ position: 'absolute', left: '63%', bottom: '96%' }} className={classes.content}>
                <p>{thep}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
