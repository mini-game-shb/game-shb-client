import {
  AssetManager,
  Color,
  InputEvent,
  PolygonBatch,
  ShapeRenderer,
  Vector2,
  ViewportInputHandler,
  createGameLoop,
  createStage,
  createViewport,
} from 'gdxts';

import { WORLD_WIDTH, WORLD_HEIGHT, GAME_EVENTS } from '../../utils/constants';
import eventEmitter from '../../utils/eventEmitter';
import { ASSETS } from '../../utils/assetUtils';

export type GameEndedData = {
  config: any;
  type: string;
};

const ORIGIN_M = 0
const ORIGIN_S = 30

const MIN_Y_FORCE = 500;
const MAX_Y_FORCE = 700;

const BONG_ORIGIN_WIDTH = WORLD_WIDTH * 0.425;

const BONG_ORIGIN_X = WORLD_WIDTH / 2;
const BONG_ORIGIN_Y = WORLD_HEIGHT * 0.73;

const MAX_FLY_TIME = 1; // 1s
const TIME_STEP = 0.016;

export const init = async () => {
  const stage = createStage();
  const canvas = stage.getCanvas();
  const viewport = createViewport(canvas, WORLD_WIDTH, WORLD_HEIGHT, { crop: false });
  const gl = viewport.getContext();

  const assetManager = new AssetManager(gl);
  assetManager.loadTexture(ASSETS.BG4, 'bg');
  assetManager.loadAtlas('./assets/atlas/da_bong.atlas', 'da_bong');
  assetManager.loadFont('./assets/font/game4.fnt', 'game4', true);

  const camera = viewport.getCamera();
  camera.setYDown(true);

  await assetManager.finishLoading();

  const text = assetManager.getFont('game4')!;

  const batch = new PolygonBatch(gl);
  batch.setYDown(true);

  const bg = assetManager.getTexture('bg')!;
  const altas = assetManager.getAtlas('da_bong')!;

  const bong = altas.findRegion('bong')!;
  const score = altas.findRegion('thong_ke')!;
  const target = altas.findRegion('muc_tieu')!;
  const shapeRenderer = new ShapeRenderer(gl);


  //const
  let bongWidth = BONG_ORIGIN_WIDTH;
  let bongHeight = (bongWidth * bong.height) / bong.width;
  const originBong = new Vector2(BONG_ORIGIN_X, BONG_ORIGIN_Y);
  let forceX = 0;
  let forceY = 0;

  let bongVX = 0;
  let bongVY = 0;

  const originPreview = new Vector2(BONG_ORIGIN_X, BONG_ORIGIN_Y);

  const scoreWidth = WORLD_WIDTH * 0.6;
  const scoreHeight = (scoreWidth * score.height) / score.width;
  const scoreOrigin = new Vector2(WORLD_WIDTH / 2 - scoreWidth / 2, WORLD_HEIGHT * 0.06);

  const targetWidth = WORLD_WIDTH * 0.14;
  const targetHeight = (targetWidth * target.width) / target.height;
  const originTarget = new Vector2(0, 0);

  let scoreBall = 0;

const typeGameEnd = {
    timeEnd: 'timeEnd',
    winner: 'winner',
}

let gameStop = false

  const onGameEnded = (selected:any) => {
      gameStop = true
    eventEmitter.emit(GAME_EVENTS.GAME_ENDED, {
      type: selected,
    });
  };

  const handleResetTarget = () => {
    originTarget.set(Math.random() * (470 - targetWidth - 170) + 170, Math.random() * (480 - targetHeight - 290) + 290);
  };

  handleResetTarget();

  const reset = () => {
    gameStop= false
  scoreBall = 0
  minutes = ORIGIN_M
  seconds = ORIGIN_S
  handleSetTime()
  };

  let kicked = false;

  const inputHandler = new ViewportInputHandler(viewport);

  gl.clearColor(0, 0, 0, 0);

  let previewPath: { x: number; y: number; }[] = [];
  let isPreview = false;

  const calculateV = () => {
    const { x, y } = inputHandler.getTouchedWorldCoord();
    // Tính toán lực tác dụng lên bóng
    forceX = x - originBong.x;
    forceY = y - originBong.y; // negative number

    if (Math.abs(forceY) < MIN_Y_FORCE) {
      forceY = -MIN_Y_FORCE;
    }
    if (Math.abs(forceY) > MAX_Y_FORCE) {
      forceY = -MAX_Y_FORCE;
    }

    // Áp dụng lực lên vận tốc bóng
    bongVX = forceX * 0.042;
    bongVY = forceY * 0.042;
    flyTime = 0;
  };
  inputHandler.addEventListener(InputEvent.TouchStart, () => {
    if(gameStop) return
    if (kicked) {
      return;
    }
    isPreview = true;

    originPreview.set(BONG_ORIGIN_X, BONG_ORIGIN_Y);
    previewPath = [];
    calculateV();
    simulation();
  });

  inputHandler.addEventListener(InputEvent.TouchMove, () => {
    if(gameStop) return
    if (kicked) {
      return;
    }
    originPreview.set(BONG_ORIGIN_X, BONG_ORIGIN_Y);
    previewPath = [];
    calculateV();

    simulation();
  });

  inputHandler.addEventListener(InputEvent.TouchEnd, () => {
    if(gameStop) return
    if (kicked) {
      return;
    }
    calculateV();
    kicked = true;
    isPreview = false;
  });

  const handleResetBall = () => {
    kicked = false;
    finished = false;
    bongWidth = BONG_ORIGIN_WIDTH;
    bongHeight = (bongWidth * bong.height) / bong.width;
    bongVY = 0;
    bongVY = 0;
    originBong.set(BONG_ORIGIN_X, BONG_ORIGIN_Y);
    flyTime = 0;
    handleResetTarget();
  };

  const simulation = () => {
    const timeDash = TIME_STEP;
    let space = 3;
    accumulate = 0;
    for (let time = 0; time < 1; time += timeDash) {
      bongVY -= forceY * pullForce * timeDash;
      originPreview.set(originPreview.x + bongVX, originPreview.y + bongVY);
      if (space <= 0) {
        previewPath.push({ x: originPreview.x, y: originPreview.y });
        space = 3;
      }
      space--;
    }
    bongVY = 0;
  };

  const pullForce = 0.049;
  let rotation = 0;
  let finished = false;
  let flyTime = 0;
  let accumulate = 0;


  // -- Time step
  let minutes = ORIGIN_M;
  let seconds = ORIGIN_S;
  let counterTime:NodeJS.Timeout
  
  const handleSetTime = ()=>{
    counterTime = setInterval(() => {
      if(gameStop === false){
        if (seconds === 0 && minutes !== 0) {
          minutes = minutes - 1;
          seconds = 59;
        } else {
          seconds--;
        }
      }
      }, 1000);
      setTimeout(() => {
        clearInterval(counterTime);
        if(gameStop) return
        onGameEnded(typeGameEnd.timeEnd)
      }, 1000*60*minutes +1000*seconds);
  }
      // -- Time step

  handleSetTime()

  const loop = createGameLoop((delta: number) => {
    // on ball fly (kicked)
    if (kicked && !finished) {
      accumulate += delta;
      while (accumulate >= TIME_STEP) {
        accumulate -= TIME_STEP;

        flyTime += TIME_STEP;

        bongVY -= forceY * pullForce * TIME_STEP;
        bongWidth -= TIME_STEP * 200;
        bongHeight = (bongWidth * bong.height) / bong.width;
        rotation += TIME_STEP * 5;
        // handle move ball
        originBong.set(originBong.x + bongVX, originBong.y + bongVY);
      }
    }
    // stop
    if (flyTime >= MAX_FLY_TIME && !finished && !isPreview) {
      bongVX = 0;
      bongVY = 0;
      finished = true;

      if (originBong.x < (originTarget.x + targetWidth) && originBong.x > originTarget.x && originBong.y < (originTarget.y + targetHeight) && originBong.y > originTarget.y) {
        scoreBall++;
      }
      if (scoreBall === 3) {
        onGameEnded(typeGameEnd.winner)
        return
      }
      setTimeout(() => {
        handleResetBall();
      }, 2000);
    }

    gl.clear(gl.COLOR_BUFFER_BIT);
    batch.setProjection(camera.combined);
    batch.begin();
    batch.draw(bg, 0, 0, WORLD_WIDTH, WORLD_HEIGHT);
    target.draw(batch, originTarget.x, originTarget.y, targetWidth, targetHeight);

    bong.draw(
      batch,
      originBong.x - bongWidth / 2,
      originBong.y - bongHeight / 2,
      bongWidth,
      bongHeight,
      bongWidth / 2,
      bongHeight / 2,
      rotation
    );

    score.draw(batch, scoreOrigin.x, scoreOrigin.y, scoreWidth, scoreHeight);

    text.data.setXYScale(0.3);
    batch.setColor(Color.fromString('#162B75'));
    text.draw(
      batch,
      `${minutes < 10 ? '0' : ''}${minutes}:${seconds < 10 ? '0' : ''}${seconds}`,
      295,
      210,
      WORLD_WIDTH * 0.8
    );
    batch.setColor(Color.WHITE);
    text.data.setXYScale(1);

    for (let i = 0; i <= scoreBall - 1; i++) {
      bong.draw(batch, 170 + i * 110, 100, 90, 90);
    }
    batch.end();

    // preview path
    shapeRenderer.setProjection(camera.combined);
    shapeRenderer.begin();
    if (isPreview)
      previewPath.forEach((preview) => {
        shapeRenderer.circle(true, preview.x, preview.y, 5, Color.WHITE);
      });
    shapeRenderer.end();
  });

  return {
    start: () => {
      reset();
      loop.start();
    },
    dispose() {
      loop.stop();
      stage.cleanup();
      inputHandler.cleanup();
      batch.dispose();
      assetManager.disposeAll();
      shapeRenderer.dispose();
    },
    reset() {
      reset();
    },
  };
};
