import {
  AssetManager,
  Color,
  InputEvent,
  PolygonBatch,
  ViewportInputHandler,
  createGameLoop,
  createStage,
  createViewport,
  Align,
} from 'gdxts';

import { ASSETS } from '../../utils/assetUtils';
import { WORLD_WIDTH, WORLD_HEIGHT, GAME_EVENTS } from '../../utils/constants';
import eventEmitter from '../../utils/eventEmitter';

import { pointInRect } from '../../utils/math';

export type GameEndedData = {
  config: any;
  type: 'timeOut' | 'win' | 'lose';
};

export const init = async () => {
  const stage = createStage();
  const canvas = stage.getCanvas();
  const viewport = createViewport(canvas, WORLD_WIDTH, WORLD_HEIGHT, { crop: false });
  const gl = viewport.getContext();

  const assetManager = new AssetManager(gl);
  assetManager.loadTexture(ASSETS.BG5, 'bg');
  assetManager.loadTexture(ASSETS.LAYER, 'layer');
  assetManager.loadAtlas('./assets/atlas/may-bay-virus.atlas', 'may-bay-virus');
  assetManager.loadFont('./assets/font/OneShinhan.fnt', 'OneShinhan', true);

  const camera = viewport.getCamera();
  camera.setYDown(true);

  await assetManager.finishLoading();

  const batch = new PolygonBatch(gl);
  batch.setYDown(true);

  const bg = assetManager.getTexture('bg')!;

  const altas = assetManager.getAtlas('may-bay-virus')!;
  const plane = altas.findRegion('plane')!;
  const thongKe = altas.findRegion('thongKe')!;
  const dan = altas.findRegion('dan')!;
  const virus1 = altas.findRegion('virus1')!;
  const virus2 = altas.findRegion('virus2')!;
  const virus3 = altas.findRegion('virus3')!;
  const diem = altas.findRegion('diem')!;
  const text = assetManager.getFont('OneShinhan')!;

  const arrVirus = [virus1, virus2, virus3];

  //const
  let countdown = 60;
  const planeWidth = WORLD_WIDTH * 0.2675;

  const planeHeight = (planeWidth * plane.originalHeight) / plane.originalWidth;
  let planeX = WORLD_WIDTH / 2 - planeWidth / 2;
  const planeY = WORLD_HEIGHT * 0.89 - planeHeight;

  let plX = 0;

  let countVirut = 0;

  let gameOver = false;

  const bulletWidth = dan.originalWidth * 0.4
  const bulletHeight = dan.originalHeight * 0.4


  setInterval(function () {
    // Giảm thời gian đếm ngược
    if (countdown > 0 && !gameOver) {
      countdown--;
    } else {
      if (!gameOver) {
        onGameEnded('timeOut');
      }
    }
  }, 1000);

  const onGameEnded = (selected: string) => {
    gameOver = true;
    eventEmitter.emit(GAME_EVENTS.GAME_ENDED, {
      type: selected,
    });
  };

  const reset = () => {
    countVirut = 0;
    countdown = 60;
    planeX = WORLD_WIDTH / 2 - planeWidth / 2;
    arrayVirus.length = 0
    arrBullet.length = 0
    gameOver = false;
  };

  const inputHandler = new ViewportInputHandler(viewport);

  gl.clearColor(0, 0, 0, 0);

  let arrBullet: any[] = [];
  let arrayVirus: any[] = [];

  const bulletFireInterval = 0.3;
  let bulletFireElapsedTime = 0;
  const bulletSpeed = 2000;
  const minY = 0 - bulletHeight * 2;

  const virusInterval = 0.5;
  let virusElapsed = 0;
  const virusSpeed = 400;
  const winVirus = 30;

  inputHandler.addEventListener(InputEvent.TouchMove, () => {
    const { x } = inputHandler.getTouchedWorldCoord();
    if (x - planeWidth / 2 > 0 && x < WORLD_WIDTH - planeWidth / 2 && !gameOver) {
      planeX = x - planeWidth / 2;
    }
  });

  const loop = createGameLoop((delta: number) => {
    if (gameOver) return;
    if (countdown > 0 && countVirut > winVirus) {
      onGameEnded('win');
    }

    // Fire
    if (bulletFireElapsedTime >= bulletFireInterval) {
      arrBullet.push({ x: planeX + planeWidth / 2 - bulletWidth / 2, y: planeY, active: true })
      bulletFireElapsedTime = 0;
    }
    bulletFireElapsedTime += delta;

    if (virusElapsed >= virusInterval){
      const random = Math.random()*(0.8-0.2)+0.2
      const randomVirus = Math.floor(Math.random()*2) 
      arrayVirus.push({ x: WORLD_WIDTH * random, y:-arrVirus[randomVirus].originalHeight* 0.4, active: true, virus: randomVirus }) // 0 - virus1.originalHeight* 0.4
      virusElapsed = 0
    }
    virusElapsed += delta;

    for (const element of arrayVirus) {
      element.y += virusSpeed * delta;
      if (element.y > WORLD_HEIGHT) {
        element.active = false
      }
    }



    for (let index = 0; index < arrBullet.length; index++) {
      let bullet = arrBullet[index];
      bullet.y -= bulletSpeed * delta;
      if (bullet.y < minY) {
        bullet.active = false;
      }

      for (let child of arrayVirus) {
        if (
          !gameOver
          && child.active === true
          && arrBullet[index].active === true
          && (pointInRect({ x: bullet.x, y: bullet.y }, child.x, child.y, arrVirus[child.virus].originalWidth * 0.4, arrVirus[child.virus].originalHeight * 0.4)
          || pointInRect({ x: bullet.x + bulletWidth, y: bullet.y }, child.x, child.y, arrVirus[child.virus].originalWidth * 0.4, arrVirus[child.virus].originalHeight * 0.4)
          )
        ) {
          countVirut+=1
          child.active = false
        }
      }

    }

    //check virus vs may bay
    for( let element of arrayVirus) {
      if (
        !gameOver
        && element.active === true
        && (pointInRect({ x: element.x-5 , y: element.y + arrVirus[element.virus].originalWidth * 0.4}, planeX+ planeWidth/2 -planeWidth/5/2+10,planeY,planeWidth/5-20,planeHeight)
        || pointInRect({ x: element.x + arrVirus[element.virus].originalWidth * 0.4, y: element.y + arrVirus[element.virus].originalWidth * 0.4}, planeX+ planeWidth/2 -planeWidth/5/2+10,planeY,planeWidth/5-20,planeHeight)
        ||pointInRect({ x: element.x-5 , y: element.y + arrVirus[element.virus].originalWidth * 0.4}, planeX +15,planeY+planeHeight/3,planeWidth-30,planeHeight)
        || pointInRect({ x: element.x + arrVirus[element.virus].originalWidth * 0.4, y: element.y + arrVirus[element.virus].originalWidth * 0.4}, planeX +15,planeY+planeHeight/3,planeWidth-30,planeHeight)
        || element.y + arrVirus[element.virus].originalHeight * 0.4 >= WORLD_HEIGHT
        )
      ){
          onGameEnded("lose")
      }
    }

    arrBullet = arrBullet.filter((item) => item.active);
    arrayVirus = arrayVirus.filter((item) => item.active)

    gl.clear(gl.COLOR_BUFFER_BIT);
    batch.setProjection(camera.combined);
    batch.begin();
    batch.draw(bg, 0, 0, WORLD_WIDTH, WORLD_HEIGHT);

    for (const element of arrayVirus) {
      arrVirus[element.virus].draw(batch, element.x, element.y, arrVirus[element.virus].originalWidth * 0.4, arrVirus[element.virus].originalHeight * 0.4)
    }


    for (let i = 0; i < arrBullet.length; i++) {
      dan.draw(batch, arrBullet[i].x, arrBullet[i].y, bulletWidth, bulletHeight);
    }

    thongKe.draw(
      batch,
      10,
      WORLD_HEIGHT - thongKe.originalHeight * 0.4,
      thongKe.originalWidth * 0.4,
      thongKe.originalHeight * 0.4
    );
    for (let i = 0; i < Math.floor(countVirut/2); i++) {
      diem.draw(
        batch,
        93 + i * 10,
        WORLD_HEIGHT - (thongKe.originalHeight * 0.4) / 2 - (diem.originalHeight * 0.4) / 2 + 1,
        diem.originalWidth * 0.4,
        diem.originalHeight * 0.4
      );
    }

    plane.draw(batch, planeX, planeY, planeWidth, planeHeight);
    text.data.setXYScale(0.4);
    batch.setColor(Color.fromString('#FDD8BB'));
    text.draw(
      batch,
      countdown.toString(),
      30,
      WORLD_HEIGHT - (thongKe.originalHeight * 0.4) / 2 - 10,
      40,
      Align.center
    );
    batch.setColor(Color.WHITE);
    text.data.setXYScale(1);

    batch.end();
    plX += 2;
  });

  return {
    start: () => {
      reset();
      loop.start();
    },
    dispose() {
      loop.stop();
      stage.cleanup();
      inputHandler.cleanup();
      batch.dispose();
      assetManager.disposeAll();
    },
    reset() {
      reset();
    },
  };
};
