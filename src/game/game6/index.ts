import {
  AssetManager,
  Color,
  InputEvent,
  PolygonBatch,
  ViewportInputHandler,
  createGameLoop,
  createStage,
  createViewport,
} from 'gdxts';

import { GAME_EVENTS, WORLD_HEIGHT, WORLD_WIDTH } from '../../utils/constants';
import eventEmitter from '../../utils/eventEmitter';

const FLIP_UP_DURATION = 0.2;

export type GameEndedData = {
  config: any;
  type: string;
};

const arrThuong = ['voucher', 'tien', 'tam'];

interface Transition {
  type: 'flip-up' | 'flip-down' | 'fade';
  elapse: number;
  duration: number;
}

export const init = async () => {
  let luot = 1;

  const stage = createStage();
  const canvas = stage.getCanvas();
  const viewport = createViewport(canvas, WORLD_WIDTH, WORLD_HEIGHT, { crop: false });
  const gl = viewport.getContext();

  const COLS = 3;
  const ROWS = 3;
  const START_X = 50;
  const START_Y = 500;

  const GAP = WORLD_WIDTH / 18;
  const CELL_WIDTH = (WORLD_WIDTH - 1.4 * START_X - GAP * (COLS + 1)) / COLS;
  const CELL_HEIGHT = (CELL_WIDTH / 329) * 330;

  const assetManager = new AssetManager(gl);
  assetManager.loadTexture('./assets/bg6.png', 'bg');
  assetManager.loadAtlas('./assets/atlas/mo_hom.atlas', 'mo_hom');
  assetManager.loadAtlas('./assets/atlas/mo_qua.atlas', 'mo_qua');
  assetManager.loadFont('./assets/font/inters.fnt', 'inters', true);

  const COLORS = {
    WHITE: Color.WHITE,
    BLUE: Color.BLUE,
    BLACK: new Color(0, 0, 0, 1),
    GRAY: new Color(0.5, 0.5, 0.5, 1),
    DEFAULT: Color.WHITE,
    RED: Color.RED,
  };

  const camera = viewport.getCamera();
  camera.setYDown(true);

  await assetManager.finishLoading();

  const batch = new PolygonBatch(gl);
  batch.setYDown(true);

  const bg = assetManager.getTexture('bg')!;
  const altas = assetManager.getAtlas('mo_qua')!;
  const backRegion = altas.findRegion('box', -1)!;
  const bongRegion = altas.findRegion('bg_game')!;
  const opened = altas.findRegion('opened')!;

  const cells: {
    type: number;
    flipped: boolean;
    hidden: boolean;
    transition?: Transition;
  }[] = [];

  const onGameEnded = () => {
    eventEmitter.emit(GAME_EVENTS.GAME_ENDED, {
      type: thuong,
    });
  };

  const reset = () => {
    flipped = 0;
    cells.length = 0;
    for (let i = 0; i < COLS * ROWS; i++) {
      cells.push({
        type: i % ((COLS * ROWS) / 2),
        flipped: false,
        hidden: false,
      });
    }
  };

  cells.length = 0;
  for (let i = 0; i < COLS * ROWS; i++) {
    cells.push({
      type: i % ((COLS * ROWS) / 2),
      flipped: false,
      hidden: false,
    });
  }

  const inputHandler = new ViewportInputHandler(viewport);

  let flipped = 0;
  let thuong:any;

  inputHandler.addEventListener(InputEvent.TouchStart, () => {
    if (flipped >= 1) {
      return;
    }
    const { x, y } = inputHandler.getTouchedWorldCoord();

    if (luot === 7) {
      thuong = arrThuong[2];
    } else {
      if (luot % 2 == 0) {
        thuong = arrThuong[1];
      } else {
        thuong = arrThuong[0];
      }
    }

    const cellX = Math.floor((x - START_X) / (CELL_WIDTH + GAP));
    const cellY = Math.floor((y - START_Y) / (CELL_HEIGHT + GAP));
    const cellPos = cellY * COLS + cellX;
    if (cellX < 0 || cellX >= COLS || cellY < 0 || cellY >= ROWS) {
      return;
    }
    const cell = cells[cellPos];
    if (cell.flipped || cell.hidden) {
      return;
    }

    cell.flipped = true;
    cell.transition = {
      type: 'flip-up',
      duration: FLIP_UP_DURATION,
      elapse: 0,
    };
    flipped++;
    luot++;
    setTimeout(() => {
      onGameEnded();
    }, 500);
  });

  gl.clearColor(0, 0, 0, 0);

  const loop = createGameLoop((delta: number) => {
    for (const cell of cells) {
      if (!cell.transition) {
        continue;
      }
      cell.transition.elapse += delta;
      if (cell.transition.elapse >= cell.transition.duration) {
        cell.transition = undefined;
      }
    }

    gl.clear(gl.COLOR_BUFFER_BIT);
    batch.setProjection(camera.combined);
    batch.begin();
    batch.draw(bg, 0, 0, WORLD_WIDTH, WORLD_HEIGHT);

    const i = 630;
    bongRegion.draw(batch, 5, 450, i, i);

    for (let i = 0; i < cells.length; i++) {
      const x = i % COLS;
      const y = Math.floor(i / COLS);

      const cell = cells[i];

      if (cell.hidden) {
        continue;
      }

      let region = cell.flipped ? opened : backRegion;
      let scaleX = 1;
      let scaleY = 1;

      const transition = cell.transition;

      if (transition && transition.type === 'flip-up') {
        if (transition.elapse < transition.duration / 2) {
          region = backRegion;
          scaleX = 1 - transition.elapse / (transition.duration / 2);
        } else {
          region = opened;
          scaleX = (transition.elapse - transition.duration / 2) / (transition.duration / 2);
        }
      }

      if (transition && transition.type === 'flip-down') {
        if (transition.elapse < transition.duration / 2) {
          region = opened;
          scaleX = 1 - transition.elapse / (transition.duration / 2);
        } else {
          region = backRegion;
          scaleX = (transition.elapse - transition.duration / 2) / (transition.duration / 2);
        }
      }

      if (transition && transition.type === 'fade') {
        scaleX = 1 - transition.elapse / transition.duration;
        scaleY = scaleX;
      }

      batch.setColor(COLORS.WHITE);

      region.draw(
        batch,
        START_X + x * (CELL_WIDTH + GAP) + 12,
        START_Y + y * (CELL_HEIGHT + GAP) + 2,
        CELL_WIDTH * 1.175,
        CELL_HEIGHT * 1.175,
        CELL_WIDTH / 2,
        CELL_HEIGHT / 2,
        0,
        scaleX,
        scaleY
      );
    }

    batch.end();
  });

  return {
    start: () => {
      reset();
      loop.start();
      luot = 1;
    },
    dispose() {
      loop.stop();
      stage.cleanup();
      inputHandler.cleanup();
      batch.dispose();
      assetManager.disposeAll();
    },
    reset() {
      reset();
    },
  };
};
