import {
  AssetManager,
  createGameLoop,
  createStage,
  createViewport,
  InputEvent,
  pointInRect,
  PolygonBatch,
  TextureAtlas,
  TextureRegion,
  Vector2,
  ViewportInputHandler,
} from 'gdxts';

import { EMITTER_EVENTS } from '../../constants';
import { VoucherType } from '../../typing';
import { Timer } from '../../utils/Timer';
import { WORLD_WIDTH, WORLD_HEIGHT, GAME_EVENTS } from '../../utils/constants';
import { bezier } from '../../utils/customEasing';
import eventEmitter from '../../utils/eventEmitter';
import { lerp } from '../../utils/math';
import { MusicId, SoundFxId, soundUtils } from '../../utils/soundUtils';
import { getMusicSetting } from '../../utils/storage';

const SPINNING_TIME = 3;

export type GameEndedData = {
  config: any;
  type: string;
};

export const init = async () => {
  // let numberOfTickets = 999;
  const stage = createStage();
  const canvas = stage.getCanvas();

  // TODO convert all to 414 621
  const viewport = createViewport(canvas, WORLD_WIDTH, WORLD_HEIGHT, {
    autoUpdate: true,
    crop: true,
    pixelRatio: Math.min(2, window.devicePixelRatio),
    contextOption: {
      antialias: false,
    },
  });
  const camera = viewport.getCamera();
  camera.setYDown(true);
  const gl = viewport.getContext();
  const batch = new PolygonBatch(gl);
  batch.setYDown(true);

  const assetManager = new AssetManager(gl);
  assetManager.loadTexture('./assets/bg1.png', 'bg');
  assetManager.loadAtlas('assets/atlas/spine.atlas', 'spine_atlas');
  await assetManager.finishLoading();

  const spine_atlas = assetManager.getAtlas('spine_atlas') as TextureAtlas;
  const outerTexture = spine_atlas.findRegion('outer') as TextureRegion;
  const innerTexture = spine_atlas.findRegion('inner') as TextureRegion;
  const arrowTexture = spine_atlas.findRegion('arrow') as TextureRegion;
  const shadowTexture = spine_atlas.findRegion('shadow') as TextureRegion;
  const buttonSpineTexture = spine_atlas.findRegion('button') as TextureRegion;
  const centerTexture = spine_atlas.findRegion('center') as TextureRegion;
  const bg = assetManager.getTexture('bg')!;

  const outerHeight = WORLD_HEIGHT * 0.45;
  const outerWidth = (outerHeight * outerTexture.originalWidth) / outerTexture.originalHeight;
  const centerOffsetY = outerHeight * 0.485;
  const centerOffsetX = outerWidth * 0.5;
  const centerHeight = WORLD_HEIGHT * 0.1;
  const centerWidth = (centerHeight * centerTexture.originalWidth) / centerTexture.originalHeight;
  const innerHeight = outerHeight * 0.83;
  const innerWidth = (innerHeight * innerTexture.originalWidth) / innerTexture.originalHeight;
  const arrowHeight = WORLD_HEIGHT * 0.076;
  const arrowWidth = (arrowHeight * arrowTexture.originalWidth) / arrowTexture.originalHeight;
  const arrowOffsetY = outerHeight * 0.048;
  const shadowHeight = WORLD_HEIGHT * 0.183;
  const shadowWidth = (shadowHeight * shadowTexture.originalWidth) / shadowTexture.originalHeight;
  const shadowOffsetY = outerHeight * 0.275;

  // const buttonHeight = WORLD_HEIGHT * 0.115;
  // const buttonWidth = (buttonHeight * buttonTexture.originalWidth) / buttonTexture.originalHeight;
  // const buttonX = WORLD_WIDTH / 2 - buttonWidth / 2;
  // const buttonY = outerHeight * 1.2;

  const buttonSpineHeight = WORLD_HEIGHT * 0.055;
  const buttonSpineWidth = (buttonSpineHeight * buttonSpineTexture.originalWidth) / buttonSpineTexture.originalHeight;
  const buttonSpineX = WORLD_WIDTH / 2 - buttonSpineWidth / 2;
  const buttonSpineY = WORLD_HEIGHT * 0.85 - buttonSpineHeight / 2;

  // const lightOnHeight = WORLD_HEIGHT * 0.03;
  // const lightOnWidth =
  //   (lightOnHeight * lightOnTexture.originalWidth) / lightOnTexture.originalHeight;

  const outerPosition: Vector2 = new Vector2(0, 0);
  const centerPosition: Vector2 = new Vector2(0, 0);
  const arrowPosition: Vector2 = new Vector2(0, 0);
  const shadowPosition: Vector2 = new Vector2(0, 0);
  // const decoPosition: Vector2 = new Vector2(0, 0);

  outerPosition.x = WORLD_WIDTH / 2 - outerWidth / 2;
  outerPosition.y = WORLD_HEIGHT / 2 - WORLD_HEIGHT * 0.28;
  centerPosition.x = outerPosition.x + centerOffsetX;
  centerPosition.y = outerPosition.y + centerOffsetY;

  const spinTimer = new Timer();
  const spinDelayTimer = new Timer();
  const buttonSlideDelayTimer = new Timer();

  let isSpinning = false;
  const reward: { type: string } = {
    type: VoucherType.CAT,
  };

  const OUTER_SIZE = WORLD_HEIGHT * 0.55;
  const lights: { time: 0; on: boolean; pos: Vector2 }[] = [];
  const radius = OUTER_SIZE / 2;
  const offsets: number[][] = [
    [0, outerHeight * 0.05],
    [outerHeight * 0.018, outerHeight * 0.037],
    [outerHeight * 0.024, outerHeight * 0.029],
    [outerHeight * 0.026, outerHeight * 0.017],
    [outerHeight * 0.024, outerHeight * 0.008],
    [0, 0],
    [outerHeight * 0.004, outerHeight * -0.002],
    [outerHeight * -0.007, outerHeight * 0.001],
    [outerHeight * -0.013, outerHeight * 0.013],
    [outerHeight * -0.014, outerHeight * 0.023],
    [0, outerHeight * 0.03],
    [outerHeight * -0.003, outerHeight * 0.038],
  ];

  for (let i = 0; i < 12; i++) {
    const angle = (-(i * 360) / 12) * (Math.PI / 180) + 45;
    const offsetX = offsets?.[i]?.[0] || 0;
    const offsetY = offsets?.[i]?.[1] || 0;
    const x = radius * Math.cos(angle) + WORLD_HEIGHT * 0.328 + offsetX;
    const y = radius * Math.sin(angle) + OUTER_SIZE * 0.605 + offsetY;
    lights.push({
      pos: new Vector2(x, y),
      on: false,
      time: 0,
    });
  }

  const REWARD_MAPS: { [key in VoucherType]: number } = {
    [VoucherType.CAT]: 8,
    [VoucherType.THUA]: 7,
    [VoucherType.THEP]: 6,
    [VoucherType.THE_NAP]: 5,
    [VoucherType.GACH]: 4,
    [VoucherType.VOUCHER]: 3,
    [VoucherType.TIEN]: 2,
    [VoucherType.NUOC]: 1,
  };

  const getTargetAngle = (type: VoucherType): number => {
    return ((2 * Math.PI) / 8) * REWARD_MAPS[type];
  };

  let isFreeSpinning = false;
  let targetAngle = 0;
  let sourceAngle = 0;
  let angle = 0;

  const handleStartSpin = () => {
    if (isSpinning) return;
    isSpinning = true;
    isFreeSpinning = true;

    soundUtils.play(SoundFxId.ROLL);
    soundUtils.loadedSounds[MusicId.THEME]?.mute(true);
    soundUtils.stop(MusicId.THEME);
    soundUtils.play(MusicId.SPIN_THEME);
  };

  const handleSpinResult = (voucherType: VoucherType) => {
    if (!isSpinning) return;

    reward.type = voucherType;
    isFreeSpinning = false;

    // Calc target angle
    const centerAngle = getTargetAngle(voucherType) + Math.PI * 2 * 7;
    targetAngle = centerAngle - 0.39;
    sourceAngle = angle;

    spinTimer.value = SPINNING_TIME * 1.05;
    spinTimer.onComplete = () => {
      soundUtils.stop(MusicId.SPIN_THEME);
      const isMusicOn = getMusicSetting();
      if (isMusicOn) {
        soundUtils.loadedSounds[MusicId.THEME]?.mute(false);
      }
      soundUtils.play(MusicId.THEME);

      isSpinning = false;
      eventEmitter.emit(EMITTER_EVENTS.SHOW_REWARD, voucherType); // show vỏcher
    };
  };

  const handleSpinError = () => {
    // Switch music
    soundUtils.stop(MusicId.SPIN_THEME);
    const isMusicOn = getMusicSetting();
    if (isMusicOn) {
      soundUtils.loadedSounds[MusicId.THEME]?.mute(false);
    }
    soundUtils.play(MusicId.THEME);

    // Stop spinning
    isSpinning = false;
    isFreeSpinning = false;
  };

  const handleUpdateNumOfTickets = (_amount: number) => {
    // numberOfTickets = amount;
  };

  eventEmitter.addListener(EMITTER_EVENTS.START_SPIN, handleStartSpin);
  eventEmitter.addListener(EMITTER_EVENTS.SPIN_RESULT, handleSpinResult);
  eventEmitter.addListener(EMITTER_EVENTS.SPIN_ERROR, handleSpinError);
  eventEmitter.addListener(EMITTER_EVENTS.UPDATE_NUMBER_OF_TICKETS, handleUpdateNumOfTickets);

  const processtimer = (delta: number) => {
    spinTimer.update(delta);
    spinDelayTimer.update(delta);
    buttonSlideDelayTimer.update(delta);
  };

  let spinnerIdleTimer = 0;
  let spinnerSpinTimer = 0;
  let spinnerSpinnerTimer = 0;

  const SWITCH_PATTERN_DELAY = 0.4;
  let currentPatternIndex = 0;
  const idlePatterns = ['even', 'odd', 'none'];

  let currentSpinLightIndex = 5;
  const SWITCH_LIGHT_DELAY = 0.06;

  let speed = 0;
  const speedAcc = WORLD_HEIGHT * 0.07;
  const maxSpeed = WORLD_HEIGHT * 0.03;

  const easingFunction = bezier(0.47, 0.87, 0.6, 1.04);

  const processSpinner = (delta: number) => {
    if (isSpinning) {
      spinnerIdleTimer = 0;
      spinnerSpinTimer += delta;
      if (spinnerSpinTimer > SWITCH_LIGHT_DELAY) {
        spinnerSpinTimer = 0;
        currentSpinLightIndex = currentSpinLightIndex === 0 ? lights.length - 1 : currentSpinLightIndex - 1;
      }

      if (!isFreeSpinning) {
        spinnerSpinnerTimer += delta;
        angle = lerp(sourceAngle, targetAngle, easingFunction(Math.min(1, spinnerSpinnerTimer / SPINNING_TIME)));
      } else {
        speed += speedAcc * delta;
        angle = Math.min(angle + maxSpeed * delta, angle + speed * delta);
      }
    } else {
      if (angle > Math.PI * 2) {
        angle = angle % (Math.PI * 2);
      }
      speed = 0;
      spinnerSpinTimer = 0;
      spinnerSpinnerTimer = 0;
      spinnerIdleTimer += delta;
      currentSpinLightIndex = 5;
      if (spinnerIdleTimer > SWITCH_PATTERN_DELAY) {
        spinnerIdleTimer = 0;
        currentPatternIndex = currentPatternIndex === idlePatterns.length - 1 ? 0 : currentPatternIndex + 1;
      }
    }

    if (!isSpinning) {
      const pattern = idlePatterns[currentPatternIndex];
      for (let i = 0; i < lights.length; i++) {
        const light = lights[i];
        if (pattern === 'even' && i % 2 === 0) {
          light.on = true;
        } else if (pattern === 'odd' && i % 2 === 1) {
          light.on = true;
        } else {
          light.on = false;
        }
      }
    }

    if (isSpinning) {
      for (let i = 0; i < lights.length; i++) {
        const light = lights[i];
        if (i === currentSpinLightIndex) {
          light.on = true;
        } else {
          light.on = false;
        }
      }
    }
  };

  const onGameEnded = () => {
    eventEmitter.emit(GAME_EVENTS.GAME_ENDED);
  };

  const inputHandler = new ViewportInputHandler(viewport);
  //logic
  inputHandler.addEventListener(InputEvent.TouchStart, () => {
    const { x, y } = inputHandler.getTouchedWorldCoord();
    if (pointInRect(x, y, buttonSpineX, buttonSpineY, buttonSpineWidth, buttonSpineHeight)) {
      //set event
      if (!isSpinning) {
        onGameEnded();
      }
    }
  });

  const gameLoop = createGameLoop((delta: number) => {
    // gl.clear(gl.COLOR_BUFFER_BIT);

    processSpinner(delta);
    // Timer
    processtimer(delta);

    // Render
    batch.setProjection(camera.combined);
    batch.begin();

    batch.draw(bg, 0, 0, WORLD_WIDTH, WORLD_HEIGHT);
    // Outer
    outerTexture.draw(batch, outerPosition.x, outerPosition.y, outerWidth, outerHeight);

    // Inner : vong quay chinh
    innerTexture.draw(
      batch,
      centerPosition.x - innerWidth / 2 + outerHeight * 0.0015,
      centerPosition.y - innerHeight / 2 - outerHeight * 0.0015,
      innerWidth,
      innerHeight,
      innerWidth / 2,
      innerHeight / 2,
      angle
    );

    //shadow outer
    shadowPosition.x = centerPosition.x;
    shadowPosition.y = outerPosition.y + shadowOffsetY;
    shadowTexture.draw(
      batch,
      shadowPosition.x - shadowWidth / 2,
      shadowPosition.y - shadowHeight / 2,
      shadowWidth,
      shadowHeight
    );

    // Arrow
    arrowPosition.x = centerPosition.x;
    arrowPosition.y = outerPosition.y + arrowOffsetY;
    arrowTexture.draw(
      batch,
      arrowPosition.x - arrowWidth / 2,
      arrowPosition.y - arrowHeight / 2,
      arrowWidth,
      arrowHeight
    );

    // Center
    centerTexture.draw(
      batch,
      centerPosition.x - centerWidth / 2 + WORLD_HEIGHT * 0.001,
      centerPosition.y - centerHeight / 2 + WORLD_HEIGHT * 0.005,
      centerWidth,
      centerHeight
    );
    // Button
    buttonSpineTexture.draw(batch, buttonSpineX, buttonSpineY, buttonSpineWidth, buttonSpineHeight);

    batch.end();
  });

  return {
    dispose: () => {
      stage.cleanup();
      inputHandler.cleanup();
      gameLoop.stop();  
      batch.dispose();
      assetManager.disposeAll();
      eventEmitter.removeListener(EMITTER_EVENTS.UPDATE_NUMBER_OF_TICKETS, handleUpdateNumOfTickets);
      eventEmitter.removeListener(EMITTER_EVENTS.START_SPIN, handleStartSpin);
      eventEmitter.removeListener(EMITTER_EVENTS.SPIN_RESULT, handleSpinResult);
      eventEmitter.removeListener(EMITTER_EVENTS.SPIN_ERROR, handleSpinError);
    },

    start: () => {
      gameLoop.start();
    },
  };
};
