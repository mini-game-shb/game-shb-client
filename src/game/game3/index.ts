import {
  AssetManager,
  InputEvent,
  PolygonBatch,
  ViewportInputHandler,
  createGameLoop,
  createStage,
  createViewport,
} from 'gdxts';

import { ASSETS } from '../../utils/assetUtils';
import { WORLD_WIDTH, WORLD_HEIGHT, GAME_EVENTS } from '../../utils/constants';
import eventEmitter from '../../utils/eventEmitter';
// import { soundUtils } from '../../utils/soundUtils';
// import { getMusicSetting } from '../../utils/storage';

export type GameEndedData = {
  config: any;
  type: string;
};

let luot = 0;

export const init = async () => {
  const stage = createStage();
  const canvas = stage.getCanvas();
  const viewport = createViewport(canvas, WORLD_WIDTH, WORLD_HEIGHT, { crop: true });
  const gl = viewport.getContext();

  const assetManager = new AssetManager(gl);
  assetManager.loadTexture(ASSETS.BG3, 'bg');
  assetManager.loadTexture(ASSETS.LAYER, 'layer');
  assetManager.loadAtlas('./assets/atlas/may_bay.atlas', 'may_bay');
  assetManager.loadFont('./assets/font/inters.fnt', 'inters', true);

  const camera = viewport.getCamera();
  camera.setYDown(true);

  await assetManager.finishLoading();

  const batch = new PolygonBatch(gl);
  batch.setYDown(true);

  const bg = assetManager.getTexture('bg')!;
  const layer = assetManager.getTexture('layer')!;
  const altas = assetManager.getAtlas('may_bay')!;
  const plane = altas.findRegion('plane')!;
  const sao = altas.findRegion('sao')!;
  const chan_sao = altas.findRegion('chan_sao')!;
  const diem_tha = altas.findRegion('diem_tha')!;
  const tower = altas.findRegion('tower')!;
  const clouds = altas.findRegion('clouds')!;
  const button = altas.findRegion('button')!;

  // let isMusicPlaying = getMusicSetting();

  //const
  const tiLeMayBay = 0.4;
  const planeWidth = plane.originalWidth * tiLeMayBay;
  const planeHeight = plane.originalHeight * tiLeMayBay;
  const planeX = WORLD_WIDTH / 2 - planeWidth / 2;
  const planeY = WORLD_HEIGHT * 0.2;

  const cloudsWidth = clouds.originalWidth * 0.4;
  const cloudsHeight = clouds.originalHeight * 0.4;
  const cloudsY = WORLD_HEIGHT * 0.14;

  const layerWidth = 14968 * 0.4;
  const layerHeight = 600 * 0.4;
  const layerY = WORLD_HEIGHT * 0.46;

  const buttonWidth = button.originalWidth * tiLeMayBay;
  const buttonHeight = button.originalHeight * tiLeMayBay;
  const buttonX = WORLD_WIDTH / 2 - buttonWidth / 2;
  const buttonY = WORLD_HEIGHT * 0.83;

  const saoWidth = sao.originalWidth * tiLeMayBay;
  const saoHeight = sao.originalHeight * tiLeMayBay;
  const saoX = WORLD_WIDTH / 2 - saoWidth / 2;
  let saoY = planeY + planeHeight - saoHeight;

  const chanSaoWidth = chan_sao.originalWidth * tiLeMayBay;
  const chanSaoHeight = chan_sao.originalHeight * tiLeMayBay;
  const chanSaoX = WORLD_WIDTH / 2 - chanSaoWidth / 2 - 2;
  let chanSaoY = saoY - chanSaoHeight + 10;

  const diemThaY = WORLD_HEIGHT * 0.8 - diem_tha.originalHeight * 0.4;

  let plX = 0;

  let toa1 = 0;
  let toa2 = 0;
  let toa3 = 0;

  let selected1 = false;
  let selected2 = false;
  let selected3 = false;

  const onGameEnded = (selected: string) => {
    if (selected1 && selected2 && selected3) {
      luot++;
    }
    eventEmitter.emit(GAME_EVENTS.GAME_ENDED, {
      type: selected,
    });
  };

  const reset = () => {
    plX = 0;
    selected1 = false;
    selected2 = false;
    selected3 = false;
  };

  const inputHandler = new ViewportInputHandler(viewport);

  let touched = false;

  inputHandler.addEventListener(InputEvent.TouchStart, () => {
    const { x, y } = inputHandler.getTouchedWorldCoord();

    if (x > buttonX && x < buttonX + buttonWidth && y > buttonY && y < buttonY + buttonHeight) {
      if(luot >2){
        onGameEnded("hetLuot")
      }else{
        touched = true

      }
    }
  });

  


  gl.clearColor(0, 0, 0, 0);

  const speed = 4

  const loop = createGameLoop((delta:number) => {
    const scaleFps = 0.0165/delta;
    if (touched) {
      saoY += speed/scaleFps;
      chanSaoY += speed/scaleFps;
    }

    if (saoY + saoHeight >= diemThaY + (diem_tha.originalHeight * 0.4) / 2) {
      if (saoX > toa1 && saoX < toa1 + diem_tha.originalWidth * 0.4 - saoWidth && toa1 !== 0) {
        selected1 = true;
        saoY = planeY + planeHeight - saoHeight;
        chanSaoY = saoY - chanSaoHeight + 10;
        onGameEnded('ve1');
      } else {
        saoY = planeY + planeHeight - saoHeight;
        chanSaoY = saoY - chanSaoHeight + 10;
      }

      if (saoX > toa2 && saoX < toa2 + diem_tha.originalWidth * 0.4 - saoWidth && toa2 !== 0) {
        selected2 = true;
        saoY = planeY + planeHeight - saoHeight;
        chanSaoY = saoY - chanSaoHeight + 10;
        onGameEnded('ve2');
      } else {
        saoY = planeY + planeHeight - saoHeight;
        chanSaoY = saoY - chanSaoHeight + 10;
      }

      if (saoX > toa3 && saoX < toa3 + diem_tha.originalWidth * 0.4 - saoWidth && toa3 !== 0) {
        selected3 = true;
        saoY = planeY + planeHeight - saoHeight;
        chanSaoY = saoY - chanSaoHeight + 10;
        onGameEnded('ve3');
      } else {
        saoY = planeY + planeHeight - saoHeight;
        chanSaoY = saoY - chanSaoHeight + 10;
      }
      touched = false;
    }

    gl.clear(gl.COLOR_BUFFER_BIT);
    batch.setProjection(camera.combined);
    batch.begin();
    batch.draw(bg, 0, 0, WORLD_WIDTH, WORLD_HEIGHT);

    const numRepeats = Math.ceil(WORLD_WIDTH / layerWidth) + 1;
    for (let i = 0; i < numRepeats; i++) {
      const layerX = i * layerWidth - (plX % layerWidth);
      if (layerX <= WORLD_WIDTH) {
        batch.draw(layer, layerX, layerY, layerWidth, layerHeight);
      }
      const diemThaX = i * layerWidth - (plX % layerWidth);

      const diem1 = diemThaX + layerWidth / 6;
      const diem2 = diemThaX + (layerWidth / 6) * 3;
      const diem3 = diemThaX + (layerWidth / 6) * 5;
      if (diem1 <= WORLD_WIDTH && diem1 >= -diem_tha.originalWidth * 0.4) {
        if (i === 0) {
          toa1 = diem1;
        }
        if (selected1) {
          tower.draw(
            batch,
            diem1,
            WORLD_HEIGHT * 0.8 - tower.originalHeight * 0.4,
            tower.originalWidth * 0.4,
            tower.originalHeight * 0.4
          );
        } else {
          diem_tha.draw(batch, diem1, diemThaY, diem_tha.originalWidth * 0.4, diem_tha.originalHeight * 0.4);
        }
      } else {
        if (i === 0) {
          toa1 = 0;
        }
      }

      if (diem2 <= WORLD_WIDTH && diem2 >= -diem_tha.originalWidth * 0.4) {
        if (i === 0) {
          toa2 = diem2;
        }
        if (selected2) {
          tower.draw(
            batch,
            diem2,
            WORLD_HEIGHT * 0.8 - tower.originalHeight * 0.4,
            tower.originalWidth * 0.4,
            tower.originalHeight * 0.4
          );
        } else {
          diem_tha.draw(batch, diem2, diemThaY, diem_tha.originalWidth * 0.4, diem_tha.originalHeight * 0.4);
        }
      } else {
        if (i === 0) {
          toa2 = 0;
        }
      }

      if (diem3 <= WORLD_WIDTH && diem3 >= -diem_tha.originalWidth * 0.4) {
        if (i === 0) {
          toa3 = diem3;
        }
        if (selected3) {
          tower.draw(
            batch,
            diem3,
            WORLD_HEIGHT * 0.8 - tower.originalHeight * 0.4,
            tower.originalWidth * 0.4,
            tower.originalHeight * 0.4
          );
        } else {
          diem_tha.draw(batch, diem3, diemThaY, diem_tha.originalWidth * 0.4, diem_tha.originalHeight * 0.4);
        }
      } else {
        if (i === 0) {
          toa3 = 0;
        }
      }
    }

    const numRepeats2 = Math.ceil(WORLD_WIDTH / cloudsWidth) + 1;
    for (let i = 0; i < numRepeats2; i++) {
      const cloudX = i * cloudsWidth - (plX % cloudsWidth);
      if (cloudX <= WORLD_WIDTH) {
        clouds.draw(batch, cloudX, cloudsY, cloudsWidth, cloudsHeight);
      }
    }

    if (chanSaoY + chanSaoHeight > planeY + planeHeight + 25) {
      chan_sao.draw(batch, chanSaoX, chanSaoY, chanSaoWidth, chanSaoHeight);
    }

    if (saoY + saoHeight > planeY + planeHeight + 20) {
      sao.draw(batch, saoX, saoY, saoWidth, saoHeight);
    }
    plane.draw(batch, planeX, planeY, planeWidth, planeHeight);
    button.draw(batch, buttonX, buttonY, buttonWidth, buttonHeight);
    batch.end();
    plX += 3;
  });

  return {
    start: () => {
      reset();
      loop.start();
    },
    dispose() {
      loop.stop();
      stage.cleanup();
      inputHandler.cleanup();
      batch.dispose();
      assetManager.disposeAll();
    },
    reset() {
      reset();
    },
  };
};
