import {
  AssetManager,
  Color,
  InputEvent,
  PolygonBatch,
  ViewportInputHandler,
  createGameLoop,
  createStage,
  createViewport,
} from 'gdxts';

import { GAME_EVENTS, WORLD_HEIGHT, WORLD_WIDTH } from '../../utils/constants';
import eventEmitter from '../../utils/eventEmitter';

const FLIP_UP_DURATION = 0.2;

export type GameEndedData = {
  config: any;
  type: string;
};

interface Transition {
  type: 'flip-up' | 'flip-down' | 'fade';
  elapse: number;
  duration: number;
}

export const init = async () => {
  const stage = createStage();
  const canvas = stage.getCanvas();
  const viewport = createViewport(canvas, WORLD_WIDTH, WORLD_HEIGHT, { crop: false });
  const gl = viewport.getContext();

  const COLS = 3;
  const ROWS = 2;
  const START_X = 50;
  const START_Y = 613;

  const GAP = WORLD_WIDTH / 18;
  const CELL_WIDTH = (WORLD_WIDTH - 1.4 * START_X - GAP * (COLS + 1)) / COLS;
  const CELL_HEIGHT = (CELL_WIDTH / 329) * 330;

  const assetManager = new AssetManager(gl);
  assetManager.loadTexture('./assets/bg2.png', 'bg');
  assetManager.loadAtlas('./assets/atlas/mo_hom.atlas', 'mo_hom');
  assetManager.loadFont('./assets/font/inters.fnt', 'inters', true);

  const COLORS = {
    WHITE: Color.WHITE,
    BLUE: Color.BLUE,
    BLACK: new Color(0, 0, 0, 1),
    GRAY: new Color(0.5, 0.5, 0.5, 1),
    DEFAULT: Color.WHITE,
    RED: Color.RED,
  };

  const camera = viewport.getCamera();
  camera.setYDown(true);

  await assetManager.finishLoading();

  const batch = new PolygonBatch(gl);
  batch.setYDown(true);

  const bg = assetManager.getTexture('bg')!;
  const altasMoHom = assetManager.getAtlas('mo_hom')!;
  const backRegion = altasMoHom.findRegion('hom', -1)!;
  const ve1 = altasMoHom.findRegion('ve1', -1)!;
  const ve2 = altasMoHom.findRegion('ve2', -1)!;
  const ve3 = altasMoHom.findRegion('ve3', -1)!;
  const ve4 = altasMoHom.findRegion('ve4', -1)!;
  const thua = altasMoHom.findRegion('thua', -1)!;
  const tien = altasMoHom.findRegion('tien', -1)!;
  const voucher = altasMoHom.findRegion('voucher', -1)!;
  const bongRegion = altasMoHom.findRegion('bg_game')!;

  const ve = [ve1, ve2, ve3, ve4];

  let luot = 1;

  const stringVe = ['ve1', 've2', 've3', 've4'];

  const stringThuongKhac = ['tien', 'voucher', 'thua'];
  let tenThuong:any;

  const thuongKhac = [tien, voucher, thua];

  const cells: {
    type: number;
    flipped: boolean;
    hidden: boolean;
    transition?: Transition;
  }[] = [];

  // let matchedPairs = 0;
  // let gameStarted = false;

  const onGameEnded = () => {
    eventEmitter.emit(GAME_EVENTS.GAME_ENDED, {
      type: tenThuong,
    });
  };

  const reset = () => {
    flipped = 0;
    cells.length = 0;
    for (let i = 0; i < COLS * ROWS; i++) {
      cells.push({
        type: i % ((COLS * ROWS) / 2),
        flipped: false,
        hidden: false,
      });
    }
  };

  cells.length = 0;
  for (let i = 0; i < COLS * ROWS; i++) {
    cells.push({
      type: i % ((COLS * ROWS) / 2),
      flipped: false,
      hidden: false,
    });
  }

  const inputHandler = new ViewportInputHandler(viewport);

  let flipped = 0;
  let thuong:any;

  inputHandler.addEventListener(InputEvent.TouchStart, () => {
    if (flipped >= 1) {
      return;
    }
    const { x, y } = inputHandler.getTouchedWorldCoord();

    if (luot % 4 == 0) {
      thuong = ve[luot / 4 - 1];
      tenThuong = stringVe[luot / 4 - 1];
    } else {
      thuong = thuongKhac[(luot % 4) - 1];
      tenThuong = stringThuongKhac[(luot % 4) - 1];
    }

    const cellX = Math.floor((x - START_X) / (CELL_WIDTH + GAP));
    const cellY = Math.floor((y - START_Y) / (CELL_HEIGHT + GAP));
    const cellPos = cellY * COLS + cellX;
    if (cellX < 0 || cellX >= COLS || cellY < 0 || cellY >= ROWS) {
      return;
    }
    const cell = cells[cellPos];
    if (cell.flipped || cell.hidden) {
      return;
    }

    cell.flipped = true;
    cell.transition = {
      type: 'flip-up',
      duration: FLIP_UP_DURATION,
      elapse: 0,
    };
    flipped++;
    luot++;
    setTimeout(() => {
      onGameEnded();
    }, 1000);
  });

  gl.clearColor(0, 0, 0, 0);

  const BG_RATIO = bg.width / bg.height;
  const BG_DRAW_WIDTH = WORLD_WIDTH * 1.3;
  const BG_DRAW_HEIGHT = BG_DRAW_WIDTH / BG_RATIO;

  const loop = createGameLoop((delta: number) => {
    for (const cell of cells) {
      if (!cell.transition) {
        continue;
      }
      cell.transition.elapse += delta;
      if (cell.transition.elapse >= cell.transition.duration) {
        cell.transition = undefined;
      }
    }

    gl.clear(gl.COLOR_BUFFER_BIT);
    batch.setProjection(camera.combined);
    batch.begin();
    batch.draw(
      bg,
      -(BG_DRAW_WIDTH - WORLD_WIDTH) / 2,
      -(BG_DRAW_HEIGHT - WORLD_HEIGHT) / 2,
      BG_DRAW_WIDTH,
      BG_DRAW_HEIGHT
    );

    bongRegion.draw(batch, 10, 300, 372 * 1.7, 441 * 1.7);

    for (let i = 0; i < cells.length; i++) {
      const x = i % COLS;
      const y = Math.floor(i / COLS);

      const cell = cells[i];

      if (cell.hidden) {
        continue;
      }

      let region = cell.flipped ? thuong : backRegion;
      let scaleX = 1;
      let scaleY = 1;

      const transition = cell.transition;

      if (transition && transition.type === 'flip-up') {
        if (transition.elapse < transition.duration / 2) {
          region = backRegion;
          scaleX = 1 - transition.elapse / (transition.duration / 2);
        } else {
          region = thuong;
          scaleX = (transition.elapse - transition.duration / 2) / (transition.duration / 2);
        }
      }

      if (transition && transition.type === 'flip-down') {
        if (transition.elapse < transition.duration / 2) {
          region = thuong;
          scaleX = 1 - transition.elapse / (transition.duration / 2);
        } else {
          region = backRegion;
          scaleX = (transition.elapse - transition.duration / 2) / (transition.duration / 2);
        }
      }

      if (transition && transition.type === 'fade') {
        scaleX = 1 - transition.elapse / transition.duration;
        scaleY = scaleX;
      }

      batch.setColor(COLORS.WHITE);

      region.draw(
        batch,
        START_X + x * (CELL_WIDTH + GAP) + 30,
        START_Y + y * (CELL_HEIGHT + GAP) + 50,
        CELL_WIDTH * 0.9,
        CELL_HEIGHT * 0.8,
        CELL_WIDTH / 2,
        CELL_HEIGHT / 2,
        0,
        scaleX,
        scaleY
      );
    }

    batch.end();
  });

  return {
    start: () => {
      reset();
      // gameStarted = true;
      loop.start();
      luot = 1;
    },
    dispose() {
      // gameStarted = false;
      loop.stop();
      stage.cleanup();
      inputHandler.cleanup();
      batch.dispose();
      assetManager.disposeAll();
    },
    reset() {
      reset();
    },
  };
};
