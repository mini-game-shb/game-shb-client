import { action, Action, createStore } from 'easy-peasy';

import gamefoxSDK from './gamefoxSDK';

export interface StoreModel {
  layoutContainerStyle: { [key: string]: any };
  setLayoutContainerStyle: Action<StoreModel, { [key: string]: any }>;
  layoutMainStyle: { [key: string]: any };
  setLayoutMainStyle: Action<StoreModel, { [key: string]: any }>;
  noticeModal: { visible: boolean; action: string; buttonLabel?: string; notice: string };
  setNoticeModal: Action<StoreModel, { visible: boolean; action: string; buttonLabel?: string; notice: string }>;
  instructVisible: boolean;
  setInstructVisible: Action<StoreModel, boolean>;
  user: null | Awaited<ReturnType<typeof gamefoxSDK.auth>>;
  setUser: Action<StoreModel, Awaited<ReturnType<typeof gamefoxSDK.auth>> | null>;
  inventory: null | Awaited<ReturnType<typeof gamefoxSDK.getInventory>>;
  setInventory: Action<StoreModel, Awaited<ReturnType<typeof gamefoxSDK.getInventory>>>;
  pool: any;
  setPool: Action<StoreModel, any>;
}

const store = createStore<StoreModel>({
  layoutContainerStyle: {},
  layoutMainStyle: {},
  setLayoutContainerStyle: action((state, style) => {
    state.layoutContainerStyle = style;
  }),
  setLayoutMainStyle: action((state, style) => {
    state.layoutMainStyle = style;
  }),
  noticeModal: {
    visible: false,
    action: '',
    notice: '',
  },
  setNoticeModal: action((state, payload) => {
    state.noticeModal = payload;
  }),
  user: null,
  setUser: action((state, payload) => {
    state.user = payload;
  }),
  instructVisible: false,
  setInstructVisible: action((state, payload) => {
    state.instructVisible = payload;
  }),
  inventory: null,
  setInventory: action((state, payload) => {
    state.inventory = payload;
  }),
  pool: null,
  setPool: action((state, payload) => {
    state.inventory = payload;
  }),
});

export default store;
