import { StoreProvider } from 'easy-peasy';
import { useCallback, useEffect, useState } from 'react';
import { BrowserRouter, Route, Routes, useNavigate } from 'react-router-dom';

import './App.css';
import Layout from './components/Layout';
import config from './config';
import gamefoxSDK from './gamefoxSDK';
import { InstructScreen1 } from './screens/Instruct/game1/InstructScreen1';
import { InstructScreen2 } from './screens/Instruct/game2/Instruct';
import { InstructScreen3 } from './screens/Instruct/game3/Instruct';
import { InstructScreen5 } from './screens/Instruct/game5/Instruct';
import { CompleteGame2 } from './screens/Complete/CompleteGame2';
import { InstructMiniMap } from './screens/Instruct/InstructMiniMap';
import { Game6 } from './screens/Game/game6/Game';
import { Game3 } from './screens/Game/game3/Game';
import { InstructScreen6 } from './screens/Instruct/game6/Instruct';
import { Intro2 } from './screens/Intro/game2/Intro';
import { Intro3 } from './screens/Intro/game3/Intro';
import { Intro6 } from './screens/Intro/game6/Intro';
import { Intro5} from './screens/Intro/game5/Intro';
import Loading from './screens/Loading/Loading';
import { MainSceen } from './screens/Main/Main';
import { MiniMap } from './screens/MiniMap/MinMap';

import store from './store';
import { Intro1 } from './screens/Intro/game1/intro';
import { Game2 } from './screens/Game/game2/Game';
import { Game5 } from './screens/Game/game5/Game';
import { Game4 } from './screens/Game/game4/Game';
import { KhanhThanh } from './screens/Game/game1/KhanhThanh';
import { Game1 } from './screens/Game/game1/Game';

const FallbackRoute = () => {
  const navigate = useNavigate();

  useEffect(() => {
    // navigate({ pathname: '/', search: window.location.search });
    navigate({ pathname: '/' },{state:"mini_map"});
  }, [navigate]);

  return null;
};

const App = () => {
  return (
    <Routes>
      <Route path="/" element={<Loading  />} />
      {/* {user && ( */}
        <>
          <Route path="/instruct-mini-map" element={<InstructMiniMap />} />
          <Route path="/mini-map" element={<MiniMap />} />
          <Route path="/complete-gane-2" element={<CompleteGame2 />} />
          <Route path="/instruct-main" element={<MainSceen />} />
          {/* intro */}
          <Route path="/intro1" element={<Intro1 />} />
          <Route path="/intro2" element={<Intro2 />} />
          <Route path="/intro3" element={<Intro3 />} />
          <Route path="/intro5" element={<Intro5 />} />
          <Route path="/intro6" element={<Intro6 />} />
          {/* instruct */}
          <Route path="/instruct1" element={< InstructScreen1/>} />
          <Route path="/instruct2" element={<InstructScreen2 />} />
          <Route path="/instruct3" element={<InstructScreen3 />} />
          <Route path="/instruct5" element={<InstructScreen5 />} />
          <Route path="/instruct6" element={<InstructScreen6 />} />
          {/* game */}
          <Route path="/game1" element={<Game1 />} />
          <Route path="/game2" element={<Game2 />} />
          <Route path="/game6" element={<Game6 />} />
          <Route path="/game3" element={<Game3 />} />
          <Route path="/game5" element={<Game5 />} />
          <Route path="/game4" element={<Game4 />} />
          <Route path="/khanh-thanh" element={<KhanhThanh />} />
        </>
      {/* )} */}
      <Route path="*" element={<FallbackRoute />} />
    </Routes>
  );
};

const WrappedApp = () => {
  const [inited, setInited] = useState(false);

  const initSdk = useCallback(async () => {
    if (!config.online) {
      window.history.pushState(
        'Home',
        'Title'
        // '/?templateKey=64b8e7300bdee13d743304a4&campaignId=64b8e7300bdee13d7433049b&token=eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJjdXN0b21lcklkIjoiMjgyMDc3MDA2MSIsImRhdGEiOnsidXNlcm5hbWUiOiJOZ3V5ZW4gVmFuIDgxIn0sImlhdCI6MTY5MjI1NDE2OX0.YvuDwQJqbtQMKpAXZidMQquiMbYavLRTMqAtoOf7xdpwVvB8eiPGa-lCdbfqanlJHLtbt6AeDNx-PItAP34Zbt2xAAZHCwOHAy4s-y_0KCw_LkEiVRZ2MeTJx9BAfIJZKl4MGhzHb1iakyvrE2VF-UDGwKIFXOePRyuEGJaupaXPl34tJVbn9bKzl6IfNr2qTIRV5oUa7UTmOFB3j3BiAwhI3TNgsSrGIFZ5heuljEE_XrpY6k_zrhFbBElLmBB_5WhMB9-r7bQEgltbwtoZLt2eBYne44plriCJJpcM38pvWdwsLC08Hz54jdSaX5C0n9F8uws7BjkNZrdw07qP1Q'
      );
      // gamefoxSDK.setMockupData(mockupData);
    }
    await gamefoxSDK.init(config.apiUrl, !config.online);
  }, []);

  useEffect(() => {
    if (!inited) {
      initSdk();
    }
    setInited(true);
  }, [inited, initSdk]);

  if (!inited) {
    return null;
  }

  return (
    <StoreProvider store={store}>
      <BrowserRouter>
        <Layout>
          <App />
        </Layout>
      </BrowserRouter>
    </StoreProvider>
  );
};

export default WrappedApp;
