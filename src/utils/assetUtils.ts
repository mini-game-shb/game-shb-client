import { IScreen } from "../screens/Loading/Loading";

const ASSETS = {
  // ui1
  BG_GAME_1: 'assets/ui1/background.png',
  GACH: 'assets/ui1/gach.png',
  CAT: 'assets/ui1/cat.png',
  NUOC: 'assets/ui1/nuoc.png',
  THEP: 'assets/ui1/thep.png',
  BG_KHANH_THANH: 'assets/ui1/Bg-game-vong-quay.png',
  VAT_LIEU: 'assets/ui1/vat_lieu_khanh_thanh.png',
  TOA_NHA_KHANH_THANH: 'assets/ui1/toa_nha_khanh_thanh.png',
  BTN_KHANH_THANH: 'assets/ui1/Button_khanh_thanh.png',
  BG_KHANH_THANH_2: 'assets/ui1/bg_khanh_thanh_2.png',
  BG_INTRO_1: 'assets/ui1/bg_intro.png',
  BG_INTRO_2: 'assets/ui1/bg_intro1.png',
  BTN_BAT_DAU: 'assets/ui1/btn_bat_dau.png',
  BG_NHAN_VAT_LIEU: 'assets/ui1/bg_nhan_vat_lieu.png',
  ICON_GACH: 'assets/ui1/icon_gach.png',
  ICON_NUOC: 'assets/ui1/icon_nuoc.png',
  ICON_CAT: 'assets/ui1/icon_cat.png',
  ICON_THEP: 'assets/ui1/icon_thep.png',
  BTN_TIEP_TUC_G1: 'assets/ui1/btn_tiep_tuc.png',
  ICON_CLOSE_G1: 'assets/ui1/icon_close.png',
  MAT_BUON: 'assets/ui1/mat_buon.png',
  G1_HET_LUOT_CHOI: 'assets/ui1/g1_het_luot_choi.png',
  G1_ICON_CLOSE: '/assets/ui1/g1_icon_close.png',
  G1_BTN_TIM_THEM_LUOT: 'assets/ui1/g1_btn_tim_them_luot.png',
  G1_BTN_TIEP_TUC: 'assets/ui1/g1_btn_tiep_tuc.png',
  G1_TRUNG_THUONG_VOUCHER: 'assets/ui1/g1_trung_thuong_voucher.png',
  G1_CHU_TAM: '  assets/ui1/g1_chu_tam.png',
  G1_CHANG_2_TIN: 'assets/ui1/g1_chang2_tin.png',
  ICON_ROTATION:'assets/ui1/icon_rotation.png',
  TITLE_INTRO: 'assets/ui1/icon_intro.png',
  VONG_QUAY: 'assets/ui1/vong_quay_full_items.png',
  VL_HUONG_DAN: 'assets/ui1/vat_lieu_huong_dan.png',
  BG_TAM: 'assets/ui1/BG_TAM.png',
  BNT_START_PLAY:'assets/ui1/start_play.png',
  HD_CHOI: 'assets/ui1/Huong_dan_choi.png',
  TITLE_TAM: 'assets/ui1/Title_Tam.png',
  TOA_NHA_KHANH_THANH_FULL :'assets/ui1/toa_nha_khanh_thanh_full.png',
  TAM:'assets/ui1/tam.png',
  HUONG_DAN_VL: 'assets/ui1/vat_lieu_huong_dan.png',
  TAM_HD: 'assets/ui1/tam_khong_chu.png',
  BNT_START2: 'assets/ui1/bnt_start_huong_dan.png',

  // ui2
  BG_GAME2: 'assets/ui2/BG_Game2.png',
  BG_GAME2_MAIN: 'assets/ui2/bg_game2_main.png',
  LOGO_30: 'assets/ui2/logo_30_nam.png',
  BG_LOADING: 'assets/ui2/bg_loading.png',
  LOGO_TRI_AN: 'assets/ui2/logo_tri_an.png',
  BG_MAIN: 'assets/ui2/main/bg_mainScreen.png',
  BG_THU_DO: 'assets/ui2/BG_thu_do.png',
  RUONG: 'assets/ui2/ruong.png',
  BTN_START_2: 'assets/ui2/bnt_start_game2.png',
  HOAN_THANH_2: 'assets/ui2/hoan_thanh_game2.png',
  BAN_DO_2: 'assets/ui2/ban_do.png',
  BTN_BAT_DAU_HD_2: 'assets/ui2/Bnt_batDau_huong_dan.png',
  VE_MAY_BAY_1: 'assets/ui2/ve_mayBay1.png',
  VE_MAY_BAY_2: 'assets/ui2/ve_mayBay2.png',
  VE_MAY_BAY_3: 'assets/ui2/ve_mayBay3.png',
  VE_MAY_BAY_4: 'assets/ui2/ve_mayBay4.png',
  VE_MAY_BAY_DONE_1: 'assets/ui2/ve_mayBay1_done.png',
  VE_MAY_BAY_DONE_2: 'assets/ui2/ve_mayBay2_done.png',
  VE_MAY_BAY_DONE_3: 'assets/ui2/ve_mayBay3_done.png',
  VE_MAY_BAY_DONE_4: 'assets/ui2/ve_mayBay4_done.png',
  GROUD_RUONG: 'assets/ui2/gr_ruong.png',
  HOM_VE_1: 'assets/ui2/hom_ve_1.png',
  HOM_VE_2: 'assets/ui2/hom_ve_2.png',
  HOM_VE_3: 'assets/ui2/hom_ve_3.png',
  HOM_VE_4: 'assets/ui2/hom_ve_4.png',

  //minni map
  BG_MINNI_MAP: 'assets/ui2/minimap/bg_global.png',
  BG_GIO_QUA_INTRO:'assets/ui2/minimap/bg_gio_qua_intro.png',
  LOGO_VUON_TAM: 'assets/ui2/minimap/logo_vuon_tam.png',
  MINNI_MAP: 'assets/ui2/minimap/minmap_black.png',
  ICON_BANK: 'assets/ui2/minimap/ngan_hang.png',
  ICON_SAN_BAY: 'assets/ui2/minimap/san_bay.png',
  MINI_MAP_DONE: 'assets/ui2/minimap/miniMap_done.png',
  MINI_MAP_CUSTOM: 'assets/ui2/minimap/miniMap_custom.png',
  MINI_MAP_MANI: 'assets/ui2/minimap/miniMap_custom_1.png',
  ICON_SAN_BAY_LOCK: 'assets/ui2/minimap/icon_san_bay_lock.png',
  ICON_ATM: 'assets/ui2/minimap/icon_ATM.png',
  ICON_ATM_LOCK: 'assets/ui2/minimap/icon_atm_lock.png',
  ICON_DA_BANH_LOCK: 'assets/ui2/minimap/icon_da_banh_lock.png',
  ICON_DA_BANH: 'assets/ui2/minimap/icon_da_banh.png',
  ICON_MAY_BAY: 'assets/ui2/minimap/icon_may_bay.png',
  ICON_MAY_BAY_LOCK: 'assets/ui2/minimap/icon_may_bay_lock.png',
  ICON_MO_QUA_LOCK: 'assets/ui2/minimap/iocn_hop_qua_lock.png',
  ICON_MO_QUA: 'assets/ui2/minimap/icon_hop_qua.png',
  BAll: 'assets/ui2/ball.png',
  BO_SUU_TAP_INTRO:'assets/ui2/minimap/bo_suu_tap_intro.png',

  //game 3
  BG3: 'assets/bg3.png',
  LAYER: 'assets/layer.png',
  VIET_NAM: 'assets/ui3/viet_nam.png',
  VIET_NAM_SELECTED: 'assets/ui3/viet_nam_selected.png',
  LAO: 'assets/ui3/lao.png',
  LAO_SELECTED: 'assets/ui3/lao_selected.png',
  CAMPUCHIA: 'assets/ui3/campuchia.png',
  CAMPUCHIA_SELECTED: 'assets/ui3/campuchia_selected.png',
  BG_GAME3: 'assets/ui3/mainScreen.png',
  BG_KHU_VUC: 'assets/ui3/logo_khu_vuc.png',
  BG_GAME3_MAIN: 'assets/ui3/G3Gameplay.png',
  LOGO_CUOC_KY: 'assets/ui3/cuoc_ky.png',
  MAY_BAY: 'assets/ui3/may_bay.png',
  SAO: 'assets/ui3/ngoi_sao.png',
  BANK: 'assets/ui3/bank.png',
  BALL: 'assets/ui3/bg_ball.png',
  VICTORY: 'assets/ui3/victory.png',
  BTN_NHAN_QUA: 'assets/ui3/btn_nhan_qua.png',
  ICO_CLOSE_G3: 'assets/ui3/icon_close.png',
  BG_TRUNG_TIEN_G3: 'assets/ui3/bg_trung_tien.png',
  BTN_TIEP_TUC_G3: 'assets/ui3/btn_tiep_tuc.png',
  BG_HET_LUOT_CHOI_G3: 'assets/ui3/bg_het_luot_choi_g3.png',
  BTN_TIM_THEM_LUOT_G3: 'assets/ui3/btn_tim_them_luot.png',

  // Nhận lượt chơi
  BG_NHAN_LUOT_CHOI: 'assets/NhanLuotChoi/bg_nhan_luot_choi.png',
  BTN_VAO_CHOI: 'assets/NhanLuotChoi/btn_vao_choi.png',
  THEM_1_LUOT_CHOI: 'assets/NhanLuotChoi/1_luot_choi.png',
  THEM_2_LUOT_CHOI: 'assets/NhanLuotChoi/2_luot_choi.png',
  THEM_3_LUOT_CHOI: 'assets/NhanLuotChoi/3_luot_choi.png',
  THEM_4_LUOT_CHOI: 'assets/NhanLuotChoi/4_luot_choi.png',
  THEM_5_LUOT_CHOI: 'assets/NhanLuotChoi/5_luot_choi.png',
  THEM_6_LUOT_CHOI: 'assets/NhanLuotChoi/6_luot_choi.png',
  THEM_7_LUOT_CHOI: 'assets/NhanLuotChoi/7_luot_choi.png',
  THEM_8_LUOT_CHOI: 'assets/NhanLuotChoi/8_luot_choi.png',
  THEM_9_LUOT_CHOI: 'assets/NhanLuotChoi/9_luot_choi.png',
  
  //game4
  BG4: 'assets/bg4.png',
  BG_HET_THOI_GIAN: 'assets/ui4/bg_het_thoi_gian.png',
  BTN_BAT_DAU_LAI: 'assets/ui4/btn_bat_dau_lai.png',
  BG_TUYET_VOI: 'assets/ui4/bg_tuyet_voi.png',
  BTN_NHAN_QUA_G4: 'assets/ui4/btn_nhan_qua.png',
  BG_CHANG_5_TRI: 'assets/ui4/bg_chang_5_tri.png',
  BTN_CHANG_5_TRI: 'assets/ui4/btn_chang_5_tri.png',
  ICON_CLOSE: 'assets/ui4/icon_close.png',
  BG_VOUCHER: 'assets/ui4/bg_voucher.png',
  BTN_TIEP_TUC: 'assets/ui4/btn_tiep_tuc.png',
  BG_HET_LUOT_CHOI: 'assets/ui4/bg_het_luot_choi.png',
  BTN_TIM_THEM_LUOT: 'assets/ui4/btn_tim_them_luot.png',
  BG_TRUNG_TIEN: 'assets/ui4/bg_trung_tien.png',

  //game5  game virrus
BG5:"assets/bg5.png",
BG_OVER_TIME:"assets/ui5/het_thoi_gian.png",
BG_CHUC_MUNG_KHIEN:"assets/ui5/chuc_mung_khien.png",
BTN_BAT_DAU_LAII:"assets/ui5/btn_bat_dau_lai.png",
BTN_NHAN_QUAA:"assets/ui5/btn_nhan_qua.png",
BTN_TIEP_TUCC:"assets/ui5/btn_tiep_tuc.png",
BTN_TIM_THEM_LUOTT:"assets/ui5/btn_tim_them_luot.png",
TRUNG_THUONG_VOUCHER:"assets/ui5/trung_thuong_voucher.png",
TRUNG_THUONG_TIEN:"assets/ui5/bg_trung_tien.png",
PHAO_HOA:"assets/ui5/phao_hoa.png",
HET_LUOT_CHOI:"assets/ui5/het_luot_choi .png",
CHU_TRI:"assets/ui5/chu_tri.png",
CHANG_5:"assets/ui5/chang_5.png",
ICON_CLOSEE:"assets/ui5/icon_close.png",
BG_THU_LAI_NAO:"assets/ui5/thu_lai_nao.png",

  //ui5
BG_Game_5: 'assets/ui5/intro/BG_Game5.png', 
  //ui6
  BG_GAME: 'assets/ui6/intro/bg_golobal.png',

  //mission
  BG_MISSION: 'assets/Mission/bg_mission.png'
};

const ASSET_MINIMAP = {
  BG_MINNI_MAP: 'assets/ui2/minimap/bg_global.png',
  BG_GIO_QUA_INTRO:'assets/ui2/minimap/bg_gio_qua_intro.png',
  LOGO_VUON_TAM: 'assets/ui2/minimap/logo_vuon_tam.png',
  MINNI_MAP: 'assets/ui2/minimap/minmap_black.png',
  ICON_BANK: 'assets/ui2/minimap/ngan_hang.png',
  ICON_SAN_BAY: 'assets/ui2/minimap/san_bay.png',
  MINI_MAP_DONE: 'assets/ui2/minimap/miniMap_done.png',
  MINI_MAP_CUSTOM: 'assets/ui2/minimap/miniMap_custom.png',
  MINI_MAP_MANI: 'assets/ui2/minimap/miniMap_custom_1.png',
  ICON_SAN_BAY_LOCK: 'assets/ui2/minimap/icon_san_bay_lock.png',
  ICON_ATM: 'assets/ui2/minimap/icon_ATM.png',
  ICON_ATM_LOCK: 'assets/ui2/minimap/icon_atm_lock.png',
  ICON_DA_BANH_LOCK: 'assets/ui2/minimap/icon_da_banh_lock.png',
  ICON_DA_BANH: 'assets/ui2/minimap/icon_da_banh.png',
  ICON_MAY_BAY: 'assets/ui2/minimap/icon_may_bay.png',
  ICON_MAY_BAY_LOCK: 'assets/ui2/minimap/icon_may_bay_lock.png',
  ICON_MO_QUA_LOCK: 'assets/ui2/minimap/iocn_hop_qua_lock.png',
  ICON_MO_QUA: 'assets/ui2/minimap/icon_hop_qua.png',
  BAll: 'assets/ui2/ball.png',
  BO_SUU_TAP_INTRO:'assets/ui2/minimap/bo_suu_tap_intro.png',
    //mission
    BG_MISSION: 'assets/Mission/bg_mission.png'
}

const ASSET_GAME1={
  BAll: 'assets/ui2/ball.png',
  BG_GAME_1: 'assets/ui1/background.png',
  GACH: 'assets/ui1/gach.png',
  CAT: 'assets/ui1/cat.png',
  NUOC: 'assets/ui1/nuoc.png',
  THEP: 'assets/ui1/thep.png',
  BG_KHANH_THANH: 'assets/ui1/Bg-game-vong-quay.png',
  VAT_LIEU: 'assets/ui1/vat_lieu_khanh_thanh.png',
  TOA_NHA_KHANH_THANH: 'assets/ui1/toa_nha_khanh_thanh.png',
  BTN_KHANH_THANH: 'assets/ui1/Button_khanh_thanh.png',
  BG_KHANH_THANH_2: 'assets/ui1/bg_khanh_thanh_2.png',
  BG_INTRO_1: 'assets/ui1/bg_intro.png',
  BG_INTRO_2: 'assets/ui1/bg_intro1.png',
  BTN_BAT_DAU: 'assets/ui1/btn_bat_dau.png',
  BG_NHAN_VAT_LIEU: 'assets/ui1/bg_nhan_vat_lieu.png',
  ICON_GACH: 'assets/ui1/icon_gach.png',
  ICON_NUOC: 'assets/ui1/icon_nuoc.png',
  ICON_CAT: 'assets/ui1/icon_cat.png',
  ICON_THEP: 'assets/ui1/icon_thep.png',
  BTN_TIEP_TUC_G1: 'assets/ui1/btn_tiep_tuc.png',
  ICON_CLOSE_G1: 'assets/ui1/icon_close.png',
  MAT_BUON: 'assets/ui1/mat_buon.png',
  G1_HET_LUOT_CHOI: 'assets/ui1/g1_het_luot_choi.png',
  G1_ICON_CLOSE: '/assets/ui1/g1_icon_close.png',
  G1_BTN_TIM_THEM_LUOT: 'assets/ui1/g1_btn_tim_them_luot.png',
  G1_BTN_TIEP_TUC: 'assets/ui1/g1_btn_tiep_tuc.png',
  G1_TRUNG_THUONG_VOUCHER: 'assets/ui1/g1_trung_thuong_voucher.png',
  G1_CHU_TAM: '  assets/ui1/g1_chu_tam.png',
  G1_CHANG_2_TIN: 'assets/ui1/g1_chang2_tin.png',
  ICON_ROTATION:'assets/ui1/icon_rotation.png',
  TITLE_INTRO: 'assets/ui1/icon_intro.png',
  VONG_QUAY: 'assets/ui1/vong_quay_full_items.png',
  VL_HUONG_DAN: 'assets/ui1/vat_lieu_huong_dan.png',
  BG_TAM: 'assets/ui1/BG_TAM.png',
  BNT_START_PLAY:'assets/ui1/start_play.png',
  HD_CHOI: 'assets/ui1/Huong_dan_choi.png',
  TITLE_TAM: 'assets/ui1/Title_Tam.png',
  TOA_NHA_KHANH_THANH_FULL :'assets/ui1/toa_nha_khanh_thanh_full.png',
  TAM:'assets/ui1/tam.png',
  HUONG_DAN_VL: 'assets/ui1/vat_lieu_huong_dan.png',
  TAM_HD: 'assets/ui1/tam_khong_chu.png',
  BNT_START2: 'assets/ui1/bnt_start_huong_dan.png',

}

const ASSET_GAME2={
  BAll: 'assets/ui2/ball.png',
  BG_GAME2: 'assets/ui2/BG_Game2.png',
  BG_GAME2_MAIN: 'assets/ui2/bg_game2_main.png',
  LOGO_30: 'assets/ui2/logo_30_nam.png',
  BG_LOADING: 'assets/ui2/bg_loading.png',
  LOGO_TRI_AN: 'assets/ui2/logo_tri_an.png',
  BG_MAIN: 'assets/ui2/main/bg_mainScreen.png',
  BG_THU_DO: 'assets/ui2/BG_thu_do.png',
  RUONG: 'assets/ui2/ruong.png',
  BTN_START_2: 'assets/ui2/bnt_start_game2.png',
  HOAN_THANH_2: 'assets/ui2/hoan_thanh_game2.png',
  BAN_DO_2: 'assets/ui2/ban_do.png',
  BTN_BAT_DAU_HD_2: 'assets/ui2/Bnt_batDau_huong_dan.png',
  VE_MAY_BAY_1: 'assets/ui2/ve_mayBay1.png',
  VE_MAY_BAY_2: 'assets/ui2/ve_mayBay2.png',
  VE_MAY_BAY_3: 'assets/ui2/ve_mayBay3.png',
  VE_MAY_BAY_4: 'assets/ui2/ve_mayBay4.png',
  VE_MAY_BAY_DONE_1: 'assets/ui2/ve_mayBay1_done.png',
  VE_MAY_BAY_DONE_2: 'assets/ui2/ve_mayBay2_done.png',
  VE_MAY_BAY_DONE_3: 'assets/ui2/ve_mayBay3_done.png',
  VE_MAY_BAY_DONE_4: 'assets/ui2/ve_mayBay4_done.png',
  GROUD_RUONG: 'assets/ui2/gr_ruong.png',
  HOM_VE_1: 'assets/ui2/hom_ve_1.png',
  HOM_VE_2: 'assets/ui2/hom_ve_2.png',
  HOM_VE_3: 'assets/ui2/hom_ve_3.png',
  HOM_VE_4: 'assets/ui2/hom_ve_4.png',
}

const ASSET_GAME3 = {
  BAll: 'assets/ui2/ball.png',
  BG3: 'assets/bg3.png',
  LAYER: 'assets/layer.png',
  VIET_NAM: 'assets/ui3/viet_nam.png',
  VIET_NAM_SELECTED: 'assets/ui3/viet_nam_selected.png',
  LAO: 'assets/ui3/lao.png',
  LAO_SELECTED: 'assets/ui3/lao_selected.png',
  CAMPUCHIA: 'assets/ui3/campuchia.png',
  CAMPUCHIA_SELECTED: 'assets/ui3/campuchia_selected.png',
  BG_GAME3: 'assets/ui3/mainScreen.png',
  BG_KHU_VUC: 'assets/ui3/logo_khu_vuc.png',
  BG_GAME3_MAIN: 'assets/ui3/G3Gameplay.png',
  LOGO_CUOC_KY: 'assets/ui3/cuoc_ky.png',
  MAY_BAY: 'assets/ui3/may_bay.png',
  SAO: 'assets/ui3/ngoi_sao.png',
  BANK: 'assets/ui3/bank.png',
  BALL: 'assets/ui3/bg_ball.png',
  VICTORY: 'assets/ui3/victory.png',
  BTN_NHAN_QUA: 'assets/ui3/btn_nhan_qua.png',
  ICO_CLOSE_G3: 'assets/ui3/icon_close.png',
  BG_TRUNG_TIEN_G3: 'assets/ui3/bg_trung_tien.png',
  BTN_TIEP_TUC_G3: 'assets/ui3/btn_tiep_tuc.png',
  BG_HET_LUOT_CHOI_G3: 'assets/ui3/bg_het_luot_choi_g3.png',
  BTN_TIM_THEM_LUOT_G3: 'assets/ui3/btn_tim_them_luot.png',
}

const ASSET_GAME4 = {
  BG4: 'assets/bg4.png',
  BG_HET_THOI_GIAN: 'assets/ui4/bg_het_thoi_gian.png',
  BTN_BAT_DAU_LAI: 'assets/ui4/btn_bat_dau_lai.png',
  BG_TUYET_VOI: 'assets/ui4/bg_tuyet_voi.png',
  BTN_NHAN_QUA_G4: 'assets/ui4/btn_nhan_qua.png',
  BG_CHANG_5_TRI: 'assets/ui4/bg_chang_5_tri.png',
  BTN_CHANG_5_TRI: 'assets/ui4/btn_chang_5_tri.png',
  ICON_CLOSE: 'assets/ui4/icon_close.png',
  BG_VOUCHER: 'assets/ui4/bg_voucher.png',
  BTN_TIEP_TUC: 'assets/ui4/btn_tiep_tuc.png',
  BG_HET_LUOT_CHOI: 'assets/ui4/bg_het_luot_choi.png',
  BTN_TIM_THEM_LUOT: 'assets/ui4/btn_tim_them_luot.png',
  BG_TRUNG_TIEN: 'assets/ui4/bg_trung_tien.png',

}

const ASSET_GAME5 = {
    //game5  game virrus
BG5:"assets/bg5.png",
BG_OVER_TIME:"assets/ui5/het_thoi_gian.png",
BG_CHUC_MUNG_KHIEN:"assets/ui5/chuc_mung_khien.png",
BTN_BAT_DAU_LAII:"assets/ui5/btn_bat_dau_lai.png",
BTN_NHAN_QUAA:"assets/ui5/btn_nhan_qua.png",
BTN_TIEP_TUCC:"assets/ui5/btn_tiep_tuc.png",
BTN_TIM_THEM_LUOTT:"assets/ui5/btn_tim_them_luot.png",
TRUNG_THUONG_VOUCHER:"assets/ui5/trung_thuong_voucher.png",
TRUNG_THUONG_TIEN:"assets/ui5/bg_trung_tien.png",
PHAO_HOA:"assets/ui5/phao_hoa.png",
HET_LUOT_CHOI:"assets/ui5/het_luot_choi .png",
CHU_TRI:"assets/ui5/chu_tri.png",
CHANG_5:"assets/ui5/chang_5.png",
ICON_CLOSEE:"assets/ui5/icon_close.png",
BG_THU_LAI_NAO:"assets/ui5/thu_lai_nao.png",
  //ui5
BG_Game_5: 'assets/ui5/intro/BG_Game5.png', 
}

const ASSET_GAME6 = {
    //ui6
    BG_GAME: 'assets/ui6/intro/bg_golobal.png',
    BG_GAME2_MAIN: 'assets/ui2/bg_game2_main.png',
    BAll: 'assets/ui2/ball.png',
}

  
export const cacheAssets = (screen:IScreen) =>{
  let asset:any
  switch (screen) {
    case 'mini_map':
      asset = ASSET_MINIMAP
      break;
      case 'game1':
      asset = ASSET_GAME1
      break;
      case 'game2':
      asset = ASSET_GAME2
      break;
      case 'game3':
      asset = ASSET_GAME3
      break;
      case 'game4':
      asset = ASSET_GAME4
      break;
      case 'game5':
      asset = ASSET_GAME5
      break;
      case 'game6':
      asset = ASSET_GAME6
      break;
    default:
      break;
  }
  return new Promise((resolve) => {
    const imageToLoad = Object.keys(asset).length;
    let imageLoaded = 0;
    if (imageToLoad <= 0) {
      resolve(null);
    }
    Object.keys(asset).forEach((assetKey) => {
      loadImage(asset[assetKey])
        .then(() => {
          imageLoaded++;
          if (imageLoaded >= imageToLoad) {
            resolve(null);
          }
        })
        .catch(() => {
          if (imageLoaded >= imageToLoad) {
            resolve(null);
          }
        });
    });
  });
}



export const loadImage = (src: string) =>
  new Promise((resolve, reject) => {
    const image = new Image();
    image.src = src;
    image.onload = () => {
      resolve(null);
    };
    image.onerror = () => {
      reject(null);
    };
  });

export const sleep = (ms: number) =>
  new Promise((resolve) => {
    setTimeout(() => {
      resolve(null);
    }, ms);
  });

export { ASSETS , ASSET_GAME1,ASSET_GAME2,ASSET_GAME3,ASSET_GAME4,ASSET_GAME5,ASSET_GAME6,ASSET_MINIMAP };
