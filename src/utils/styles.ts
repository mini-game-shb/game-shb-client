const getDocumentPropertyNumber = (varName: string) => {
  const value = getComputedStyle(document.documentElement).getPropertyValue(varName);
  if (value) {
    const number = parseFloat(value.replace('px', ''));
    if (isNaN(number)) {
      return 0;
    }
    return number;
  } else {
    return 0;
  }
};

export const getViewport = () => {
  return {
    width: getDocumentPropertyNumber('--viewport-width'),
    height: getDocumentPropertyNumber('--viewport-height'),
  };
};

export const getEnvInsets = () => {
  return {
    top: getDocumentPropertyNumber('--inset-top'),
    bottom: getDocumentPropertyNumber('--inset-bottom'),
    left: getDocumentPropertyNumber('--inset-left'),
    right: getDocumentPropertyNumber('--inset-right'),
  };
};
