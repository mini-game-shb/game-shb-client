import classes from "./Instruct.module.css"
import { ASSETS } from "../../../utils/assetUtils"
import Header from "../../../components/Header"
import cx from "classnames"
import { useEffect, useState } from "react"
import { useNavigate } from "react-router";
// type Prop = object
// : React.FC<Prop>
export const InstructScreen6 = () => {
    sessionStorage.setItem("intro6","true")
    const addres = "Thời đại số"
    const [next, setNext] = useState(0)
    const navigate = useNavigate()
    return (
        <div
            className={classes.globalBg}
            style={{ backgroundImage: `url(${ASSETS.BG_GAME2_MAIN})` }}
        >
            <Header
                showBackBtn={true}
                hideMute={true}
                hideShareBtn={true}
                content="chặng 6-"
                user={addres}
            >
            </Header>
            {/* next 0 */}
            <div style={next == 0 ? {} : { display: "none" }}>
                <div className={classes.modal_huong_dan}>
                    <img className={classes.icon_tam} src="/assets/ui6/intro/icon_tam.png" />
                    <div className={cx(classes.content, 'pro-light-shb')}>
                        Trải qua “Chặng đường 30 năm”,
                        SHB đã từ TÂM vươn TẦM, trở thành
                        1 trong 5 Ngân hàng TMCP tư nhân có quy mô lớn nhất Việt Nam.
                        Cùng thổi nến, cắt bánh và chúc mừng sinh nhật SHB bạn nhé!
                    </div>
                    <div
                        onClick={() => { setNext(1) }}
                        className={cx(classes.bnt_next, 'pro-light-shb')}>CHẠM VÀO ĐỂ TIẾP TỤC</div>
                </div>
            </div>
            {/* next 1 */}
            <div style={next == 1 ? {} : { display: "none" }}>
                <img style={{ zIndex: "11", position: "absolute", marginTop: "calc(var(--viewport-width) * 0.63)" }} className={classes.ruong_qua} src="/assets/ui6/intro/ruong_qua.png" />
                <div className={classes.modal_huong_dan_ruong_qua}>
                    <div className={cx(classes.content, 'pro-light-shb')}>
                        Mừng sinh nhật 30 năm tặng bạn rất nhiều phần quà hấp dẫn. Chọn một hộp quà bất kì để khám phá món quà may mắn dành cho bạn nhé!
                    </div>
                    <div onClick={() => { setNext(2) }}
                        className={cx(classes.bnt_next, 'pro-light-shb')}>CHẠM VÀO ĐỂ TIẾP TỤC</div>
                </div>
            </div>
               {/* next 2 */}
               <div style={next == 2 ? {} : { display: "none" }}>
                <img style={{ zIndex: "11", position: "absolute", marginTop: "calc(var(--viewport-width) * 0.5)" }} className={classes.ruong_qua} src="/assets/ui6/tam_instruct.png" />
                <div className={classes.modal_huong_dan_tam}>
                    <div className={cx(classes.content, 'pro-light-shb')}>
                        Người chơi nhận được chữ TẦM sẽ hoàn thành hành trình Từ TÂM vươn TẦM cùng SHB
                    </div>
                    <div   onClick={() => { setNext(3) }}
                        className={cx(classes.bnt_next, 'pro-light-shb')}>CHẠM VÀO ĐỂ TIẾP TỤC</div>
                </div>
            </div>
            
               {/* next 3 */}
               <div style={next == 3 ? {} : { display: "none" }}>
               <div className={classes.wow} style={{ zIndex: "11", position: "absolute", marginTop: "calc(var(--viewport-width) * 0.5)" }}>
                <img  src="/assets/ui6/Wow.png" alt="" />
               </div>
                <img style={{ zIndex: "11", position: "absolute", marginTop: "calc(var(--viewport-width) * 0.53)" }} className={classes.ruong_qua} src="/assets/ui6/gold_reward.png" />
                <div className={classes.title_trung_thuong} >
                    <p>Mã số dự thưởng của bạn là</p>
                    <p style={{fontWeight: 600,fontSize: 'calc(var(--viewport-height) * 0.022)'}}>0293HDSFGO123410</p>
                </div>
                <img src="/assets/ui6/btn_xem_gio_qua.png" className={classes.btn_xem_gio_qua} />
                <img src="/assets/ui6/icon_close.png" className={classes.btn_close} alt="" />
                <div className={classes.modal_huong_dan_ruong_qua}>
                    <div className={cx(classes.content, 'pro-light-shb')}>
                    Sau khi thu thập đủ 6 chữ, người chơi sẽ nhận được mã dự thưởng quay số cuối chương trình 
                    </div>
                    <div onClick={() => { 
                        navigate({ pathname: '/game6', search: window.location.search });
                    }} className={cx(classes.bnt_next, 'pro-light-shb')}>CHẠM VÀO ĐỂ TIẾP TỤC</div>
                </div>
            </div>

            <img className={classes.log0_30nam} src="/assets/ui6/intro/logo_icon_30nam.png" />
            <img className={classes.ruong_qua} src="/assets/ui6/intro/ruong_qua.png" />
            <div className={cx(classes.content_text, 'pro-light-shb')}>Số lượt chơi: 5 lượt</div>
            <div className={classes.ball}
                style={{ backgroundImage: `url(${ASSETS.BAll})` }}>
            </div>
        </div>
    )
}


