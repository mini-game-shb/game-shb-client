import cx from 'classnames';
import { useState } from 'react';
import { useNavigate } from 'react-router';

import Header from '../../../components/Header';
import { ASSET_GAME6 } from '../../../utils/assetUtils';
import classes from './Instruct.module.css';

type Prop = object;

export const InstructScreen6: React.FC<Prop> = () => {
  sessionStorage.setItem('intro6', 'true');
  const addres = 'Thời đại số';
  const [next, setNext] = useState(0);
  const navigate = useNavigate();
  return (
    <div className={classes.globalBg} style={{ backgroundImage: `url(${ASSET_GAME6.BG_GAME2_MAIN})` }}>
      <Header showBackBtn={true} hideMute={true} hideShareBtn={true} content="chặng 6-" user={addres}></Header>
      {/* next 0 */}
      <div style={next == 0 ? {} : { display: 'none' }}>
        <div className={classes.modal_huong_dan}>
          <img className={classes.icon_tam} src="/ASSET_GAME6/ui6/intro/icon_tam.png" />
          <div className={cx(classes.content, 'pro-light-shb')}>
            Trải qua “Chặng đường 30 năm”, SHB đã từ TÂM vươn TẦM, trở thành 1 trong 5 Ngân hàng TMCP tư nhân có quy mô
            lớn nhất Việt Nam. Cùng thổi nến, cắt bánh và chúc mừng sinh nhật SHB bạn nhé!
          </div>
          <div
            onClick={() => {
              setNext(1);
            }}
            className={cx(classes.bnt_next, 'pro-light-shb')}>
            CHẠM VÀO ĐỂ TIẾP TỤC
          </div>
        </div>
      </div>
      {/* next 1 */}
      <div style={next == 1 ? {} : { display: 'none' }}>
        <img
          style={{ zIndex: '11', position: 'absolute', marginTop: 'calc(var(--viewport-width) * 0.63)' }}
          className={classes.ruong_qua}
          src="/ASSET_GAME6/ui6/intro/ruong_qua.png"
        />
        <div className={classes.modal_huong_dan_ruong_qua}>
          <div className={cx(classes.content, 'pro-light-shb')}>
            Mừng sinh nhật 30 năm tặng bạn rất nhiều phần quà hấp dẫn. Chọn một hộp quà bất kì để khám phá món quà may
            mắn dành cho bạn nhé!
          </div>
          <div
            onClick={() => {
              navigate({ pathname: '/game6', search: window.location.search });
            }}
            className={cx(classes.bnt_ruong_qua, 'pro-light-shb')}>
            CHẠM VÀO ĐỂ TIẾP TỤC
          </div>
        </div>
      </div>
      <img className={classes.log0_30nam} src="/ASSET_GAME6/ui6/intro/logo_icon_30nam.png" />
      <img className={classes.ruong_qua} src="/ASSET_GAME6/ui6/intro/ruong_qua.png" />
      <div className={cx(classes.content_text, 'pro-light-shb')}>Số lượt chơi: 5 lượt</div>
      <div className={classes.ball} style={{ backgroundImage: `url(${ASSET_GAME6.BAll})` }}></div>
    </div>
  );
};
