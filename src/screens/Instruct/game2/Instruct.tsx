import cx from 'classnames';
import { useState } from 'react';
import { useNavigate } from 'react-router';

import Header from '../../../components/Header';
import { ASSET_GAME2 } from '../../../utils/assetUtils';
import classes from './Instruct.module.css';

type Prop = object;

export const InstructScreen2: React.FC<Prop> = () => {
  sessionStorage.setItem('intro2', 'true');
  const addres = 'Tiến tới thủ đô';
  const [next, setNext] = useState(0);
  const navigate = useNavigate();
  return (
    <>
      {next == 4 ? (
        <div style={{ backgroundImage: `url(${ASSET_GAME2.HOAN_THANH_2})` }} className={classes.globalBg}>
          <Header showBackBtn={true} hideMute={true} hideShareBtn={true} content="chặng 2-" user={addres}></Header>
          <div style={next == 4 ? {} : { display: 'none' }}>
            <img className={classes.ban_do} src={ASSET_GAME2.BAN_DO_2} />
            <div style={{ height: 'calc(var(--viewport-width)* 0.33)' }} className={classes.modal_huong_dan_ban_do}>
              <div className={cx(classes.content_map, 'pro-light-shb')}>Click vào để bắt đầu chơi game</div>
              <div
                style={{ marginTop: 'calc(var(--viewport-width) * 0.01)' }}
                onClick={() => {
                  navigate({ pathname: '/game2', search: window.location.search });
                }}
                className={cx(classes.bnt_next_game, 'pro-light-shb')}>
                <img src={ASSET_GAME2.BTN_BAT_DAU_HD_2} />
              </div>
            </div>
            <div className={classes.ball} style={{ backgroundImage: `url(${ASSET_GAME2.BAll})` }}></div>
          </div>
        </div>
      ) : (
        <div className={classes.globalBg} style={{ backgroundImage: `url(${ASSET_GAME2.BG_GAME2_MAIN})` }}>
          <Header showBackBtn={true} hideMute={true} hideShareBtn={true} content="chặng 2-" user={addres}></Header>
          {/* next 0 */}
          <div style={next == 0 ? {} : { display: 'none' }}>
            <img
              style={{ zIndex: '11', position: 'absolute', marginTop: 'calc(var(--viewport-width) * 0.4)' }}
              className={classes.gr_ruong}
              src="/ASSET_GAME2/ui2/gr_ruong.png"
            />
            <div className={classes.modal_huong_dan}>
              <div className={cx(classes.content, 'pro-light-shb')}>Người chơi mở hòm từ 1 đến 6</div>
              <div
                onClick={() => {
                  setNext(1);
                }}
                className={cx(classes.bnt_next, 'pro-light-shb')}>
                CHẠM VÀO ĐỂ TIẾP TỤC
              </div>
            </div>
          </div>
          {/* next 1 */}
          <div style={next == 1 ? {} : { display: 'none' }}>
            <img
              style={{ zIndex: '11', position: 'absolute', marginTop: 'calc(var(--viewport-width) * 0.7)' }}
              className={classes.ruong_qua}
              src="/ASSET_GAME2/ui2/ruong_qua.png"
            />
            <div
              style={{ top: 'calc(var(--viewport-width) * 1.45)', paddingTop: 'calc(var(--viewport-width) * 0.06)' }}
              className={classes.modal_huong_dan}>
              <div className={cx(classes.content, 'pro-light-shb')}>Mở rương ra sẽ được nhiều phần quà khác nhau</div>
              <div
                style={{ marginTop: 'calc(var(--viewport-height)*0.02)' }}
                onClick={() => {
                  setNext(2);
                }}
                className={cx(classes.bnt_next, 'pro-light-shb')}>
                CHẠM VÀO ĐỂ TIẾP TỤC
              </div>
            </div>
          </div>
          {/* next 2 */}
          <div style={next == 2 ? {} : { display: 'none' }}>
            <div style={{ zIndex: '11', position: 'absolute' }} className={classes.container_veMayBay}>
              <img className={classes.veMayBay} src={ASSET_GAME2.VE_MAY_BAY_DONE_1} />
              <img className={classes.veMayBay} src={ASSET_GAME2.VE_MAY_BAY_DONE_2} />
              <img className={classes.veMayBay} src={ASSET_GAME2.VE_MAY_BAY_DONE_3} />
              <img className={classes.veMayBay} src={ASSET_GAME2.VE_MAY_BAY_DONE_4} />
            </div>
            <div style={{ top: 'calc(var(--viewport-width) * 0.5)' }} className={classes.modal_huong_dan}>
              <div className={cx(classes.content, 'pro-light-shb')}>Thu thập đủ 4 vé để ghép vé máy bay</div>
              <div
                onClick={() => {
                  setNext(3);
                }}
                className={cx(classes.bnt_next, 'pro-light-shb')}>
                CHẠM VÀO ĐỂ TIẾP TỤC
              </div>
            </div>
          </div>
          {/* next 3 */}
          <div style={next == 3 ? {} : { display: 'none' }}>
            <img
              style={{
                zIndex: '11',
                position: 'absolute',
                marginTop: 'calc(var(--viewport-width) * 0.4)',
                marginLeft: 'calc(var(--viewport-width) * 0.06)',
              }}
              className={classes.gr_ruong}
              src="/ASSET_GAME2/ui2/trung_ve_may_bay.png"
            />
            <div
              style={{
                top: 'calc(var(--viewport-width) * 1.45)',
                paddingTop: 'calc(var(--viewport-width) * 0.06)',
                paddingLeft: 'calc(var(--viewport-width) * 0.06',
              }}
              className={classes.modal_huong_dan}>
              <div
                style={{ marginTop: 'calc(var(--viewport-width) *0.02' }}
                className={cx(classes.content, 'pro-light-shb')}>
                Khi đã có vé máy bay người chơi sẽ bay ra Hà Nội
              </div>
              <div
                style={{ marginTop: 'calc(var(--viewport-width) *0.015)' }}
                onClick={() => {
                  setNext(4);
                }}
                className={cx(classes.bnt_next, 'pro-light-shb')}>
                CHẠM VÀO ĐỂ TIẾP TỤC
              </div>
            </div>
          </div>
          <div className={classes.container_veMayBay}>
            <img className={classes.veMayBay} src={ASSET_GAME2.VE_MAY_BAY_1} />
            <img className={classes.veMayBay} src={ASSET_GAME2.VE_MAY_BAY_2} />
            <img className={classes.veMayBay} src={ASSET_GAME2.VE_MAY_BAY_3} />
            <img className={classes.veMayBay} src={ASSET_GAME2.VE_MAY_BAY_4} />
          </div>
          <img className={classes.gr_ruong} src={ASSET_GAME2.GROUD_RUONG} />
          <div className={cx(classes.content_text, 'pro-light-shb')}>Số lượt chơi: 5 lượt</div>
          <div className={classes.ball} style={{ backgroundImage: `url(${ASSET_GAME2.BAll})` }}></div>
        </div>
      )}
    </>
  );
};
