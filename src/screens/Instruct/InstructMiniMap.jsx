import classes from "./Minimap.module.css"
import cx from "classnames"
import Header from "../../components/Header"
import { ASSET_MINIMAP } from "../../utils/assetUtils"
import { useState } from "react"
import { useNavigate } from "react-router"


// type Prop = object
// : React.FC<Prop>
export const InstructMiniMap = () => {
    sessionStorage.setItem("intro_main","true")
    const [next, setNext] = useState(0)
    const navigate = useNavigate();
    return (
        <div
            style={{ backgroundImage: `url(${ASSET_MINIMAP.BG_MINNI_MAP})` }}
            className={classes.globalBg}>
            <Header
                showBackBtn={true}
                hideMute={false}
                hideShareBtn={true}
                content=""
                user={""}
            >
            </Header>

            {/* next 0 */}
            <div style={next == 0 ? {} : { display: "none" }}>
                <img
                    style={{ zIndex: "11", position: "absolute", marginTop: "calc(var(--viewport-width) * 1.52)" }}
                    className={classes.icon_bank} src={ASSET_MINIMAP.ICON_BANK} />
                <div className={classes.modal_huong_dan}>
                    <div className={cx(classes.content, 'pro-light-shb')}>Người chơi cần bắt đầu từ Chặng 1, 
                    thu thập được chữ "TÂM" để đạt điểu kiện chơi các chặng tiếp theo</div>
                    <div
                        onClick={() => { setNext(1) }}
                        className={cx(classes.bnt_next, 'pro-light-shb')}>CHẠM VÀO ĐỂ TIẾP TỤC</div>
                </div>
            </div>
            {/* next 1 */}
            <div style={next == 1 ? {} : { display: "none" }}>
                <img
                    style={{ zIndex: "11", position: "absolute", marginTop: "calc(var(--viewport-width) * 1.29)", marginLeft: "0px", }}
                    className={classes.icon_bank} src={ASSET_MINIMAP.ICON_SAN_BAY} />
                <div className={classes.modal_huong_dan_san_bay}>
                    <div style={{ fontSize: "calc(var(--viewport-width) * 0.045)" }} className={cx(classes.content, 'pro-light-shb')}>
                        Tương tự người chơi sẽ thu thập các chữ “TIN“ - Chặng 2, “TÍN“ - Chặng 3,
                        “TRI“ -Chặng 4, “TRÍ” -  Chặng 5,
                        “TẦM” - Chặng 6 và hoàn thành hành trình
                    </div>
                    <div
                        onClick={() => { setNext(2) }}
                        className={cx(classes.bnt_next, 'pro-light-shb')}>CHẠM VÀO ĐỂ TIẾP TỤC</div>
                </div>
            </div>
            {/* next 2 */}
            <div style={next == 2 ? {} : { display: "none" }}>
                <img
                    style={{ zIndex: "11", position: "absolute"}}
                    className={classes.bg_gio_qua_intro} src={ASSET_MINIMAP.BG_GIO_QUA_INTRO} />
                <div className={classes.modal_huong_dan_san_bay}>
                    <div style={{ fontSize: "calc(var(--viewport-width) * 0.045)" }} className={cx(classes.content, 'pro-light-shb')}>
                    Trong quá trình chơi bạn có thể quy đổi những chữ thu thập được lấy những phần quà hấp dẫn từ SHB
                    </div>
                    <div
                        onClick={() => { setNext(3) }}
                        className={cx(classes.bnt_next, 'pro-light-shb')}>CHẠM VÀO ĐỂ TIẾP TỤC</div>
                </div>
                <div className={classes.bo_suu_tap_intro}>
                    <img src={ASSET_MINIMAP.BO_SUU_TAP_INTRO} alt="" />
                </div>
            </div>
             {/* next 3 */}
            <div style={next == 3 ? {} : { display: "none" }}>
                <div className={classes.modal_huong_dan_san_bay}>
                    <div style={{ fontSize: "calc(var(--viewport-width) * 0.045)" }} className={cx(classes.content, 'pro-light-shb')}>
                        Bạn hãy hoàn thành đủ 6 chặng để nhận mã dự thưởng quay số cuối chương trình với cơ hội trúng giải thưởng lên tới
                        03 lượng vàng SJC của SHB nhé. Bên cạnh đó sẽ có các giải thưởng cực xịn trong quá trình chơi game cùng SHB
                    </div>
                    <div
                        onClick={() => { navigate({
                            pathname:"/mini-map",
                            search: window.location.search 
                        }) }}
                        className={cx(classes.bnt_next, 'pro-light-shb')}>CHẠM VÀO ĐỂ TIẾP TỤC</div>
                </div>
            </div>

            <img className={classes.logo_vuon_tam} src={ASSET_MINIMAP.LOGO_VUON_TAM} />
            <img className={classes.mini_map} src={next == 3 ? `${ASSET_MINIMAP.MINNI_MAP}` : `${ASSET_MINIMAP.MINI_MAP_CUSTOM}`} />
            <div className={cx(classes.content_text, 'pro-light-shb')}>Số lượt chơi: 5 lượt</div>
            <div className={classes.ball}
                style={{ backgroundImage: `url(${ASSET_MINIMAP.BAll})` }}>
            </div>
        </div>
    )
}