import classes from "./Instruct.module.css"
import { ASSETS } from "../../../utils/assetUtils"
import Header from "../../../components/Header"
import cx from "classnames"
import { useState } from "react"
import { useNavigate } from "react-router";
type Prop = object
export const InstructScreen5: React.FC<Prop> = () => {
    sessionStorage.setItem("intro5","true")
    const addres = "Thời đại số"
    const [next, setNext] = useState(0)
    const navigate = useNavigate()
    return (
        <div
            style={{ backgroundImage: `url(${ASSETS.BG5})`}}
            className={classes.globalBg}>
            <Header showBackBtn={false} hideMute={true} hideShareBtn={false} content="Chặng 5-" user={addres}></Header>

            {/* next 0 */}
            <div style={next == 0 ? {} : { display: "none" }}>
                <img className={classes.img_virrus} src="assets/ui5/virus.png"/>
                <img className={classes.img_hp} src="assets/ui5/HP.png"/>
                <div className={classes.modal_huong_dan_tri}>
                    <div className={cx(classes.content, 'pro-light-shb')}>
                        <img className={classes.img_tri} src="assets/ui5/Trí.png"/>
                        <div className={classes.text_tri}>
                            Bước vào kỷ nguyên số, SHB đã đầu tư TRÍ lực, nhân lực, áp dụng các nền tảng công nghệ số hiện đại, 
                            mang đến những sản phẩm số ưu việt với trải nghiệm mượt mà, an tâm. 
                            Bạn đã sẵn sàng chinh phục “Thời đại số” cùng SHB chưa?
                        </div>
                    </div>
                    <div
                        onClick={() => { setNext(1) }}
                        className={cx(classes.bnt_next_0, 'pro-light-shb')}>CHẠM VÀO ĐỂ TIẾP TỤC</div>
                </div>
            </div>
            {/* next 1 */}
            <div style={next == 1 ? {} : { display: "none" }}>
                <img className={classes.img_virrus} src="assets/ui5/virus.png"/>
                <img className={classes.img_hp} src="assets/ui5/HP.png"/>
                <img className={classes.img_may_bay} src="assets/ui5/maybay.png"/>
                <img className={classes.img_arrow_right} src="assets/ui5/arrow_right.png"/>
                <img className={classes.img_arrow_left} src="assets/ui5/arrow_left.png"/>
                <img className={classes.img_hand} src="assets/ui5/hand.png"/>
                <div className={classes.modal_huong_dan_next_1}>
                    <div className={cx(classes.content, 'pro-light-shb')}>
                        <div className={classes.text_next_1}>
                            <p>Người chơi ấn và giữ máy bay đi chuyển </p>
                            <p>qua bên trái hoặc phải để bắt đầu chơi.</p>
                        </div>
                    </div>
                    <div
                        onClick={() => { setNext(2) }}
                        className={cx(classes.bnt_next_1, 'pro-light-shb')}>CHẠM VÀO ĐỂ TIẾP TỤC
                    </div>
                </div>
            </div>
            {/* next 2 */}
            <div style={next == 2 ? {} : { display: "none" }}>
                <img className={classes.img_virrus} src="assets/ui5/virus.png"/>
                <img className={classes.img_virrus_next_2} src="assets/ui5/virus_ver_2.png"/>
                <img className={classes.img_hp} src="assets/ui5/HP.png"/>
                <img className={classes.img_may_bay} src="assets/ui5/maybay.png"/>
                <div className={classes.modal_huong_dan_next_2}>
                    <div className={cx(classes.content, 'pro-light-shb')}>
                        <div className={classes.text_next_2}>
                            <p>Di chuyển máy bay để tiêu diệt virus mã</p>
                            <p>độc đang định xâm nhập vào app của bạn</p>
                        </div>
                    </div>
                    <div
                        onClick={() => { setNext(3) }}
                        className={cx(classes.bnt_next_1, 'pro-light-shb')}>CHẠM VÀO ĐỂ TIẾP TỤC
                    </div>
                </div>
            </div>
            {/* next 3 */}
            <div style={next == 3 ? {} : { display: "none" }}>
                <img className={classes.img_virrus} src="assets/ui5/virus.png"/>
                <img className={classes.img_virrus_next_3} src="assets/ui5/virus_next_3.png"/>
                <img className={classes.img_hp} src="assets/ui5/HP.png"/>
                <img className={classes.img_may_bay} src="assets/ui5/maybay.png"/>
                <div className={classes.modal_huong_dan_next_2}>
                    <div className={cx(classes.content, 'pro-light-shb')}>
                        <div className={classes.text_next_2}>
                            <p>Nếu như virus mã độc chạm vào máy bay</p>
                            <p>bạn sẽ thua cuộc.</p>
                        </div>
                    </div>
                    <div
                        onClick={() => { setNext(4) }}
                        className={cx(classes.bnt_next_1, 'pro-light-shb')}>CHẠM VÀO ĐỂ TIẾP TỤC
                    </div>
                </div>
            </div>
            {/* {next 4} */}
            <div style={next == 4 ? {} : { display: "none" }}>
                <img className={classes.img_virrus} src="assets/ui5/virus.png"/>
                <img className={classes.img_hp} style={{zIndex:11}} src="assets/ui5/HP29.png"/>
                <img className={classes.img_may_bay} src="assets/ui5/maybay.png"/>
                <img className={classes.img_chuc_mung} src="assets/ui5/Chucmung.png"/>
                <img className={classes.btn_nhan_qua} src="assets/ui5/Button_nhan_qua.png"/>
                <div className={classes.modal_huong_dan_next_4_1}>
                    <div className={cx(classes.content, 'pro-light-shb')}>
                        <div className={classes.text_next_4_1}>
                            Bạn đã bảo vệ thành công ứng dụng SHB
                        </div>
                        <img className={classes.img_khien} src="assets/ui5/khien.png"/>
                    </div>
                </div>
                <div className={classes.modal_huong_dan_next_4_2}>
                    <div className={cx(classes.content, 'pro-light-shb')}>
                        <div className={classes.text_next_4}>
                        Tiêu diệt càng nhiều virus mã độc sẽ càng tăng năng lượng bảo vệ app của bạn. 
                        Hãy cố gắng tiêu diệt toàn bộ virus mã độc trong thời gian quy định. Bạn sẽ dành chiến thắng
                        </div>
                    </div>
                    <div
                        onClick={() => { setNext(5) }}
                        className={cx(classes.bnt_next_1, 'pro-light-shb')}>CHẠM VÀO ĐỂ TIẾP TỤC
                    </div>
                </div>
            </div>
            {/* {next 5} */}
            <div style={next == 5 ? {} : { display: "none" }}>
                <img className={classes.img_virrus} src="assets/ui5/virus.png"/>
                <img className={classes.img_tri_final} src="assets/ui5/chu_tri.png"/>
                <img className={classes.img_hp} src="assets/ui5/HP29.png"/>
                <img className={classes.img_may_bay} src="assets/ui5/maybay.png"/>
                <img className={classes.btn_2} src="assets/ui5/Button_2.png"/>
                <div className={classes.text_next_5}>
                   CHÚC MỪNG BẠN NHẬN ĐƯỢC 1 CHỮ TRÍ
                </div>
                <div className={classes.modal_huong_dan_next_5}>
                    <div className={cx(classes.content, 'pro-light-shb')}>
                        <div className={classes.text_next_2}>
                            Người chơi nhận được chữ TRÍ sẽ hoàn thành chặng.
                        </div>
                    </div>
                    <div
                        onClick={() => { setNext(5) ,navigate('/game5', { replace: true }) }}
                        className={cx(classes.bnt_next_1, 'pro-light-shb')}>BẮT ĐẦU CHƠI
                    </div>
                </div>
            </div>
            {/* <img src={ASSETS.LOGO_CUOC_KY} className={classes.cuoc_ky} />
            <img src={ASSETS.MAY_BAY} className={classes.may_bay} />
            <img src={ASSETS.SAO} className={classes.ngoi_sao} />
            <img src={next == 3?'':ASSETS.BANK} className={classes.bank} />
            <img className={classes.choi_ngay} src="assets/ui2/game3/bnt_tha.png"
                onClick={() => {
                    navigate({ pathname: '/mini-map', search: window.location.search });
                }}
            /> */}


            <div className={classes.ball}
                style={{ backgroundImage: `url(${ASSETS.BALL})` }}>
            </div>
        </div>
    )
}




