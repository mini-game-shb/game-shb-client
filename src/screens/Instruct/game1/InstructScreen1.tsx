import { useState } from 'react';
import { useNavigate } from 'react-router-dom';

import { ASSET_GAME1 } from '../../../utils/assetUtils';
import classes from './InstructScreen1.module.css';

type Props = {};

export const InstructScreen1: React.FC<Props> = () => {
  sessionStorage.setItem('intro1', 'true');
  const navigate = useNavigate();
  const [next, setNext] = useState(0);

  return (
    <div className={classes.container}>
      <div
        className={classes.ball}
        style={
          next === 0
            ? {
                backgroundImage: `url(${ASSET_GAME1.BAll})`,
              }
            : {
                backgroundImage: `url(${ASSET_GAME1.BAll})`,
                display: 'none',
              }
        }>
        <div className={classes.HD_CHOI} style={{ backgroundImage: `url(${ASSET_GAME1.HD_CHOI})` }}></div>
        <div className={classes.modal_huong_dan}>
          <div className={classes.content}>
            <div className={classes.title_tam}>
              <img src={ASSET_GAME1.TITLE_TAM} alt="" />
            </div>
            <p>
              Lấy TÂM làm gốc, kể từ khi thành lập vào năm 1993, SHB đã luôn dành trọn TÂM cống hiến cho khách hàng. Hãy
              trải nghiệm “Trọn Tâm khai mở” để khám phá công việc dựng xây những nền móng ban đầu của SHB bạn nhé!
            </p>
          </div>
          <div
            onClick={() => {
              setNext(1);
            }}
            className={classes.bnt_next}>
            CHẠM VÀO ĐỂ TIẾP TỤC
          </div>
        </div>
      </div>

      <div
        className={classes.ball}
        style={
          next === 1
            ? {
                backgroundImage: `url(${ASSET_GAME1.BAll})`,
              }
            : {
                backgroundImage: `url(${ASSET_GAME1.BAll})`,
                display: 'none',
              }
        }>
        <div className={classes.HD_CHOI} style={{ backgroundImage: `url(${ASSET_GAME1.HD_CHOI})` }}></div>
        <div className={classes.vong_quay}>
          <img src={ASSET_GAME1.VONG_QUAY} alt="" />
        </div>
        <div className={classes.modal_huong_dan_vong_quay}>
          <div className={classes.content_center}>Nguời chơi quay số và thu thập các nguyên liệu để xây dựng</div>
          <div
            onClick={() => {
              setNext(2);
            }}
            className={classes.bnt_next}>
            CHẠM VÀO ĐỂ TIẾP TỤC
          </div>
        </div>
      </div>

      <div
        className={classes.ball}
        style={
          next === 2
            ? {
                backgroundImage: `url(${ASSET_GAME1.BAll})`,
              }
            : {
                backgroundImage: `url(${ASSET_GAME1.BAll})`,
                display: 'none',
              }
        }>
        <div className={classes.HD_CHOI} style={{ backgroundImage: `url(${ASSET_GAME1.HD_CHOI})` }}></div>
        <img className={classes.vat_lieu} src={ASSET_GAME1.VL_HUONG_DAN} alt="" />
        <div style={{ top: 'calc(var(--viewport-width) * 0.45)' }} className={classes.modal_huong_dan_vong_quay}>
          <div className={classes.content_center}>
            Nguời chơi thu thập đủ 4 nguyên vật liêu: Cát, Gạch, Thép, Nước để xây ngân hàng
          </div>
          <div className={classes.bnt_next} onClick={() => setNext(3)}>
            CHẠM VÀO ĐỂ TIẾP TỤC
          </div>
        </div>
      </div>

      <div
        className={classes.ball}
        style={
          next === 3
            ? {
                backgroundImage: `url(${ASSET_GAME1.BAll})`,
              }
            : {
                backgroundImage: `url(${ASSET_GAME1.BAll})`,
                display: 'none',
              }
        }>
        <div className={classes.HD_CHOI} style={{ backgroundImage: `url(${ASSET_GAME1.HD_CHOI})` }}></div>
        <div className={classes.toa_nha}>
          <img src={ASSET_GAME1.TOA_NHA_KHANH_THANH_FULL} alt="" />
        </div>
        <div style={{ top: 'calc(var(--viewport-width)* 0.1)' }} className={classes.modal_huong_dan_vong_quay}>
          <div className={classes.content_center}>Nguời chơi nhấn nút khánh thành để hoàn thiện việc xây dựng ngân hàng</div>
          <div
            onClick={() => {
              setNext(4);
            }}
            className={classes.bnt_next}>
            CHẠM VÀO ĐỂ TIẾP TỤC
          </div>
        </div>
        <div
          className={classes.btn_khanh_thanh}
          onClick={() => {
            setNext(4);
          }}>
          <img src={ASSET_GAME1.BTN_KHANH_THANH} alt="" />
        </div>
      </div>

      <div
        className={classes.ball}
        style={
          next === 4
            ? {
                backgroundImage: `url(${ASSET_GAME1.BAll})`,
              }
            : {
                backgroundImage: `url(${ASSET_GAME1.BAll})`,
                display: 'none',
              }
        }>
        <div className={classes.HD_CHOI} style={{ backgroundImage: `url(${ASSET_GAME1.HD_CHOI})` }}></div>
        <div className={classes.tam}>
            <img src={ASSET_GAME1.TAM} alt="" />
        </div>
        <div
          style={{ top: 'calc(var(--viewport-width)* 1.2)'}}
          className={classes.modal_huong_dan}>
          <div className={classes.content_center}>
            Sau khi khánh thành ngân hàng, người chơi sẽ nhận được chữ TÂM và đủ điều kiện chơi Chặng 2 - TIN
          </div>
          <div
            onClick={() => {
              navigate('/game1', {
                replace: true,
              });
            }}
            className={classes.start}>
            <img src={ASSET_GAME1.BNT_START_PLAY} alt="" />
          </div>
        </div>
      </div>

      {/* <img src={ASSET_GAME1.BACKGROUND} alt="" className={classes['background']} /> */}
      {/* <MaterialComponent
                cat={0}
                thep={0}
                gach={0}
                nuoc={0} /> */}
    </div>
  );
};
