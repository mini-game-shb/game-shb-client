import cx from 'classnames';
import { useState } from 'react';
import { useNavigate } from 'react-router';

import Header from '../../../components/Header';
import { ASSET_GAME3 } from '../../../utils/assetUtils';
import classes from './Instruct.module.css';

type Prop = object;

export const InstructScreen3: React.FC<Prop> = () => {
  sessionStorage.setItem('intro3', 'true');
  const navigate = useNavigate();
  const user = 'Vu Thanh Tuyen';
  const [next, setNext] = useState(0);

  return (
    <div style={{ backgroundImage: `url(${ASSET_GAME3.BG_GAME3_MAIN})` }} className={classes.gbBackground}>
      <Header showBackBtn={false} hideMute={true} hideShareBtn={false} content="Xin chào" user={user}></Header>

      {/* next 0 */}
      <div style={next == 0 ? {} : { display: 'none' }}>
        <div className={classes.modal_huong_dan}>
          <div className={cx(classes.content, 'pro-light-shb')}>
            <img className={classes.img_tin} src="ASSET_GAME3/ui3/Tín.png" />
            <div className={classes.text_tin}>
              Bền bỉ và cam kết, SHB dần khẳng định được uy TÍN trên thị trường. Năm 2012, SHB mở chi nhánh tại Lào và
              Campuchia. Cùng SHB “Vươn ra khu vực” bạn nhé!
            </div>
          </div>
          <div
            onClick={() => {
              setNext(1);
            }}
            className={cx(classes.bnt_next, 'pro-light-shb')}>
            CHẠM VÀO ĐỂ TIẾP TỤC
          </div>
        </div>
      </div>
      {/* next 1 */}
      <div style={next == 1 ? {} : { display: 'none' }}>
        <img className={classes.may_bay_intruction} src="ASSET_GAME3/ui3/may_bay.png" />
        <div className={classes.modal_huong_dan_maybay}>
          <div className={cx(classes.content, 'pro-light-shb')}>
            <p>Người chơi điều khiển máy bay thả</p>
            <p>những ngôi sao khi bay qua</p>
            <p>Việt nam, Lào, Campuchia</p>
          </div>
          <div
            onClick={() => {
              setNext(2);
            }}
            className={cx(classes.btn_next_1, 'pro-light-shb')}>
            CHẠM VÀO ĐỂ TIẾP TỤC
          </div>
        </div>
      </div>
      {/* next 2 */}
      <div style={next == 2 ? {} : { display: 'none' }}>
        <img className={classes.may_bay_intruction} src="ASSET_GAME3/ui3/may_bay.png" />
        <img className={classes.ngoi_sao_intruction} src="ASSET_GAME3/ui3/ngoi_sao.png" />
        <img className={classes.btn_tha_sao} src="ASSET_GAME3/ui3/bnt_tha.png" />
        <div className={classes.modal_huong_dan_tha_sao}>
          <div className={cx(classes.content, 'pro-light-shb')}>Người chơi ấn nút Thả để thả ngôi sao từ máy bay</div>
          <div
            onClick={() => {
              setNext(3);
            }}
            className={cx(classes.btn_next_2, 'pro-light-shb')}>
            CHẠM VÀO ĐỂ TIẾP TỤC
          </div>
        </div>
      </div>
      {/* next 3 */}
      <div style={next == 3 ? {} : { display: 'none' }}>
        <img className={classes.de_toa_nha_intruction} src="ASSET_GAME3/ui3/De_toa_nha.png" />
        <img className={classes.btn_tha_sao} src="ASSET_GAME3/ui3/bnt_tha.png" />
        <img className={classes.ngoi_sao_next_3} src="ASSET_GAME3/ui3/ngoi_sao.png" />
        <div className={classes.modal_huong_dan_tha_sao_chi_tiet}>
          <div className={cx(classes.content, 'pro-light-shb')}>
            Khi ngôi sao rơi đúng vào vùng đất được đánh dấu, người chơi sẽ mở chi nhánh SHB thành công
          </div>
          <div
            onClick={() => {
              setNext(4);
            }}
            className={cx(classes.btn_next_3, 'pro-light-shb')}>
            CHẠM VÀO ĐỂ TIẾP TỤC
          </div>
        </div>
      </div>
      {/* {next 4} */}
      <div style={next == 4 ? {} : { display: 'none' }}>
        <img className={classes.icon_congratulations} src="ASSET_GAME3/ui3/icon_congratulations.png" />
        <img className={classes.bg_congratulations} src="ASSET_GAME3/ui3/bg_congratulations.png" />
        <div className={classes.text_congratulations}>Bạn đã xây dựng thành công các chi nhánh của SHB</div>
        <img className={classes.bank_congratulations} src="ASSET_GAME3/ui3/bank_congratulations.png" />
        <img className={classes.bnt_congratulations} src="ASSET_GAME3/ui3/bnt_nhan_qua.png" />
        <div className={classes.modal_huong_dan_thanh_cong}>
          <div className={cx(classes.content, 'pro-light-shb')}>
            Người chơi mở chi nhánh SHB thành công tại 3 đất nước Việt Nam, Lào, Campuchia sẽ chiến thắng trò chơi.
          </div>
          <div
            onClick={() => {
              setNext(4), navigate('/game3', { replace: true });
            }}
            className={cx(classes.btn_next_3, 'pro-light-shb')}>
            CHẠM VÀO ĐỂ TIẾP TỤC
          </div>
        </div>
      </div>
      {/* {next 5} */}

      <img src={next == 4 ? `ASSET_GAME3/ui3/cuoc_ky_sao.png` : `${ASSET_GAME3.LOGO_CUOC_KY}`} className={classes.cuoc_ky} />
      <img src={ASSET_GAME3.MAY_BAY} className={classes.may_bay} />
      <img src={ASSET_GAME3.SAO} className={classes.ngoi_sao} />
      <img src={next == 3 ? '' : ASSET_GAME3.BANK} className={classes.bank} />
      <img className={classes.choi_ngay} src="ASSET_GAME3/ui3/bnt_tha.png" />

      <div className={classes.ball} style={{ backgroundImage: `url(${ASSET_GAME3.BAll})` }}></div>
    </div>
  );
};
