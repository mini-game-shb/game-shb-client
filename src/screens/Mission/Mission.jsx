import { useNavigate } from 'react-router-dom';
import Header from '../../components/Header';
import { ASSETS } from '../../utils/assetUtils';
import classes from './Mission.css';

// type Props = {
//   //
// };
// : React.FC<Props>
export const Mission = () => {
  const navigate = useNavigate();
  return (
    <div className={classes.globalBg} style={{backgroundImage: `url(${ASSETS.BG_MISSION})`}} >
      <Header
        showBackBtn={true}
        hideMute={true}
        hideShareBtn={false}
        content="Xin chào"
        user={'Vũ Thanh Tuyền'}
        onBack={() => navigate({ pathname: '/', search: window.location.search })}></Header>
    </div>
  );
};
