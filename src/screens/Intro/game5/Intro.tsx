import cx from 'classnames';
import { useNavigate } from 'react-router';

import Header from '../../../components/Header';
import { ASSET_GAME5 } from '../../../utils/assetUtils';
import classes from './Intro.module.css';

type Prop = object;

export const Intro5: React.FC<Prop> = () => {
  const intro5 = sessionStorage.getItem('intro5');
  const navigate = useNavigate();
  const user = 'Vu Thanh Tuyen';
  return (
    <div style={{ backgroundImage: `url(${ASSET_GAME5.BG_Game_5})` }} className={classes.globalBg}>
      <Header
        showBackBtn={true}
        hideMute={true}
        hideShareBtn={true}
        content="Xin chào"
        user={user}
        onBack={() => navigate({ pathname: '/mini-map', search: window.location.search })}></Header>
      <img className={classes.logo_thoi_dai_so} src="assets/ui5/intro/Thoi_dai_so.png" />
      <img
        onClick={() => {
          intro5 ? navigate('/game5', { replace: true }) : navigate('/instruct5', { replace: true });
        }}
        className={classes.bnt_start}
        src="assets/ui6/intro/bnt_start.png"
      />
      <div className={cx(classes.content_text, 'pro-light-shb')}>Số lượt chơi: 5 lượt</div>
      <div className={cx(classes.content_text_2, 'pro-light-shb')} 
       onClick={() => {navigate('/instruct5', { replace: true });
      }}>Xem lại hướng dẫn</div>
    </div>
  );
};
