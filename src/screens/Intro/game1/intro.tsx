import { useNavigate } from 'react-router-dom';

import Header from '../../../components/Header';
import { ASSET_GAME1 } from '../../../utils/assetUtils';
import classes from './intro.module.css';

type Props = {
  //
};

export const Intro1: React.FC<Props> = () => {
  const navigate = useNavigate();
  const intro1 = sessionStorage.getItem('intro1');
  return (
    <div className={classes['container']} style={{ backgroundImage: `url(${ASSET_GAME1.BG_INTRO_2})` }}>
      <div className={classes['title-intro']}>
        <img className={classes['title-img']} src={ASSET_GAME1.TITLE_INTRO} />
      </div>
      <Header
        showBackBtn={true}
        hideMute={true}
        hideShareBtn={false}
        content="Xin chào"
        user={'Tiến tới thủ đô'}
        onBack={() => navigate({ pathname: '/instruct-mini-map', search: window.location.search })}></Header>
      <div className={classes['body']} >
        <img src={ASSET_GAME1.ICON_ROTATION} alt="" 
           
        />
      </div>
      <div className={classes['footer']}>
        <div className={classes['btn-khanh-thanh']} style={{ backgroundImage: `url(${ASSET_GAME1.BTN_BAT_DAU})` }} onClick={() => {
            intro1 ? navigate('/game1', { replace: true }) : navigate('/instruct1', { replace: true });
          }}></div>
        <div className={classes['text']}>Số lượt chơi: 5 lượt</div>
      </div>
    </div>
  );
};
