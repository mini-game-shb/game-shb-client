import cx from 'classnames';
import { useNavigate } from 'react-router';

import Header from '../../../components/Header';
import { ASSET_GAME6 } from '../../../utils/assetUtils';
import classes from './Intro.module.css';

type Prop = object;

export const Intro6: React.FC<Prop> = () => {
  const intro6 = sessionStorage.getItem('intro6');
  const navigate = useNavigate();
  const user = 'Vu Thanh Tuyen';
  return (
    <div style={{ backgroundImage: `url(${ASSET_GAME6.BG_GAME})` }} className={classes.globalBg}>
      <Header
        showBackBtn={true}
        hideMute={true}
        hideShareBtn={true}
        content="Xin chào"
        user={user}
        onBack={() => navigate({ pathname: '/mini-map', search: window.location.search })}></Header>
      <img className={classes.logo_30nam} src="assets/ui6/intro/logo_30nam.png" />
      <img
        onClick={() => {
          intro6 ? navigate('/game6', { replace: true }) : navigate('/instruct6', { replace: true });
        }}
        className={classes.bnt_start}
        src="assets/ui6/intro/bnt_start.png"
      />
      <div className={cx(classes.content_text, 'pro-light-shb')}>Số lượt chơi: 5 lượt</div>
    </div>
  );
};
