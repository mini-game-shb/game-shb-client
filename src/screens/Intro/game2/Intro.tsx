import cx from 'classnames';
import { useNavigate } from 'react-router';

import Header from '../../../components/Header';
import { ASSET_GAME2 } from '../../../utils/assetUtils';
import classes from './Intro.module.css';

type Prop = object;

export const Intro2: React.FC<Prop> = () => {
  const intro2 = sessionStorage.getItem('intro2');
  const navigate = useNavigate();
  const user = 'Vu Thanh Tuyen';
  return (
    <div style={{ backgroundImage: `url(${ASSET_GAME2.BG_GAME2})` }} className={classes.globalBg}>
      <Header
        showBackBtn={true}
        hideMute={true}
        hideShareBtn={true}
        content="Xin chào"
        user={user}
        onBack={() => navigate({ pathname: '/mini-map', search: window.location.search })}></Header>
      <img className={classes.bg_thu_do} src={ASSET_GAME2.BG_THU_DO} />
      <img className={classes.ruong} src={ASSET_GAME2.RUONG} />
      <img
        onClick={() => {
          intro2 ? navigate('/game2', { replace: true }) : navigate('/instruct2', { replace: true });
        }}
        className={classes.bnt_start}
        src={ASSET_GAME2.BTN_START_2}
      />
      <div className={cx(classes.content_text, 'pro-light-shb')}>Số lượt chơi: 5 lượt</div>
    </div>
  );
};
