import cx from 'classnames';
import { useNavigate } from 'react-router';

import Header from '../../../components/Header';
import { ASSET_GAME3 } from '../../../utils/assetUtils';
import classes from './Intro.module.css';

type Prop = object;

export const Intro3: React.FC<Prop> = () => {
  const intro3 = sessionStorage.getItem('intro3');
  const navigate = useNavigate();
  const user = 'Vu Thanh Tuyen';
  return (
    <div style={{ backgroundImage: `url(${ASSET_GAME3.BG_GAME3})` }} className={classes.globalBg}>
      <Header
        showBackBtn={true}
        hideMute={true}
        hideShareBtn={true}
        content="Xin chào"
        user={user}
        onBack={() => navigate({ pathname: '/mini-map', search: window.location.search })}></Header>
      <img className={classes.bg_thu_do} src={ASSET_GAME3.BG_KHU_VUC} />
      <img
        onClick={() => {
          intro3 ? navigate('/game3', { replace: true }) : navigate('/instruct3', { replace: true });
        }}
        className={classes.bnt_start}
        src="asset/ui2/bnt_start_game2.png"
      />
      <div className={cx(classes.content_text, 'pro-light-shb')}>Số lượt chơi: 5 lượt</div>
    </div>
  );
};
