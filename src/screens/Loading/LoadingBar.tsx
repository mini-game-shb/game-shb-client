import classes from './LoadingBar.module.css';

const LoadingBar: React.FC = () => {
  return (
    <div className={classes['container']}>
      <div
        className={classes['progress']}
        style={{
          width: '30%',
        }}></div>
    </div>
  );
};
export default LoadingBar;
