import cx from 'classnames';
import { Fragment, useCallback, useEffect } from 'react';
import { useLocation, useNavigate } from 'react-router';

import { useRefreshInventory, useRefreshUser, useStoreActions } from '../../hooks';
import { ASSET_GAME2, cacheAssets } from '../../utils/assetUtils';
import { soundUtils } from '../../utils/soundUtils';
import classes from './Loading.module.css';
import LoadingBar from './LoadingBar';

export type IScreen = "mini_map" | "game1" | "game2" | "game3" | "game4" |"game5"|"game6"

export default function Loading() {
  const {state} = useLocation();
  
  const navigate = useNavigate();
  const setNoticeModal = useStoreActions((actions) => actions.setNoticeModal);
  const setInstructVisible = useStoreActions((actions) => actions.setInstructVisible);

  const refreshInventory = useRefreshInventory();
  const refreshUser = useRefreshUser();

  const init = useCallback(async () => {
    await soundUtils.loadSound('/assets/sounds/', 'theme', true, 1);
    await soundUtils.loadSound('/assets/sounds/', 'flipSound', false, 1);
    await soundUtils.loadSound('/assets/sounds/', 'wrongSound', false, 1);
    await soundUtils.loadSound('/assets/sounds/', 'correctSound', false, 1);
    
    try {
      await cacheAssets(state||"mini_map")
      // await refreshUser();
      // await refreshInventory();
      // setInstructVisible(true);
      // navigate({ pathname: '/play', search: window.location.search });
      switch (state) {
        case 'mini_map':
          navigate({ pathname: '/instruct-main', search: window.location.search });
          break;
          case 'game1':
            navigate({ pathname: '/intro1', search: window.location.search });
          break;
          case 'game2':
            navigate({ pathname: '/intro2', search: window.location.search });
          break;
          case 'game3':
            navigate({ pathname: '/intro3', search: window.location.search });
          break;
          case 'game4':
            navigate({ pathname: '/game4', search: window.location.search });
          break;
          case 'game5':
            navigate({ pathname: '/intro5', search: window.location.search });
          break;
          case 'game6':
            navigate({ pathname: '/intro6', search: window.location.search });
          break;
        default:
          navigate({ pathname: '/instruct-main', search: window.location.search });
          break;
      }

      // navigate(
      //   { pathname: '/achievements', search: window.location.search },
      //   { state: { remainingTime: 30, remainingMoves: 30 } }
      // );
    } catch (e: any) {
      setNoticeModal({ notice: e.message, visible: true, action: '' });
    }
  }, [navigate, setNoticeModal, refreshUser, refreshInventory, setInstructVisible]);

  useEffect(() => {
    init();
  }, [init]);

  return (
    <Fragment>
      <div style={{ backgroundImage: `url(${ASSET_GAME2.BG_LOADING})` }} className={classes.globalBg}>
        <img src={ASSET_GAME2.LOGO_TRI_AN} className={classes.logo_tri_an} />
        <img src={ASSET_GAME2.LOGO_30} className={classes.logo_30} />
        <div className={cx(classes.title, 'pro-light-shb')}>Chờ chút nhé...</div>
        <div className={classes.loaddingBar}>
          <LoadingBar />
        </div>
      </div>
    </Fragment>
  );
}
