import { useEffect, useState } from 'react';

import Header from '../../components/Header';
import TinModal from '../../components/modal/game2/TinModal';
import { ASSETS } from '../../utils/assetUtils';
import classes from './CompleteGame2.module.css';

type Prop = object;

export const CompleteGame2: React.FC<Prop> = () => {
  const [tinModal, setTinModal] = useState(false);

  useEffect(() => {
    setTimeout(() => {
      setTinModal(true);
    }, 1000 * 2);
  }, []);

  const addres = 'Tiến tới thủ đô';
  return (
    <div style={{ backgroundImage: `url(${ASSETS.HOAN_THANH_2})` }} className={classes.globalBg}>
      <Header showBackBtn={true} hideMute={true} hideShareBtn={true} content="chặng 2-" user={addres}></Header>
      <img className={classes.ban_do} src={ASSETS.BAN_DO_2} />
      <TinModal
        isOpen={tinModal}
        closeModal={() => {
          setTinModal(false);
        }}
      />
    </div>
  );
};
