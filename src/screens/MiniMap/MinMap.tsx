import cx from 'classnames';
import { useNavigate } from 'react-router';

import Header from '../../components/Header';
import { ASSET_MINIMAP } from '../../utils/assetUtils';
import classes from './MiniMap.module.css';

type Prop = object;

export const MiniMap: React.FC<Prop> = () => {
  sessionStorage.setItem('intro_map', 'true');

  const navigate = useNavigate();
  const ListGame = {
    game2: true,
    game3: true,
    game4: true,
    game5: true,
    game6: true,
  };
  return (
    <div style={{ backgroundImage: `url(${ASSET_MINIMAP.BG_MINNI_MAP})` }} className={classes.globalBg}>
      <Header
        showBackBtn={true}
        hideMute={false}
        hideShareBtn={true}
        content=""
        user={''}
        onBack={() => navigate({ pathname: '/instruct-main', search: window.location.search })}></Header>
      <img className={classes.logo_vuon_tam} src={ASSET_MINIMAP.LOGO_VUON_TAM} />
      <div>
        <img className={classes.mini_map} src={ASSET_MINIMAP.MINI_MAP_MANI} />
      </div>
      {/* game 2 */}
      <div>
        <img
          src={ListGame.game2 == false ? ASSET_MINIMAP.ICON_SAN_BAY_LOCK : ASSET_MINIMAP.ICON_SAN_BAY}
          className={classes.icon_san_bay}
          onClick={() =>  navigate({ pathname: '/' },{state:"game2"})}
        />
      </div>
      {/* game 1 */}
      <div>
          <img 
          onClick={() =>  navigate({ pathname: '/' },{state:"game1"})}
          src={ASSET_MINIMAP.ICON_BANK} className={classes.icon_bank} />
      </div>
      {/* game 3 */}
      <div>
        <img
          src={ListGame.game3 == false ? ASSET_MINIMAP.ICON_ATM_LOCK : ASSET_MINIMAP.ICON_ATM}
          className={classes.icon_atm}
          onClick={() =>  navigate({ pathname: '/' },{state:"game3"})}
        />
      </div>
      {/* game 4 */}
      <div>
        <img
          src={ListGame.game4 == false ? ASSET_MINIMAP.ICON_DA_BANH_LOCK : ASSET_MINIMAP.ICON_DA_BANH}
          className={classes.icon_da_banh}
          onClick={() =>  navigate({ pathname: '/' },{state:"game4"})}
        />
      </div>
      {/* game 5 */}
      <div>
        <img
          src={ListGame.game5 == false ? ASSET_MINIMAP.ICON_MAY_BAY_LOCK : ASSET_MINIMAP.ICON_MAY_BAY}
          className={classes.icon_may_bay}
          onClick={() =>  navigate({ pathname: '/' },{state:"game5"})}
        />
      </div>
      {/* game 6 */}
      <div>
        <img
          src={ListGame.game6 == false ? ASSET_MINIMAP.ICON_MO_QUA_LOCK : ASSET_MINIMAP.ICON_MO_QUA}
          className={classes.icon_mo_qua}
          onClick={() =>  navigate({ pathname: '/' },{state:"game6"})}
        />
      </div>

      <div className={cx(classes.content_text, 'pro-light-shb')}>Số lượt chơi: 5 lượt</div>
    </div>
  );
};
