import cx from 'classnames';
import { useState } from 'react';
import { useNavigate } from 'react-router';

import Header from '../../components/Header';
import { ASSETS } from '../../utils/assetUtils';
import classes from './Main.module.css';

type Prop = object;

export const MainSceen: React.FC<Prop> = () => {
  const introMap = sessionStorage.getItem('intro_map');
  const introMain = sessionStorage.getItem('intro_main');
  const navigate = useNavigate();
  const user = 'Vu Thanh Tuyen';
  const [next, setNext] = useState(0);
  const [modal, setModal] = useState(false);

  return (
    <div style={{ backgroundImage: `url(${ASSETS.BG_MAIN})` }} className={classes.gbBackground}>
      <Header showBackBtn={false} hideMute={true} hideShareBtn={false} content="Xin chào" user={user}></Header>

      {/* next 0 */}
      <div style={next == 0 && !introMain ? {} : { display: 'none' }}>
        <img className={classes.choi_ngay_instruct} src="assets/ui2/main/choi_ngay.png" />
        <div className={classes.modal_huong_dan}>
          <div className={cx(classes.content, 'pro-light-shb')}>
            Người chơi ấn vào Chơi ngay để bắt đầu khám phá Hành trình từ Tâm vươn tầm cùng SHB
          </div>
          <div
            onClick={() => {
              setNext(1);
            }}
            className={cx(classes.bnt_next, 'pro-light-shb')}>
            CHẠM VÀO ĐỂ TIẾP TỤC
          </div>
        </div>
      </div>
      {/* next 1 */}
      <div style={next == 1 && !introMain ? {} : { display: 'none' }}>
        <img className={classes.nhiem_vu_instruct} src="assets/ui2/main/nhiem_vu.png" />
        <div className={classes.modal_huong_dan_nhiem_vu}>
          <div className={cx(classes.content, 'pro-light-shb')}>
            Người chơi ấn vào Nhiệm vụ để thực hiện nhiệm vụ và kiếm lượt chơi
          </div>
          <div
            onClick={() => {
              setNext(2);
            }}
            className={cx(classes.bnt_next, 'pro-light-shb')}>
            CHẠM VÀO ĐỂ TIẾP TỤC
          </div>
        </div>
      </div>
      {/* next 2 */}
      <div style={next == 2 && !introMain ? {} : { display: 'none' }}>
        <img className={classes.gio_qua_instruct} src="assets/ui2/main/qua_cua_ban.png" />
        <div className={classes.modal_huong_dan_gio_qua}>
          <div className={cx(classes.content, 'pro-light-shb')}>
            Người chơi ấn vào Giỏ quà để kiểm tra và theo dõi các phần quà
          </div>
          <div
            onClick={() => {
              setNext(3);
            }}
            className={cx(classes.bnt_next, 'pro-light-shb')}>
            CHẠM VÀO ĐỂ TIẾP TỤC
          </div>
        </div>
      </div>
      {/* next 3 */}
      <div style={next == 3 && !introMain ? {} : { display: 'none' }}>
        <img className={classes.giai_thuong_instruct} src="assets/ui2/main/co_cau_giai_thuong.png" />
        <div className={classes.modal_huong_dan_giai_thuong}>
          <div className={cx(classes.content, 'pro-light-shb')}>
            Người chơi ấn vào Cơ cấu giải thưởng để xem danh sách giải thưởng giái trị của chương trình
          </div>
          <div
            onClick={() => {
              setNext(4);
              setTimeout(() => {
                setModal(true);
              }, 1000 * 2);
            }}
            className={cx(classes.bnt_next, 'pro-light-shb')}>
            CHẠM VÀO ĐỂ TIẾP TỤC
          </div>
        </div>
      </div>
      {/* {next 5} */}
      {modal && (
        <div>
          <div className={classes.ball} style={{ backgroundImage: `url(${ASSETS.BAll})` }}></div>
          <img className={classes.bg_nhan_thuong} src="assets/ui2/main/bg_nhan_thuong.png" />
          <img className={classes.nhan_thuong} src="assets/ui2/main/nhan_thuong.png" />
          <img className={classes.number_one} src="assets/ui2/main/numberOne.png" />
          <div className={cx(classes.cong_luot, 'pro-light-shb')}>lượt chơi</div>
          <div
            onClick={() => {
              setNext(4);
              setModal(false);
            }}>
            <img className={classes.bnt_choi_ngay} src="assets/ui2/main/bnt_vao_choi_ngay.png" />
          </div>
        </div>
      )}
      {/* {next 4} */}
      <img src={ASSETS.LOGO_TRI_AN} className={classes.logo_tri_an} />
      <div className={cx(classes.turn, 'pro-light-shb')}>Số lượt chơi: 5 lượt</div>
      <img
        className={classes.choi_ngay}
        src="assets/ui2/main/choi_ngay.png"
        onClick={() => {
          introMap
            ? navigate({ pathname: '/mini-map', search: window.location.search })
            : navigate({ pathname: '/instruct-mini-map', search: window.location.search });
        }}
      />
      <img className={classes.rule} src="assets/ui2/main/nhiem_vu.png" />
      <img className={classes.rule} src="assets/ui2/main/qua_cua_ban.png" />
      <img className={classes.rule} src="assets/ui2/main/co_cau_giai_thuong.png" />
      <label className={classes['test']}>
        <input type="checkbox" name="checkbox" id="1" />
        <span className={classes['checkmark']}></span>
        Tôi đồng ý với thể lệ chương trình
      </label>

      <div
        className={classes.ball}
        style={next == 4 || introMain ? { display: 'none' } : { backgroundImage: `url(${ASSETS.BAll})` }}></div>
    </div>
  );
};
