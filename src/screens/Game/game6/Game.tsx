import { Fragment, useCallback, useEffect, useRef, useState } from 'react';
import { useNavigate } from 'react-router';

import Header from '../../../components/Header';
import RewardCash from '../../../components/modal/game6/RewardCash';
import RewardVoucherModal from '../../../components/modal/game6/RewardVoucherModal';
import TamModal from '../../../components/modal/game6/TamModal';
import { GameEndedData, init } from '../../../game/game6';
import { useRefreshInventory, useRefreshUser } from '../../../hooks';
import { GAME_EVENTS } from '../../../utils/constants';
import eventEmitter from '../../../utils/eventEmitter';
import { soundUtils } from '../../../utils/soundUtils';

const arrCash = ['CASH_10K', 'CASH_20K', 'CASH_50K', 'CASH_100K', 'CASH_200K', 'CASH_500K'];

export function Game6() {
  const addres = 'Thời đại số';
  const [gameInited, setGameInited] = useState(false);
  const [voucherModal, setVoucherModal] = useState(false);
  const [tamModal, setTamModal] = useState(false);
  const [cashModal, setCashModal] = useState(false);
  const [typeCash, setTypeCash] = useState<string>();
  const navigate = useNavigate();

  const gameStarted = useRef(false);

  const game = useRef<Awaited<ReturnType<typeof init>>>();
  const refreshUser = useRefreshUser();
  const refreshInventory = useRefreshInventory();

  const initGame = useCallback(async () => {
    // hàm để chỉnh cấu hình level 1-level 2 - level 3
    const gameInstance = await init();
    if (gameStarted.current) {
      gameInstance.start();
      gameStarted.current = false;
    }
    game.current = gameInstance;
  }, []);

  const gameEndedLn = useRef<any>();

  useEffect(() => {
    return () => {
      if (gameInited) {
        gameStarted.current = false;
        if (game.current) {
          game.current.dispose();
          game.current = undefined;
        }
        if (gameEndedLn.current) {
          eventEmitter.removeListener(GAME_EVENTS.GAME_ENDED, gameEndedLn.current);
          gameEndedLn.current = null;
        }
      }
    };
  }, [gameInited, gameEndedLn]);

  useEffect(() => {
    if (gameInited) {
      return;
    }
    setGameInited(true);
    initGame();

    const showPopup = async (data: GameEndedData) => {
      if (data.type === 'voucher') {
        setVoucherModal(true);
      }
      if (data.type === 'tam') {
        setTamModal(true);
      }

      if (data.type === 'tien') {
        const soTien = Math.floor(Math.random() * 5);
        setTypeCash(arrCash[soTien]);
        setCashModal(true);
      }
    };
    eventEmitter.addListener(GAME_EVENTS.GAME_ENDED, showPopup);
  }, [gameInited, initGame, navigate, gameEndedLn, refreshUser, refreshInventory]);

  soundUtils.play('flipSound');

  return (
    <Fragment>
      <Header
        showBackBtn={true}
        hideMute={true}
        hideShareBtn={true}
        content="chặng 6-"
        user={addres}
        onBack={() => navigate({ pathname: '/intro6', search: window.location.search })}></Header>
      <RewardVoucherModal
        isOpen={voucherModal}
        closeModal={() => {
          setVoucherModal(false);
          game.current!.reset();
        }}
      />
      <TamModal
        isOpen={tamModal}
        closeModal={() => {
          setTamModal(false);
          game.current!.start();
        }}
      />
      <RewardCash
        isOpen={cashModal}
        closeModal={() => {
          setCashModal(false), game.current!.reset();
        }}
        rewardValue={typeCash!}
      />
    </Fragment>
  );
}
