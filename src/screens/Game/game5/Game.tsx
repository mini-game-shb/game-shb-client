import { Fragment, useCallback, useEffect, useRef, useState } from 'react';
import { useNavigate } from 'react-router';

import Header from '../../../components/Header';
import ChucMungKhien from '../../../components/modal/game5/ChucMungKhien';
import HetLuotChoi from '../../../components/modal/game5/HetLuotChoi';
import HetThoiGian from '../../../components/modal/game5/HetThoiGian';
import NhanChu from '../../../components/modal/game5/NhanChu';
import ThuLaiNao from '../../../components/modal/game5/ThuLaiNao';
import TrungThuongTien from '../../../components/modal/game5/TrungThuongTien';
import { GameEndedData, init } from '../../../game/game5';
import { useRefreshInventory, useRefreshUser } from '../../../hooks';
import { GAME_EVENTS } from '../../../utils/constants';
import eventEmitter from '../../../utils/eventEmitter';
import { soundUtils } from '../../../utils/soundUtils';

const arrCash = ['CASH_10K', 'CASH_20K', 'CASH_50K', 'CASH_100K', 'CASH_200K', 'CASH_500K'];

let luot = 0;

export function Game5() {
  const [gameInited, setGameInited] = useState(false);
  const [timeOutModal, setTimeOutModal] = useState(false);
  const [winModal, setWinModal] = useState(false);
  const [loseModal, setLoseModal] = useState(false);
  const [triModal, setTriModal] = useState(false);
  const [cashModal, setCashModal] = useState(false);
  const [hetLuotModal, setHetLuotModal] = useState(false);
  const navigate = useNavigate();
  const [typeCash, setTypeCash] = useState<string>();

  const gameStarted = useRef(false);

  const game = useRef<Awaited<ReturnType<typeof init>>>();
  const refreshUser = useRefreshUser();
  const refreshInventory = useRefreshInventory();

  const initGame = useCallback(async () => {
    const gameInstance = await init();
    if (gameStarted.current) {
      gameInstance.start();
      gameStarted.current = false;
    }
    game.current = gameInstance;
  }, []);

  const gameEndedLn = useRef<any>();

  useEffect(() => {
    return () => {
      if (gameInited) {
        gameStarted.current = false;
        if (game.current) {
          game.current.dispose();
          game.current = undefined;
        }
        if (gameEndedLn.current) {
          eventEmitter.removeListener(GAME_EVENTS.GAME_ENDED, gameEndedLn.current);
          gameEndedLn.current = null;
        }
      }
    };
  }, [gameInited, gameEndedLn]);

  const handleGetThuong = () => {
    if (luot % 2 == 0) {
      setTriModal(true);
    } else {
      const soTien = Math.floor(Math.random() * 5);
      setTypeCash(arrCash[soTien]);
      setCashModal(true);
    }
  };

  useEffect(() => {
    if (gameInited) {
      return;
    }
    setGameInited(true);
    initGame();

    const showPopup = async (data: GameEndedData) => {
      if (data.type === 'lose') {
        setLoseModal(true);
      }
      if (data.type === 'timeOut') {
        setTimeOutModal(true);
      }
      if (data.type === 'win') {
        luot++;
        setWinModal(true);
      }
    };

    eventEmitter.addListener(GAME_EVENTS.GAME_ENDED, showPopup);
  }, [gameInited, initGame, navigate, gameEndedLn, refreshUser, refreshInventory]);

  soundUtils.play('flipSound');

  return (
    <Fragment>
      <Header
        showBackBtn={true}
        hideMute={true}
        hideShareBtn={false}
        content="Xin chào"
        user={'Tiến tới thủ đô'}
        onBack={() => navigate({ pathname: '/intro5', search: window.location.search })}></Header>
      <HetThoiGian
        isOpen={timeOutModal}
        closeModal={() => {
          setTimeOutModal(false), game.current!.reset();
        }}
      />
      <ChucMungKhien
        isOpen={winModal}
        closeModal={() => {
          setWinModal(false), handleGetThuong();
        }}
      />
      <TrungThuongTien
        isOpen={cashModal}
        closeModal={() => {
          setCashModal(false), luot === 3 ? setHetLuotModal(true) : game.current!.reset();
        }}
        rewardValue={typeCash!}
      />
      <HetLuotChoi
        isOpen={hetLuotModal}
        closeModal={() => {
          setHetLuotModal(false);
        }}
      />
      <NhanChu
        isOpen={triModal}
        closeModal={() => {
          setTriModal(false), game.current!.reset();
        }}
        onNext={() => navigate({ pathname: '/instruct6', search: window.location.search })}
      />
      <ThuLaiNao
        isOpen={loseModal}
        closeModal={() => {
          setLoseModal(false), game.current!.reset();
        }}
      />
    </Fragment>
  );
}
