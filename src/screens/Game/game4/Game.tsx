// import clsx from 'clsx';
import { Fragment, useCallback, useEffect, useRef, useState } from 'react';
import { useNavigate } from 'react-router';

import Header from '../../../components/Header';
import { GameEndedData, init } from '../../../game/game4';
import { useRefreshInventory, useRefreshUser } from '../../../hooks';
import { GAME_EVENTS } from '../../../utils/constants';
import eventEmitter from '../../../utils/eventEmitter';
import { soundUtils } from '../../../utils/soundUtils';
import HetThoiGian from '../../../components/modal/game4/HetThoiGian';
import TuyetVoi from '../../../components/modal/game4/TuyetVoi';
import TrungTien from '../../../components/modal/game4/TrungTien';
import HetLuotChoi from '../../../components/modal/game4/HetLuotChoi';
import Chang5Tri from '../../../components/modal/game4/Chang5Tri';

const arrCash = [
  "CASH_10K", "CASH_20K", "CASH_50K", "CASH_100K", "CASH_200K", "CASH_500K"
]

// let luot = 16;

export function Game4() {
  const [gameInited, setGameInited] = useState(false);
  const [modalHetThoiGian, setModalHetThoiGian] = useState(false);
  const [modalTuyetVoi, setModalTuyetVoi] = useState(false);
  const [modalHetLuotChoi, setModalHetLuotChoi] = useState(false);
  const [cashModal, setCashModal] = useState(false);
  const [modalChang5Tri, setModalChang5Tri] = useState(false);
  const navigate = useNavigate();
  const [typeCash, setTypeCash] = useState<string>();

  const gameStarted = useRef(false);

  const game = useRef<Awaited<ReturnType<typeof init>>>();
  const refreshUser = useRefreshUser();
  const refreshInventory = useRefreshInventory();

  const initGame = useCallback(async () => {
    const gameInstance = await init();
    if (gameStarted.current) {
      gameInstance.start();
      gameStarted.current = false;
    }
    game.current = gameInstance;
  }, []);


  const gameEndedLn = useRef<any>();

  useEffect(() => {
    return () => {
      if (gameInited) {
        gameStarted.current = false;
        if (game.current) {
          game.current.dispose();
          game.current = undefined;
        }
        if (gameEndedLn.current) {
          eventEmitter.removeListener(GAME_EVENTS.GAME_ENDED, gameEndedLn.current);
          gameEndedLn.current = null;
        }
      }
    };
  }, [gameInited, gameEndedLn]);

  useEffect(() => {
    if (gameInited) {
      return;
    }
    setGameInited(true);
    initGame();

    const showPopup = async (data: GameEndedData) => {
      // luot -= 1
     
      //windy
      if (data.type === 'timeEnd' && modalTuyetVoi === false) {
        setModalHetThoiGian(true)
      }
      
      if (data.type === 'hetLuot') {
        setModalHetLuotChoi(true)
      }

      if (data.type === 'winner') {
        setModalTuyetVoi(true)
      }

      if (data.type === 'chang5') {
        setModalChang5Tri(true)
      }
// windy
      if (data.type === 'tien') {
        const soTien = Math.floor(Math.random() * 5);
        setTypeCash(arrCash[soTien]);
        setCashModal(true);
      }
    };

    eventEmitter.addListener(GAME_EVENTS.GAME_ENDED, showPopup);
  }, [gameInited, initGame, navigate, gameEndedLn, refreshUser, refreshInventory]);


  soundUtils.play('flipSound');

  return (
    <Fragment>
      <Header
        showBackBtn={true}
        hideMute={true}
        hideShareBtn={false}
        content="Xin chào"
        user={'Tiến tới thủ đô'}
        onBack={() => navigate({ pathname: '/instruct-mini-map', search: window.location.search })}></Header>
      <HetThoiGian isOpen={modalHetThoiGian} closeModal={() => {setModalHetThoiGian(false);  game.current!.reset()}} />
      <TuyetVoi isOpen={modalTuyetVoi} closeModal={() => setModalTuyetVoi(false)} />
      <TrungTien isOpen={cashModal} closeModal={() => setModalTuyetVoi(false)} rewardValue={typeCash!}/>
      <HetLuotChoi isOpen={modalHetLuotChoi} closeModal={() => setModalHetLuotChoi(false)} />
      <Chang5Tri isOpen={modalChang5Tri} closeModal={() => setModalChang5Tri(false)} />
    </Fragment>
  );
}
