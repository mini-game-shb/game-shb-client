import { Fragment, useCallback, useEffect, useRef, useState } from 'react';
import { useNavigate } from 'react-router';

import Header from '../../../components/Header';
import HetLuotChoi from '../../../components/modal/game3/HetLuotChoi';
import RewardVoucherModal from '../../../components/modal/game3/RewardVoucherModal';
import TinModal from '../../../components/modal/game3/TinModal';
import TrungTien from '../../../components/modal/game3/TrungTien';
import VictoryModal from '../../../components/modal/game3/VictoryModal';
import { GameEndedData, init } from '../../../game/game3';
import { useRefreshInventory, useRefreshUser } from '../../../hooks';
import { ASSETS } from '../../../utils/assetUtils';
import { GAME_EVENTS } from '../../../utils/constants';
import eventEmitter from '../../../utils/eventEmitter';
import { soundUtils } from '../../../utils/soundUtils';
import classes from './Game.module.css';

const arrCash = ['CASH_10K', 'CASH_20K', 'CASH_50K', 'CASH_100K', 'CASH_200K', 'CASH_500K'];

let luot = 0;
let ve1 = false;
let ve2 = false;
let ve3 = false;

export function Game3() {
  const [gameInited, setGameInited] = useState(false);
  const [victoryModal, setVictoryModal] = useState(false);
  const [tinModal, setTinModal] = useState(false);
  const [voucherModal, setVoucherModal] = useState(false);
  const [hetLuotModal, setHetLuotModal] = useState(false);
  const [cashModal, setCashModal] = useState(false);
  const navigate = useNavigate();
  const [_typeVe, setTypeVe] = useState<string>();
  const [typeCash, setTypeCash] = useState<string>();

  const gameStarted = useRef(false);

  const game = useRef<Awaited<ReturnType<typeof init>>>();
  const refreshUser = useRefreshUser();
  const refreshInventory = useRefreshInventory();

  const initGame = useCallback(async () => {
    const gameInstance = await init();
    if (gameStarted.current) {
      gameInstance.start();
      gameStarted.current = false;
    }
    game.current = gameInstance;
  }, []);

  const gameEndedLn = useRef<any>();

  useEffect(() => {
    return () => {
      if (gameInited) {
        gameStarted.current = false;
        if (game.current) {
          game.current.dispose();
          game.current = undefined;
        }
        if (gameEndedLn.current) {
          eventEmitter.removeListener(GAME_EVENTS.GAME_ENDED, gameEndedLn.current);
          gameEndedLn.current = null;
        }
      }
    };
  }, [gameInited, gameEndedLn]);

  useEffect(() => {
    if (gameInited) {
      return;
    }
    setGameInited(true);
    initGame();

    const showPopup = async (data: GameEndedData) => {
      if (data.type === 'hetLuot') {
        setHetLuotModal(true);
      }
      if (data.type === 've1') {
        ve1 = true;
        setTypeVe(data.type);
      }
      if (data.type === 've2') {
        ve2 = true;
        setTypeVe(data.type);
      }
      if (data.type === 've3') {
        ve3 = true;
        setTypeVe(data.type);
      }
      if (ve1 && ve2 && ve3) {
        luot += 1;
        setVictoryModal(true);
      }
      
    };

    eventEmitter.addListener(GAME_EVENTS.GAME_ENDED, showPopup);
  }, [gameInited, initGame, navigate, gameEndedLn, refreshUser, refreshInventory]);

  const handleReset = () => {
    ve1 = false;
    ve2 = false;
    ve3 = false;
    game.current!.reset();
  };

  const handleThuong = () => {
    const soTien = Math.floor(Math.random() * 5);
    setTypeCash(arrCash[soTien]);
    switch (luot) {
      case 1:
        setCashModal(true);
        break;
      case 2:
        setVoucherModal(true);
        break;

      case 3:
        setTinModal(true);
        break;

      default:
        break;
    }
  };

  soundUtils.play('flipSound');

  return (
    <Fragment>
      <Header
        showBackBtn={true}
        hideMute={true}
        hideShareBtn={false}
        content="Xin chào"
        user={'Tiến tới thủ đô'}
        onBack={() => navigate({ pathname: '/intro3', search: window.location.search })}></Header>
      <div className={classes.container_veMayBay}>
        <img className={classes.veMayBay} src={ve1 == false ? ASSETS.VIET_NAM : ASSETS.VIET_NAM_SELECTED} />
        <img className={classes.veMayBay} src={ve2 == false ? ASSETS.LAO : ASSETS.LAO_SELECTED} />
        <img className={classes.veMayBay} src={ve3 == false ? ASSETS.CAMPUCHIA : ASSETS.CAMPUCHIA_SELECTED} />
      </div>
      <VictoryModal
        isOpen={victoryModal}
        closeModal={() => {
          setVictoryModal(false), handleThuong();
        }}
      />
      <RewardVoucherModal
        isOpen={voucherModal}
        closeModal={() => {
          setVoucherModal(false), handleReset();
        }}
      />
      <TinModal
        isOpen={tinModal}
        closeModal={() => {
          setTinModal(false), handleReset();
        }}
      />
      <TrungTien
        isOpen={cashModal}
        closeModal={() => {
          setCashModal(false), handleReset();
        }}
        rewardValue={typeCash!}
      />
      <HetLuotChoi
        isOpen={hetLuotModal}
        closeModal={() => {
          setHetLuotModal(false), handleReset();
        }}
      />
    </Fragment>
  );
}
