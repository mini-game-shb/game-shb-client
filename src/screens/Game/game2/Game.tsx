// import clsx from 'clsx';
import cx from 'classnames';
import { Fragment, useCallback, useEffect, useRef, useState } from 'react';
import { useNavigate } from 'react-router';

import Header from '../../../components/Header';
import Failure from '../../../components/modal/game2/Failure';
import HomVe from '../../../components/modal/game2/HomVe';
import PlaneTickets from '../../../components/modal/game2/PlaneTickets';
import RewardCash from '../../../components/modal/game2/RewardCash';
import RewardVoucherModal from '../../../components/modal/game2/RewardVoucherModal';
import { GameEndedData, init } from '../../../game/game2';
import { useRefreshInventory, useRefreshUser } from '../../../hooks';
import { ASSETS } from '../../../utils/assetUtils';
import { GAME_EVENTS } from '../../../utils/constants';
import eventEmitter from '../../../utils/eventEmitter';
import { soundUtils } from '../../../utils/soundUtils';
import classes from './Game.module.css';

const arrCash = ['CASH_10K', 'CASH_20K', 'CASH_50K', 'CASH_100K', 'CASH_200K', 'CASH_500K'];

let luot = 16;

export function Game2() {
  const [gameInited, setGameInited] = useState(false);
  const [modalThuong, setModalThuong] = useState(false);
  const [voucherModal, setVoucherModal] = useState(false);
  const [falseModal, setFalseModal] = useState(false);
  const [planeModal, setPlaneModal] = useState(false);
  const [cashModal, setCashModal] = useState(false);
  const navigate = useNavigate();
  const [typeVe, setTypeVe] = useState<string>();
  const [typeCash, setTypeCash] = useState<string>();

  const gameStarted = useRef(false);

  const game = useRef<Awaited<ReturnType<typeof init>>>();
  const refreshUser = useRefreshUser();
  const refreshInventory = useRefreshInventory();

  const [ve1, setVe1] = useState(0)
  const [ve2, setVe2] = useState(0)
  const [ve3, setVe3] = useState(0)
  const [ve4, setVe4] = useState(0)

  const initGame = useCallback(async () => {
    const gameInstance = await init();
    if (gameStarted.current) {
      gameInstance.start();
      gameStarted.current = false;
    }
    game.current = gameInstance;
  }, []);

  const gameEndedLn = useRef<any>();

  useEffect(() => {
    return () => {
      if (gameInited) {
        gameStarted.current = false;
        if (game.current) {
          game.current.dispose();
          game.current = undefined;
        }
        if (gameEndedLn.current) {
          eventEmitter.removeListener(GAME_EVENTS.GAME_ENDED, gameEndedLn.current);
          gameEndedLn.current = null;
        }
      }
    };
  }, [gameInited, gameEndedLn]);


  const handleReset = () => {
    setVe1(0)
    setVe2(0)
    setVe3(0);
    setVe4(0);
    luot = 16
  }

  useEffect(() => {
    if (gameInited) {
      return;
    }
    setGameInited(true);
    initGame();

    const showPopup = async (data: GameEndedData) => {
      luot -= 1
      if (data.type === "ve1") {
        setVe1((ve1)=>ve1 + 1);
        setTypeVe(data.type)
        setModalThuong(true)
      }
      if (data.type === "ve2") {
        setVe2((ve2)=>ve2 + 1)
        setTypeVe(data.type)
        setModalThuong(true)
      }
      if (data.type === "ve3") {
        setVe3((ve3)=>ve3 + 1)
        setTypeVe(data.type)
        setModalThuong(true)
      }
      if (data.type === "ve4") {
        setVe4((ve4)=>ve4 + 1)
        setPlaneModal(true)
      }

      if (data.type === 'voucher') {
        setVoucherModal(true);
      }

      if (data.type === 'tien') {
        const soTien = Math.floor(Math.random() * 5);
        setTypeCash(arrCash[soTien]);
        setCashModal(true);
      }

      if (data.type === 'thua') {
        setFalseModal(true);
      }
    };

    eventEmitter.addListener(GAME_EVENTS.GAME_ENDED, showPopup);
  }, [gameInited, initGame, navigate, gameEndedLn, refreshUser, refreshInventory]);

  soundUtils.play('flipSound');

  return (
    <Fragment>
      <Header
        showBackBtn={true}
        hideMute={true}
        hideShareBtn={false}
        content="Xin chào"
        user={'Tiến tới thủ đô'}
        onBack={() => navigate({ pathname: '/intro2', search: window.location.search })}></Header>
      <div className={classes.container_veMayBay}>
        <div className={classes.content_veMayBay}><img className={classes.veMayBay} src={ASSETS.VE_MAY_BAY_1} />
          <div className={classes.number_veMayBay}>{ve1}</div></div>
        <div className={classes.content_veMayBay}><img className={classes.veMayBay} src={ASSETS.VE_MAY_BAY_2} />
          <div className={classes.number_veMayBay}>{ve2}</div></div>
        <div className={classes.content_veMayBay}><img className={classes.veMayBay} src={ASSETS.VE_MAY_BAY_3} />
          <div className={classes.number_veMayBay}>{ve3}</div></div>
        <div className={classes.content_veMayBay}><img className={classes.veMayBay} src={ASSETS.VE_MAY_BAY_4} />
          <div className={classes.number_veMayBay}>{ve4}</div></div>
      </div>
      <HomVe isOpen={modalThuong} manhVe={typeVe!} closeModal={() => { setModalThuong(false); game.current!.reset() }} />
      <RewardVoucherModal isOpen={voucherModal} closeModal={() => { setVoucherModal(false); game.current!.reset() }} />
      <RewardCash isOpen={cashModal} rewardValue={typeCash!} closeModal={() => { setCashModal(false); game.current!.reset() }} />
      <PlaneTickets isOpen={planeModal} closeModal={() => { setPlaneModal(false); game.current!.start(); handleReset() }} onOk={() => navigate({ pathname: '/complete-gane-2', search: window.location.search })} />
      <Failure isOpen={falseModal} closeModal={() => { setFalseModal(false); game.current!.reset() }} />
      <div className={cx(classes.content_text, 'pro-light-shb')}>Số lượt chơi: {luot} lượt</div>
    </Fragment>
  );
}
