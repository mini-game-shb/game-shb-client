import { useState } from 'react';
import { useNavigate } from 'react-router-dom';

import Header from '../../../components/Header';
import { ASSETS } from '../../../utils/assetUtils';
import classes from './KhanhThanh.module.css';

type Props = {
  //
};

export const KhanhThanh: React.FC<Props> = () => {
  const [_tamModalVisible, setTamModalVisible] = useState(false);
  const openTamModal = () => {
    setTamModalVisible(true);
  };

  const navigate = useNavigate();
  return (
    <div className={classes['container']} style={{ backgroundImage: `url(${ASSETS.BG_KHANH_THANH_2})` }}>
      <Header
        showBackBtn={true}
        hideMute={true}
        hideShareBtn={false}
        content="Xin chào"
        user={'Tiến tới thủ đô'}
        onBack={() => navigate({ pathname: '/instruct-mini-map', search: window.location.search })}></Header>
      <div className={classes['footer']}>
        <div
          className={classes['btn-khanh-thanh']}
          style={{ backgroundImage: `url(${ASSETS.BTN_KHANH_THANH})` }}
          onClick={openTamModal}></div>
        <div className={classes['text']}>Số lượt chơi: 5 lượt</div>
      </div>
    </div>
  );
};
