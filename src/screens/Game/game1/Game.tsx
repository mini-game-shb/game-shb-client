// import clsx from 'clsx';
import { Fragment, useCallback, useEffect, useRef, useState } from 'react';
import { useNavigate } from 'react-router';

import Header from '../../../components/Header';
import { MaterialComponent } from '../../../components/materials/Materials';
import ChucMayMan from '../../../components/modal/game1/ChucMayMan';
import HetLuotChoiG1 from '../../../components/modal/game1/HetLuotChoiG1';
import NhanChuG1 from '../../../components/modal/game1/NhanChuG1';
import NhanVatLieu from '../../../components/modal/game1/NhanVatLieu';
import TrungThuongVoucherG1 from '../../../components/modal/game1/TrungThuongVoucherG1';
import config from '../../../config';
import { EMITTER_EVENTS } from '../../../constants';
import { init } from '../../../game/game1';
import { useStoreState } from '../../../hooks';
import { VoucherType } from '../../../typing';
import { GAME_EVENTS } from '../../../utils/constants';
import eventEmitter from '../../../utils/eventEmitter';
import { randomInArray } from '../../../utils/math';
import showNotice from '../../../utils/showNotice';
import { soundUtils } from '../../../utils/soundUtils';

let luotQuay = 40;

export function Game1() {
  const [gameInited, setGameInited] = useState(false);
  const [chuG1Modal, setNhanChuG1Modal] = useState(false);
  const [chucMayManModal, setChucMayManModal] = useState(false);
  const [hetLuotModal, setHetLuotModal] = useState(false);
  const [vatLieuModal, setVatLieuModal] = useState(false);
  const [rewardType, _setRewardType] = useState<VoucherType | 'KHANHTHANH'>();
  const [voucherModal, setVoucherModal] = useState(false);
  const [cat,setCat] = useState(0)
  const [thep,setThep] = useState(0)
  const [nuoc,setNuoc] = useState(0)
  const [gach,setGach] = useState(0)

  const navigate = useNavigate();

  const gameStarted = useRef(false);

  const inventory = useStoreState((state) => state.inventory);
  const game = useRef<Awaited<ReturnType<typeof init>>>();

  const initGame = useCallback(async () => {
    const gameInstance = await init();
    if (gameStarted.current) {
      gameInstance.start();
      gameStarted.current = false;
    }
    game.current = gameInstance;
  }, []);

  const gameEndedLn = useRef<any>();

  useEffect(() => {
    return () => {
      if (gameInited) {
        gameStarted.current = false;
        if (game.current) {
          game.current.dispose();
          game.current = undefined;
        }
        if (gameEndedLn.current) {
          eventEmitter.removeListener(GAME_EVENTS.GAME_ENDED, gameEndedLn.current);
          gameEndedLn.current = null;
        }
      }
    };
  }, [gameInited, gameEndedLn]);

  const randomSpin = useCallback(() => {
    luotQuay -= 1;
    setTimeout(() => {
      const indices = [
        VoucherType.CAT,
        VoucherType.GACH,
        VoucherType.NUOC,
        VoucherType.THE_NAP,
        VoucherType.THEP,
        VoucherType.THUA,
        VoucherType.TIEN,
        VoucherType.VOUCHER,
      ];
      const randomType = randomInArray(indices);
      // eventEmitter.emit(EMITTER_EVENTS.SPIN_ERROR);
      eventEmitter.emit(EMITTER_EVENTS.SPIN_RESULT, randomType);
    }, 300);
  }, []);

  useEffect(() => {
    if (gameInited) {
      return;
    }
    setGameInited(true);
    initGame();

    const handleSpin = async () => {
      eventEmitter.emit(EMITTER_EVENTS.START_SPIN);
      if (!config.online) {
        randomSpin();
        return;
      }

      if (!inventory) return;
      if (!inventory.tickets.length) {
        eventEmitter.emit(EMITTER_EVENTS.SHOW_OUT_OF_TICKET);
        return;
      }
      try {
        // eventEmitter.emit(EMITTER_EVENTS.START_SPIN);
        // const usingTicket = inventory.tickets[0];
        // const reward = await spin(usingTicket.id); //api quay
        // await getInventory();
        // const rewardType = VoucherTypeMap[reward.itemType] || VoucherType.VOUCHER_0K;
        // eventEmitter.emit(EMITTER_EVENTS.SPIN_RESULT, rewardType);
      } catch (error) {
        showNotice('Đã có lỗi xảy ra, vui lòng thử lại sau.');
        eventEmitter.emit(EMITTER_EVENTS.SPIN_ERROR);
      }
    };
    eventEmitter.addListener(GAME_EVENTS.GAME_ENDED, handleSpin);
  }, [gameInited, initGame, inventory, randomSpin]);

  useEffect(() => {
    const handleShowReward = async (voucherType:VoucherType) => {
      switch (voucherType) {
        case VoucherType.GACH:
          setGach((gach)=>gach + 1)
          break;
        case VoucherType.NUOC:
          setNuoc((nuoc)=>nuoc +1)
          break;
        case VoucherType.THEP:
          setThep((thep)=>thep +1)
          break;
        case VoucherType.CAT:
          setCat((cat)=>cat +1)
          break;
        case VoucherType.VOUCHER:
          setVoucherModal(true);
          return;
        default:
          break;
      }
    };
    eventEmitter.addListener(EMITTER_EVENTS.SHOW_REWARD, handleShowReward);
  }, []);

  soundUtils.play('flipSound');

  return (
    <Fragment>
      <Header
        showBackBtn={true}
        hideMute={true}
        hideShareBtn={false}
        content="Xin chào"
        user={'Tiến tới thủ đô'}
        onBack={() => {
          navigate({ pathname: '/intro1', search: window.location.search })
        }}></Header>
        <MaterialComponent cat={cat} thep={thep} nuoc={nuoc} gach={gach}/>
        <NhanChuG1 isOpen={chuG1Modal} closeModal={()=>{setNhanChuG1Modal(false)}}/>
        <ChucMayMan isOpen={chucMayManModal} closeModal={()=>setChucMayManModal(false)}/>
        <HetLuotChoiG1 isOpen={hetLuotModal} closeModal={()=>setHetLuotModal(false)}/>
        <NhanVatLieu isOpen={vatLieuModal} closeModal={()=>setVatLieuModal(false)} rewardType={rewardType!} />
        <TrungThuongVoucherG1 closeModal={()=>setVoucherModal(false)} isOpen={voucherModal}/>
    </Fragment>
  );
}
