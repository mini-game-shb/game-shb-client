export enum VoucherType {
  CAT = 'CAT',
  THUA = 'THUA',
  THEP = 'THEP',
  THE_NAP = 'THE_NAP',
  GACH = 'GACH',
  VOUCHER = 'VOUCHER',
  TIEN = 'TIEN',
  NUOC = 'NUOC',
}

export type VoucherItem = {
  _id: string;
  itemId: string;
  type: VoucherType;
  createdAt: string;
  expiredAt: string;
};
