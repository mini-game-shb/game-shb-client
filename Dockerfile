# Fetching the latest node image on apline linux
FROM node:16.20.2 AS builder

# ARG VITE_USER_API_URL $VITE_USER_API_URL
# ENV VITE_USER_API_URL $VITE_USER_API_URL

# ARG VITE_CUSTOM_API_URL $VITE_CUSTOM_API_URL
# ENV VITE_CUSTOM_API_URL $VITE_CUSTOM_API_URL

# ARG VITE_PRODUCTION $VITE_PRODUCTION
# ENV VITE_PRODUCTION $VITE_PRODUCTION

# Setting up the work directory
WORKDIR /app

# Installing dependencies
RUN   npm i -g pnpm
COPY ./package.json ./
COPY ./package-lock.json ./
RUN pnpm install

# Copying all the files in our project
COPY . /app/

# Building our application
RUN pnpm build

# Fetching the latest nginx image
FROM nginx:1.25.2

# Copying built assets from builder
COPY --from=builder /app/dist /usr/share/nginx/html

# Copying our nginx.conf
COPY ./nginx-site-conf /etc/nginx/conf.d/default.conf
